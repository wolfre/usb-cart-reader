#!/bin/bash

source ci-common.source.sh

zip_build_dir=${SLUG}_${SEMVER}
mkdir -p $zip_build_dir
license_file="$zip_build_dir/license.txt"

function add_file
{
    local src="$1"
    local dir_in_zip="$2"
    local target="$zip_build_dir/$dir_in_zip"
    
    file_or_die "$src"
    mkdir -p "$target"
    cp -v "$src" "$target/"
}

function add_license
{
    local src="$1"
    local what="$2"
    
    if [[ "$what" == "" ]] ; then 
        die "Don't know what license '$src' is for?!"
    fi
    
    file_or_die "$src"
    
    if [[ $(stat "$src" -c %s) -lt 200 ]] ; then
        die "License file '$src' does not seem useable"
    fi
    
    echo "===== License of $what =====" >> "$license_file"
    echo "" >> "$license_file"
    cat "$src" >> "$license_file"
    echo "" >> "$license_file"
    echo "" >> "$license_file"
}

find -wholename "*/$DIR_APP_BUILD_RELATIVE/*.dll" -or -iwholename "*/$DIR_APP_BUILD_RELATIVE/*.exe" -or -iwholename "*/$DIR_APP_BUILD_RELATIVE/*.pdb" | while read f ; do
    if [[ ! "$f" == *nunit* ]] && [[ ! "$f" == *ubstitu* ]] && [[ ! "$f" == *"$BIN_APP_TEST"* ]] ; then
        add_file "$f" "app"
    fi
done

add_file "$DIR_FW/$BIN_FW" "fw"
add_file "CHANGELOG.md" ""

# copy together licenses, starting with our own
add_license "LICENSE" "$SLUG"

# Binah ships its license file
license_binah="$(find -iwholename "*/binah*/*license*" | head -n 1)"
add_license "$license_binah" "Binah"

# CRC32 does not ship its license file, so we have to download it
license_crc32=license.force.crc32.txt
curl -L "https://raw.githubusercontent.com/force-net/Crc32.NET/develop/LICENSE" > $license_crc32
add_license "$license_crc32" "force Crc32"

# See STM32CubeF1/License.md for overview
DIR_CUBE="$DIR_FW/STM32CubeF1"
add_license "$DIR_CUBE/Drivers/CMSIS/LICENSE.txt" "CMSIS"
add_license "$DIR_CUBE/Drivers/CMSIS/Device/ST/STM32F1xx/License.md" "CMSIS Device STM32F1"
add_license "$DIR_CUBE/Drivers/STM32F1xx_HAL_Driver/License.md" "STM32F1 HAL"

zip -r "${zip_build_dir}.zip" "$zip_build_dir"
echo "Done packaging"
