# Change Log

All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
The version of this project refers to the command line interface of the host PC app.

## [Unreleased]

### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.1.0] - 2022-01-16

### Added
- Extend the list of known rom sizes with all sizes from datomatic.no-intro.org (as on Nov. 2021) ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/10))
- Fully emulate FRAM chips in test ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/12))
  - This also adds detection for MX29L0000 and MX29L0001.
    However its a bit unclear if those really exist, so no mention on main README.md for now.
- Incorporate realworld findings of FRAM 29L1100KC-15B0 and 29L1101KC-15B0 ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/14))
  - Helps with FRAM detection
- Support datomatic.no-intro.org xml import format for rom info ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/13))

### Changed
- Rename and improve fields in nfo file ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/11))
  - Rename 'Physical Media Serial' to 'Media Serial 1' and allow more than one
  - Rename 'Physical Media Stamp' to 'Media Stamp'
  - Rename 'ROM Chip Serial' to 'ROM Chip Serial 1' and allow more than one
  - Default 'Dump release date' to 'n/a' as this is most likely only used for *scene releases*

## [1.0.1] - 2021-11-07

### Added
- Include debugging symbols in zip ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/8))

### Fixed
- Do not package NSubstitute in zip ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/7))
- Fix wrong buffer size in CartDumper and add missing tests ([MR](https://gitlab.com/wolfre/usb-cart-reader/-/merge_requests/9))

## [1.0.0] - 2021-11-07

- Initial release

[Unreleased]: https://gitlab.com/wolfre/usb-cart-reader/-/compare/v1.1.0...master
[1.1.0]: https://gitlab.com/wolfre/usb-cart-reader/-/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/wolfre/usb-cart-reader/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/wolfre/usb-cart-reader/-/tree/v1.0.0
