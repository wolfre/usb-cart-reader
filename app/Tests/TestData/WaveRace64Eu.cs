﻿using System;
using System.IO;
using CartReader;
using CartReader.NoIntro;

namespace Tests.TestData
{
    static class WaveRace64Eu
    {


        public static DataFile.Game DatGame()
        {
            return new DataFile.Game()
            {
                description = TheTitle,
                name = TheTitle,
                rom = new DataFile.Rom[]
                {
                    ToDatRom(Big(), TheSerial),
                },
            };
        }

        public static CartReader.CommonFileInfo Big()
        {
            return new CartReader.CommonFileInfo()
            {
                file = new FileInfo(TheTitle + ".z64"),
                size = 8388608,
                crc32 = "FB289893",
                md5 = "310659115E9939F219A783ABDD456CE9",
                sha1 = "C20AA97DC25AEC7A9B9C6889286BBD216FC7CAA4",
                sha256 = "8897DA0CB6E286659AC4D72C5737F6FE97A51BCDFC6E2328AE7DFC0715BF1761",
            };
        }

        public static CartReader.CommonFileInfo Swap()
        {
            return new CartReader.CommonFileInfo()
            {
                file = new FileInfo(TheTitle + ".v64"),
                size = 8388608,
                crc32 = "D74117B0",
                md5 = "BAFF32EE50896E62DC713E23D1A589DA",
                sha1 = "7FFDD44890B1A00E0C13868A18B482FBF8C752F7",
                sha256 = "4C07E69B289777A3ECC3719A1970BA4287737BE03BB79536CCCCE52EB7049EA0",
            };
        }

        static DataFile.Rom ToDatRom(CommonFileInfo dmp, string serial)
        {
            return new DataFile.Rom()
            {
                name = dmp.file.Name,
                size = dmp.size,
                crc = dmp.crc32,
                md5 = dmp.md5,
                sha1 = dmp.sha1,
                status = "verified",
                serial = serial,
            };
        }

        static DbGameEntry.Dump ToDump(CartReader.CommonFileInfo dmp, string serial)
        {
            return new DbGameEntry.Dump()
            {
                size = dmp.size,
                crc32 = dmp.crc32,
                md5 = dmp.md5,
                sha1 = dmp.sha1,
                sha256 = dmp.sha256,
                serial = serial,
            };
        }

        public static DbGameEntry DbGameEntry()
        {
            return new DbGameEntry()
            {
                fullName = TheTitle,
                name = "Wave Race 64 - Kawasaki Jet Ski",
                number = 0820,
                trustedDumps = new DbGameEntry.TrustedDump[]
                {
                    new DbGameEntry.TrustedDump()
                    {
                        dumper = "Mr X",
                        bigEndian = ToDump(Big(), TheSerial),
                        byteSwapped = ToDump(Swap(), TheSerial),
                    },
                },
            };
        }

        public static CartReader.N64.Header Header()
        {
            return new CartReader.N64.Header()
            {
                version = new CartReader.N64.Header.Version() { major = 1, minor = 0 },
                mediaFormat = new CartReader.N64.Header.MediaFormat() 
                {
                    type = 0x4e,
                    id = "WR",
                    region = 0x50, 
                },
            };
        }

        public const string TheTitle = "Wave Race 64 - Kawasaki Jet Ski (Europe) (En,De)";
        public const string TheSerial = "NWRP";

        public const string BoxSerial = "P-NWRP-NNOE";
        public const string BoxBarCode = "045496870034";

        public const string PhysicalMediaSerial = "NUS-NWRP-EUR";
        public const string PhysicalMediaStamp = "19";

        public const string PcbSerial = "NUS-01A-01";
        public const string RomChipSerial = "NUS-NWRP-0";
    }
}
