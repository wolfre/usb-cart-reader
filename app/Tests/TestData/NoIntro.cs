﻿using System;
using System.IO;
using System.Linq;
using CartReader;

namespace Tests.TestData
{
    static class NoIntro
    {

        static DirectoryInfo solution = null;
        static DirectoryInfo Solution
        {
            get
            {
                if (solution == null)
                {
                    var testAssembly = new FileInfo(typeof(NoIntro).Assembly.Location);

                    solution = testAssembly.Directory.Parent;
                    var sln = solution.GetFiles().FirstOrDefault(f => f.Name.EndsWith(".sln"));
                    if (sln == null)
                        throw new Exception("Was not able to find solution dir (" + solution.FullName + ")");
                }

                return solution;
            }
        }

        static DirectoryInfo TestData
        {
            get
            {
                return Solution.Dir("Tests").Dir("TestData");
            }
        }

        public static FileInfo DbFile
        {
            get { return TestData.File("Nintendo - Nintendo 64 (DB Export) (20211114-091233).txt"); }
        }

        public static FileInfo DatFile
        {
            get { return TestData.File("Nintendo - Nintendo 64 (BigEndian) (20211114-091233).dat"); }
        }

    }
}
