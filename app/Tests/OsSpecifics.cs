﻿using System;
using System.IO;
using System.Linq;

namespace Tests
{
    static class OsSpecifics
    {
        public static bool RunningOnWindows
        {
            get
            {
                // https://stackoverflow.com/questions/9129491/c-sharp-compiled-in-mono-detect-os
                int p = (int)Environment.OSVersion.Platform;
                var isUnix = (p == 4) || (p == 128);
                return isUnix == false;
            }
        }

        public static void CreateSymlink(FileInfo symlink, string target)
        {
            // TODO make this happen under windows?!
            if (RunningOnWindows)
                throw new NotSupportedException("Not supported on Windows");

            var ln = new Executable("ln");
            ln.Run("-s", target, symlink.FullName);
        }

        public static string BuildArgs(params string[] args)
        {
            return string.Join(" ", args.Select(Escape));
        }

        static string Escape(string a)
        {
            // TODO possibly different under windows
            var hasSpace = a.Contains(' ');
            var hasSingleQuote = a.Contains('\'');
            var hasDoubleQuote = a.Contains('"');

            if (hasSpace == false && hasSingleQuote == false && hasDoubleQuote == false)
                return a;

            if ((hasSpace || hasSingleQuote) && hasDoubleQuote == false)
                return '"' + a + '"';

            if ((hasSpace || hasDoubleQuote) && hasSingleQuote == false)
                return "'" + a + "'";

            throw new NotSupportedException("Don't know how to escape: " + a);
        }

        public static DirectoryInfo[] Path
        {
            get
            {
                return Environment
                    .GetEnvironmentVariable("PATH")
                    .Split(RunningOnWindows ? ';' : ':')
                    .Select(p => new DirectoryInfo(p))
                    .ToArray();
            }
        }
    }
}
