﻿using System;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class ExecutableTest
    {
        [Test]
        public void EchoEscape([Values("hello", "with space", "with ' single quote", "with \" double quote")] string echoMe)
        {
            var echo = new Executable("echo");
            Console.WriteLine("Using " + echo.Exe);

            var echoed = echo.Run("-n", echoMe);

            Assert.That(echoed, Is.EqualTo(echoMe));
        }
    }
}
