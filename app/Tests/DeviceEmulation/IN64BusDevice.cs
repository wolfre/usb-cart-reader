﻿using System;
namespace Tests.DeviceEmulation
{
    interface IN64BusDevice
    {
        string Name { get; }
        uint Offset { get; }
        uint SizeInBytes { get; }
        ushort MaxStride { get; }
        byte[] Read(uint relativeAddress, ushort wordCount);
        void Write(uint relativeAddress, byte[] data);
    }
}
