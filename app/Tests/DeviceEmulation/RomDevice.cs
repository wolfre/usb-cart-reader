﻿using System;
using System.Linq;
using NUnit.Framework;

namespace Tests.DeviceEmulation
{
    class RomDevice : IN64BusDevice
    {
        readonly uint address;
        readonly byte[] data;

        public RomDevice(uint address, byte[] data)
        {
            this.address = address;
            this.data = data;
        }

        public string Name
        {
            get { return "Rom"; }
        }

        public uint Offset => address;

        public uint SizeInBytes => (uint)data.Length;

        public virtual ushort MaxStride => CartReader.N64.N64Consts.StrideWordsRom;

        public byte[] Read(uint relativeAddress, ushort wordCount)
        {
            return data.Skip((int)relativeAddress).Take(wordCount * 2).ToArray();
        }

        public void Write(uint relativeAddress, byte[] data)
        {
            Assert.Fail("Writing is not supported on roms");
        }
    }
}
