﻿using System;
namespace Tests.DeviceEmulation
{
    class SRamDevice : RomDevice
    {
        public SRamDevice(uint address, byte[] data) : base(address, data)
        {
        }

        public override ushort MaxStride => CartReader.N64.N64Consts.StrideWordsSRam;
    }
}
