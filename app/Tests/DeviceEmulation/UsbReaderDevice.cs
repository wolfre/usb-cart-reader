﻿using System;
using System.Collections.Generic;
using System.Linq;
using CartReader;
using CartReader.Device;
using NUnit.Framework;

namespace Tests.DeviceEmulation
{
    class UsbReaderDevice : IDeviceInterface10
    {
        readonly byte[] buffer;

        public UsbReaderDevice(ushort bufferSize)
        {
            buffer = new byte[bufferSize];
        }

        enum Mode
        {
            None,
            ReadFromBuffer,
            WriteToBuffer,
        }

        Mode mode = Mode.None;

        #region IDeviceInterface10

        public DeviceInfo10 Info
        {
            get
            {
                return new DeviceInfo10()
                {
                    bufferSize = (ushort)buffer.Length,
                    gitCommit = new byte[20],
                    unknownEntries = null,
                };
            }
        }

        public void CommandBufferRead()
        {
            mode = Mode.ReadFromBuffer;
            Console.WriteLine(nameof(CommandBufferRead) + "()");
        }

        public void CommandBufferWrite()
        {
            mode = Mode.WriteToBuffer;
            Console.WriteLine(nameof(CommandBufferWrite) + "()");
        }

        public void CommandBusRead(uint address, ushort words, ushort stride)
        {
            AssertBus(nameof(CommandBusRead), address, words, stride);
            var dev = GetDevice(address, words, stride);
            var rd = dev.Read(address - dev.Offset, words);
            Array.Copy(rd, buffer, rd.Length);
            mode = Mode.ReadFromBuffer;
        }

        public void CommandBusWrite(uint address, ushort words, ushort stride)
        {
            AssertBus(nameof(CommandBusWrite), address, words, stride);
            var dev = GetDevice(address, words, stride);
            dev.Write(address - dev.Offset, buffer.Take(words * 2).ToArray());
            mode = Mode.None;
        }

        void AssertBus(string cmd, uint address, ushort words, ushort stride)
        {
            Console.WriteLine(cmd + "(" + address.Hex() + "," + words + "," + stride + ")");
            Assert.That(words, Is.GreaterThan(0), "Tried to use 0 words");
            Assert.That(words * 2, Is.LessThanOrEqualTo(buffer.Length), "Tried to use more than internal buffer could take");
            Assert.That(stride, Is.GreaterThan(0), "stride");
        }

        IN64BusDevice GetDevice(uint address, ushort words, ushort stride)
        {
            foreach (var d in bus)
            {
                var end = d.Offset + d.SizeInBytes;
                if (address >= d.Offset && address < end)
                {
                    Assert.That(address + (uint)(words * 2), Is.LessThanOrEqualTo(end), "Access outside the bounds of " + d.Name);
                    Assert.That(stride, Is.LessThanOrEqualTo(d.MaxStride), "Access of device " + d.Name + " had illegal stride");
                    return d;
                }
            }

            throw new Exception("Unmapped access at " + address.Hex() + " for " + (words * 2));
        }

        public void CommandReadEeprom(byte word)
        {
            Assert.That(eeprom, Is.Not.Null, "No EEPROM was mapped");
            Assert.That(word, Is.LessThan(eeprom.SizeInWords), "Read beyond end of EEPROM");

            var rd = eeprom.Read(word);
            if (rd.Length != CartReader.N64.N64Consts.EepromWordSizeBytes)
                throw new Exception("Emulated eeprom device returned an illegal word size: " + rd.Length);

            Array.Copy(rd, buffer, rd.Length);
            mode = Mode.ReadFromBuffer;
        }

        public byte[] ReadFromBuffer(int len)
        {
            Assert.That(mode, Is.EqualTo(Mode.ReadFromBuffer));
            Console.WriteLine(nameof(ReadFromBuffer) + "(" + len + ")");
            Assert.That(len, Is.LessThanOrEqualTo(buffer.Length), "Too much data");
            return buffer.Take(len).ToArray();
        }

        public void SendToBuffer(byte[] data)
        {
            Assert.That(mode, Is.EqualTo(Mode.WriteToBuffer));
            Console.WriteLine(nameof(SendToBuffer) + "(" + data.Length + ")");
            Assert.That(data.Length, Is.LessThanOrEqualTo(buffer.Length), "Too much data");
            Array.Copy(data, buffer, data.Length);
        }

        #endregion

        #region DeviceSimulation

        readonly List<IN64BusDevice> bus = new List<IN64BusDevice>();

        public void Add(IN64BusDevice device)
        {
            if (device.SizeInBytes < 2)
                throw new Exception("Bus device needs to have at least 2 bytes of address space");
            if ((device.SizeInBytes % 2) != 0)
                throw new Exception("Bus devices need to have an even size address space: " + device.SizeInBytes);

            bus.Add(device);
        }

        IN64EepromDevice eeprom;

        public void Add(IN64EepromDevice device)
        {
            if (eeprom != null)
                throw new NotSupportedException("Alread have an EEPROM");

            if (device.SizeInWords < 1 || device.SizeInWords > (int)byte.MaxValue + 1 )
                throw new Exception("EEPROMs need to have [1;256] words: " + device.SizeInWords);

            eeprom = device;
        }

        #endregion
    }

}
