﻿using System;
using System.Linq;

namespace Tests.DeviceEmulation
{
    class EepromDevice : IN64EepromDevice
    {
        readonly Random rnd = new Random(0);
        double readErrorQuota;

        private readonly byte[] data;

        public EepromDevice(byte[] data, double readErrorQuota)
        {
            this.data = data;
            this.readErrorQuota = readErrorQuota;
        }

        public ushort SizeInWords => (ushort)(data.Length / CartReader.N64.N64Consts.EepromWordSizeBytes);

        public byte[] Read(byte word)
        {
            var withReadError = rnd.NextDouble() < readErrorQuota;

            var wordData = data
                .Skip(word * CartReader.N64.N64Consts.EepromWordSizeBytes)
                .Take(CartReader.N64.N64Consts.EepromWordSizeBytes)
                .ToArray();

            if (withReadError)
                wordData[2] ^= 0xff;

            return wordData;
        }
    }
}
