﻿using System;
namespace Tests.DeviceEmulation
{
    interface IN64EepromDevice
    {
        ushort SizeInWords { get; }
        byte[] Read(byte word);
    }
}
