﻿using System;
using System.Linq;
using CartReader;
using CartReader.N64;
using NUnit.Framework;

namespace Tests.DeviceEmulation
{
    class FRamDeviceConfig
    {
        public FramId id;
        public FramInfo nfo;
    }

    class FRamDevice : IN64BusDevice
    {
        const uint OffsetRegisterCommand = N64Consts.FRamRegisterCommand - N64Consts.FRamBase;
        const uint OffsetRegisterStatus = N64Consts.FRamRegisterStatus - N64Consts.FRamBase;

        static void Trace(string m)
        {
            Console.WriteLine("FRam: " + m);
        }

        readonly FRamDeviceConfig cfg;
        readonly uint address;
        readonly DataCodec codec = new DataCodec();

        public FRamDevice(FRamDeviceConfig cfg, uint address, byte[] data)
        {
            this.cfg = cfg;
            this.address = address;
            this.data = data;
        }

        public string Name
        {
            get { return "FRam"; }
        }

        public uint Offset => address;

        public uint SizeInBytes => (uint)data.Length;

        public ushort MaxStride => N64Consts.StrideWordsFRam;


        #region state

        enum Mode
        {
            None,
            ReadData
        }

        readonly byte[] data;
        byte[] statusRegister = null;
        Mode mode = Mode.None;

        #endregion

        public byte[] Read(uint relativeAddress, ushort wordCount)
        {
            if (mode == Mode.None)
            {
                Assert.That(relativeAddress, Is.EqualTo(OffsetRegisterStatus), "Illegal read in mode " + mode);
                Assert.That(wordCount, Is.EqualTo(4), "Illegal read size from status register, possibly trying to read data without setting up read mode?!");
                Assert.That(statusRegister != null, "Status register was not ready");
                var s = statusRegister;
                statusRegister = null;
                Trace("Read status register: " + s.Hex());
                return s;
            }

            Assert.That(mode, Is.EqualTo(Mode.ReadData), "Illegal mode for read");
            return data.Skip((int)relativeAddress).Take(wordCount * 2).ToArray();
        }

        public void Write(uint relativeAddress, byte[] data)
        {
            if (mode == Mode.None)
            {
                Assert.That(relativeAddress, Is.EqualTo(OffsetRegisterCommand), "Illegal write in mode " + mode);
                Assert.That(data.Length, Is.EqualTo(4), "Illegal write size to command register");
                Command(codec.DecodeU32(data));
                return;
            }

            throw new AssertionException("Write not implements");
        }

        void Command(uint cmd)
        {
            if (cmd == N64Consts.FRamCommandReadMode)
            {
                Trace("Read mode command");
                mode = Mode.ReadData;
                return;
            }

            if (cmd == N64Consts.FRamCommandId)
            {
                Trace("Get ID command");
                statusRegister = codec.Encode(cfg.id.unknown).Concat(codec.Encode(cfg.id.id)).ToArray();
                return;
            }

            throw new AssertionException("Unknown command " + cmd.Hex());
        }
    }
}
