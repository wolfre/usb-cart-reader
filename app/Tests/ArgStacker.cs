﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Tests
{
    class ArgStacker<T, X>
    {
        readonly List<T> a = new List<T>();
        readonly List<X> ret = new List<X>();

        public X Call(T args)
        {
            a.Add(args);
            Assert.That(ret.Count, Is.GreaterThan(0), "No returns left");
            var r = ret[0];
            ret.RemoveAt(0);
            return r;
        }

        public T TakeNext()
        {
            Assert.That(a.Count, Is.GreaterThan(0), "No calls left");
            var n = a[0];
            a.RemoveAt(0);
            return n;
        }

        public bool ReturnAvailable { get { return ret.Count > 0; } }

        public void ReturnNext(X ret)
        {
            this.ret.Add(ret);
        }

        public void NoCallsLeft()
        {
            Assert.That(a.Count, Is.EqualTo(0), "Calls left");
        }
    }
}
