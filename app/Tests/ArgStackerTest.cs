﻿using System;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    class ArgStackerTest
    {
        [Test]
        public void Stack()
        {
            var s = new ArgStacker<int, int>();

            s.ReturnNext(42);
            var r = s.Call(10);

            Assert.That(r, Is.EqualTo(42));
            Assert.That(s.TakeNext(), Is.EqualTo(10));
        }

        [Test]
        public void StackMore()
        {
            var s = new ArgStacker<int, int>();

            s.ReturnNext(1);
            s.ReturnNext(2);
            var r1 = s.Call(10);
            var r2 = s.Call(20);

            Assert.That(r1, Is.EqualTo(1));
            Assert.That(r2, Is.EqualTo(2));
            Assert.That(s.TakeNext(), Is.EqualTo(10));
            Assert.That(s.TakeNext(), Is.EqualTo(20));
            s.NoCallsLeft();
        }
    }
}
