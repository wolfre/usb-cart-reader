﻿using System;
using System.IO;
using CartReader;
using NUnit.Framework;

namespace Tests.UnitTest
{
    [TestFixture]
    public class CommonFileInfoGeneratorTest
    {
        const int Size = (1 * 1024 * 1024);

        static void GenTestFile(FileInfo tst)
        {
            const int BytePrime = 251;

            using (var s = tst.Open(FileMode.Create, FileAccess.Write, FileShare.Read))
                for (int i = 0; i < Size; ++i)
                    s.WriteByte((byte)(i % BytePrime));
        }

        static void Swap(FileInfo output, FileInfo input)
        {
            var buffer = new byte[1024];

            using (var strmO = output.Open(FileMode.Create, FileAccess.Write, FileShare.Read))
            using (var strmI = input.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                while (true)
                {
                    var r = strmI.Read(buffer, 0, buffer.Length);
                    if (r < 1)
                        return;

                    for (int i = 0; i < r; i+=2)
                    {
                        var h = buffer[i];
                        buffer[i] = buffer[i + 1];
                        buffer[i + 1] = h;
                    }

                    strmO.Write(buffer, 0, r);
                }
            }
        }

        static readonly CommonFileInfo ExpectedNonSwapped = new CommonFileInfo()
        {
            size = Size,
            crc32 = "ef0e6054",
            md5 = "8f293a2f6c19b345152f7a49bb4c643c",
            sha1 = "c2fc4cb20f1301a6b0dd211c19e69a13925dbe40",
            sha256 = "631b84027d6b9e52b539c4e8373622d23032dfadc64d60af87339c9037e4f769",
        };

        TempFile testFile;
        TempFile testFileSymLink;
        TempFile testFileSwapped;
        TempFile testFileSwappedSymLink;

        [SetUp]
        public void Init()
        {
            testFile = new TempFile();
            testFileSwapped = new TempFile();

            GenTestFile(testFile.file);
            Swap(testFileSwapped.file, testFile.file);

            testFileSymLink = new TempFile();
            testFileSwappedSymLink = new TempFile();

            OsSpecifics.CreateSymlink(testFileSymLink.file, testFile.file.FullName);
            OsSpecifics.CreateSymlink(testFileSwappedSymLink.file, testFileSwapped.file.FullName);
        }

        [TearDown]
        public void Cleanup()
        {
            testFile.Dispose();
            testFileSymLink.Dispose();
            testFileSwapped.Dispose();
            testFileSwappedSymLink.Dispose();
        }

        [Test]
        public void NonSwapped([Values(true,false)] bool symLink)
        {
            var srcFile = symLink ? testFileSymLink.file : testFile.file;
            var calc = CommonFileInfoGenerator.CalcFileInfo(srcFile, false);

            Assert.That(calc.file.FullName, Is.EqualTo(srcFile.FullName));
            AssertHashes(calc);
        }

        [Test]
        public void Swapped([Values(true, false)] bool symLink)
        {
            var srcFile = symLink ? testFileSwappedSymLink.file : testFileSwapped.file;
            var calc = CommonFileInfoGenerator.CalcFileInfo(srcFile, true);

            Assert.That(calc.file.FullName, Is.EqualTo(srcFile.FullName));
            AssertHashes(calc);
        }

        static void AssertHashes(CommonFileInfo calc)
        {
            Assert.That(calc.size, Is.EqualTo(Size));
            Assert.That(calc.md5, Is.EqualTo(ExpectedNonSwapped.md5), "md5");
            Assert.That(calc.sha1, Is.EqualTo(ExpectedNonSwapped.sha1), "sha1");
            Assert.That(calc.sha256, Is.EqualTo(ExpectedNonSwapped.sha256), "sha256");
            Assert.That(calc.crc32, Is.EqualTo(ExpectedNonSwapped.crc32), "crc32");
        }
    }
}
