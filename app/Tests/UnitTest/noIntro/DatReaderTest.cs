﻿using System;
using System.IO;
using CartReader.NoIntro;
using NUnit.Framework;

namespace Tests.UnitTest.noIntro
{
    [TestFixture]
    class DatReaderTest
    {
        static readonly string[] example20211114 = new string[]
        {
            "<?xml version=\"1.0\"?>",
            "<!DOCTYPE datafile PUBLIC \"-//Logiqx//DTD ROM Management Datafile//EN\" \"http://www.logiqx.com/Dats/datafile.dtd\">",
            "<datafile>",
            "       <header>",
            "               <name>Nintendo - Nintendo 64 (BigEndian)</name>",
            "               <description>Nintendo - Nintendo 64 (BigEndian)</description>",
            "               <version>20211114-091233</version>",
            "               <author>aci68, Aringon, BigFred, BitLooter, Blank, C. V. Reynolds, chillerecke, DeriLoko3, einstein95, ElBarto, Fake Shemp, Hiccup, jimmsu, kazumi213, Madeline, Money_114, omonim2007, Powerpuff, PPLToast, relax, Rifu, scorp256, sCZther, SonGoku, Special T, TeamEurope, xuom2</author>",
            "               <homepage>No-Intro</homepage>",
            "               <url>http://www.no-intro.org</url>",
            "               <clrmamepro forcenodump=\"required\"/>",
            "       </header>",
            "       <game name=\"007 - The World Is Not Enough (USA) (v2) (Beta)\">",
            "               <description>007 - The World Is Not Enough (USA) (v2) (Beta)</description>",
            "               <rom name=\"007 - The World Is Not Enough (USA) (v2) (Beta).z64\" size=\"16777216\" crc=\"6C180FEF\" md5=\"39ABE0C383A44C464B408BE3E1D6766E\" sha1=\"376042DDEDFD4F738786500531D67D5C299577BE\" serial=\"NM4E\"/>",
            "       </game>",
            "       <game name=\"007 - The World Is Not Enough (Europe) (En,Fr,De)\">",
            "               <description>007 - The World Is Not Enough (Europe) (En,Fr,De)</description>",
            "               <rom name=\"007 - The World Is Not Enough (Europe) (En,Fr,De).z64\" size=\"33554432\" crc=\"002C3B2A\" md5=\"34AB1DEA3111A233A8B5C5679DE22E83\" sha1=\"7FDE668850A7E1A8402AB94BB09538A537A7E38B\" status=\"verified\" serial=\"NO7P\"/>",
            "       </game>",
            "       <game name=\"007 - The World Is Not Enough (USA)\">",
            "               <description>007 - The World Is Not Enough (USA)</description>",
            "               <rom name=\"007 - The World Is Not Enough (USA).z64\" size=\"33554432\" crc=\"26360987\" md5=\"9D58996A8AA91263B5CD45C385F45FE4\" sha1=\"D347159808F0374A93CF44CFB6135D8F56279F7B\" serial=\"NO7E\"/>",
            "       </game>",
            // ...
            "</datafile>",
        };

        [SetUp]
        public void Init()
        {
        }

        [TearDown]
        public void Cleanup()
        {
        }

        [Test]
        public void Read()
        {
            var dat = Read(example20211114);
            Assert.That(dat.game.Length, Is.EqualTo(3));
            Assert.That(dat.game[0].name, Is.EqualTo("007 - The World Is Not Enough (USA) (v2) (Beta)"));
            Assert.That(dat.game[0].description, Is.EqualTo("007 - The World Is Not Enough (USA) (v2) (Beta)"));
            Assert.That(dat.game[0].rom.Length, Is.EqualTo(1));
            Assert.That(dat.game[0].rom[0].name, Is.EqualTo("007 - The World Is Not Enough (USA) (v2) (Beta).z64"));
            Assert.That(dat.game[0].rom[0].size, Is.EqualTo(16777216));
            Assert.That(dat.game[0].rom[0].crc, Is.EqualTo("6C180FEF"));
            Assert.That(dat.game[0].rom[0].md5, Is.EqualTo("39ABE0C383A44C464B408BE3E1D6766E"));
            Assert.That(dat.game[0].rom[0].sha1, Is.EqualTo("376042DDEDFD4F738786500531D67D5C299577BE"));
            Assert.That(dat.game[0].rom[0].status, Is.EqualTo(null));
            Assert.That(dat.game[0].rom[0].serial, Is.EqualTo("NM4E"));
        }

        DataFile Read(string[] lines)
        {
            using (var tmp = new TempFile())
            {
                File.WriteAllLines(tmp.file.FullName, lines);
                var cut = new DatReader();
                return cut.Read(tmp.file);
            }
        }
    }
}
