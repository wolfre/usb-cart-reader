﻿using System;
using System.IO;
using CartReader;
using CartReader.NoIntro;
using NUnit.Framework;

namespace Tests.UnitTest.noIntro
{
    [TestFixture]
    class DatabaseCompletetionTest
    {
        [Test]
        public void FindAll()
        {
            var datReader = new DatReader();
            var dat = datReader.Read(TestData.NoIntro.DatFile);

            var complete = new DatabaseCompletion(TestData.NoIntro.DatFile, TestData.NoIntro.DbFile);

            Console.WriteLine("Read DAT with " + dat.game.Length + " games in it");
            foreach (var game in dat.game)
            {
                var bigEndian = FakeFileInfo(game);
                var completed = complete.GetSources(bigEndian);
                Assert.That(completed.datGame, Is.Not.Null, game.name);
                Assert.That(completed.dbGame, Is.Not.Null, game.name);
            }
        }

        CommonFileInfo FakeFileInfo(DataFile.Game game)
        {
            if (game.rom == null || game.rom.Length == 0)
                throw new Exception("Game '" + game.name + "' does not have a rom");

            var r = game.rom[0];

            return new CommonFileInfo()
            {
                crc32 = r.crc,
                md5 = r.md5,
                sha1 = r.sha1,
                sha256 = null,
                size = r.size,
                file = new FileInfo(game.name + ".z64"),
            };
        }
    }
}
