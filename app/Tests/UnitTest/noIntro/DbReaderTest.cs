﻿using System.Collections.Generic;
using System.IO;
using CartReader.NoIntro;
using NUnit.Framework;

namespace Tests.UnitTest.noIntro
{
    [TestFixture]
    class DbReaderTest
    {
        TempFile tmp;

        [SetUp]
        public void Init()
        {
            tmp = new TempFile();
        }

        [TearDown]
        public void Cleanup()
        {
            tmp.Dispose();
        }

        void ToFile(FileInfo output, string lineEnding, string[] lines)
        {
            using (var wrt = new StreamWriter(output.FullName) { NewLine = lineEnding })
                foreach (var l in lines)
                    wrt.WriteLine(l);
        }

        static readonly TestCaseData[] LineEndings = new TestCaseData[]
        {
            new TestCaseData("\n"),
        };

        [Test]
        [TestCaseSource(nameof(LineEndings))]
        public void Read(string lf)
        {
            ToFile(tmp.file, lf, DbExamples.n64DbExport20211114);

            var all = Read(tmp.file);


            Assert.That(all[0].fullName, Is.EqualTo("007 - The World Is Not Enough (Europe) (En,Fr,De)"));
            Assert.That(all[0].name, Is.EqualTo("007 - The World Is Not Enough"));
            Assert.That(all[0].number, Is.EqualTo(4));
            Assert.That(all[0].trustedDumps.Length, Is.EqualTo(2));
            Assert.That(all[0].trustedDumps[0].dumper, Is.EqualTo("MikeHaggar"));
            Assert.That(all[0].trustedDumps[0].bigEndian.size, Is.EqualTo(33554432));
            Assert.That(all[0].trustedDumps[0].bigEndian.crc32, Is.EqualTo("002C3B2A"));
            Assert.That(all[0].trustedDumps[0].bigEndian.md5, Is.EqualTo("34AB1DEA3111A233A8B5C5679DE22E83"));
            Assert.That(all[0].trustedDumps[0].bigEndian.sha1, Is.EqualTo("7FDE668850A7E1A8402AB94BB09538A537A7E38B"));
            Assert.That(all[0].trustedDumps[0].bigEndian.sha256, Is.EqualTo("06C65F4F35C869468502F33BF7EDD2D4995C9CCCADA6BFF30FE4E8DA0209C2AC"));
            Assert.That(all[0].trustedDumps[0].bigEndian.serial, Is.EqualTo("NO7P"));
            Assert.That(all[0].trustedDumps[0].byteSwapped.crc32, Is.EqualTo("7F030D3A"));

            Assert.That(all[1].fullName, Is.EqualTo("007 - The World Is Not Enough (USA)"));
            Assert.That(all[1].name, Is.EqualTo("007 - The World Is Not Enough"));
            Assert.That(all[1].number, Is.EqualTo(5));
            Assert.That(all[1].trustedDumps.Length, Is.EqualTo(1));
            Assert.That(all[1].trustedDumps[0].dumper, Is.EqualTo("JCreazy"));
            Assert.That(all[1].trustedDumps[0].bigEndian.size, Is.EqualTo(33554432));

            Assert.That(all.Length, Is.EqualTo(2));
        }

        DbGameEntry[] Read(FileInfo txt)
        {
            var all = new List<DbGameEntry>();

            using (var r = new DbReader(txt))
            {
                while (true)
                {
                    var e = r.ReadNext();
                    if (e == null)
                        return all.ToArray();

                    all.Add(e);
                }
            }
        }
    }
}
