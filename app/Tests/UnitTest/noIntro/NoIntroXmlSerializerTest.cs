﻿using System;
using System.IO;
using CartReader.NoIntro;
using NUnit.Framework;
using CartReader;
using Tests.TestData;
using System.Collections.Generic;
using CartReader.N64;
using CartReader.Actions;

namespace Tests.UnitTest.noIntro
{
    [TestFixture]
    class NoIntroXmlSerializerTest
    {
        const string Dumper = "Your Name";
        const string Tool = "The Tool v4.2";

        static NoIntroRedumpSources GenRd()
        {
            return new NoIntroRedumpSources()
            {
                datGame = WaveRace64Eu.DatGame(),
                dbGame = WaveRace64Eu.DbGameEntry(),
                bigEndian = WaveRace64Eu.Big(),
                byteSwapped = WaveRace64Eu.Swap(),
                header = WaveRace64Eu.Header(),
                manualEntry = new NoIntroRedumpSources.AdditionalManualInfo()
                {
                    dumper = Dumper,
                    tool = Tool,
                    boxBarcode = WaveRace64Eu.BoxBarCode,
                    boxSerial = new string[] { WaveRace64Eu.BoxSerial },
                    mediaSerial = new string[] { WaveRace64Eu.PhysicalMediaSerial },
                    mediaStamp = WaveRace64Eu.PhysicalMediaStamp,
                    pcbSerial = new string[] { WaveRace64Eu.PcbSerial },
                    romChipSerial = new string[] { WaveRace64Eu.RomChipSerial },
                },
            };
        }

        static KeyValuePair<string, string> Kv(string k, string v)
        {
            return new KeyValuePair<string, string>(k, v);
        }

        [Test]
        public void WaveRace64()
        {
            var xml = Serialize(GenRd());

            AssertAttributes(xml, new KeyValuePair<string, string>[]
            {
                Kv("name", WaveRace64Eu.TheTitle),
                Kv("gameid", "0820"),
                Kv("dumper", Dumper),
                Kv("tool", Tool),
                Kv("region", "Europe"),

                Kv("mediaserial1","NUS-NWRP-EUR" ),
                Kv("pcbserial","NUS-01A-01" ),
                Kv("romchipserial1","NUS-NWRP-0" ),
                Kv("boxserial","P-NWRP-NNOE" ),
                Kv("mediastamp","19" ),
                Kv("boxbarcode","045496870034"),

                Kv("format", NoIntroConsts.FormatBigEndian),
                Kv("version","0" ),
                Kv("size","8388608" ),
                Kv("crc","fb289893"),
                Kv("md5","310659115e9939f219a783abdd456ce9" ),
                Kv("sha1","c20aa97dc25aec7a9b9c6889286bbd216fc7caa4" ),
                Kv("sha256","8897da0cb6e286659ac4d72c5737f6fe97a51bcdfc6e2328ae7dfc0715bf1761" ),

                Kv("format", NoIntroConsts.FormatByteSwapped),
                Kv("version","0" ),
                Kv("size","8388608" ),
                Kv("crc","d74117b0" ),
                Kv("md5","baff32ee50896e62dc713e23d1a589da" ),
                Kv("sha1","7ffdd44890b1a00e0c13868a18b482fbf8c752f7" ),
                Kv("sha256","4c07e69b289777a3ecc3719a1970ba4287737be03bb79536cccce52eb7049ea0" ),

                Kv("serial","NWRP" ),
                Kv("bad","0" ),
                Kv("unique","1"),
            });
        }

        static void AssertAttributes(string xml, KeyValuePair<string, string>[] attributes)
        {
            foreach (var e in attributes)
                Assert.That(xml, Contains.Substring(e.Key + "=\"" + e.Value + "\""));
        }

        [Test]
        public void MultiSerial()
        {
            const string MS1 = "media-serial-1";
            const string MS2 = "media-serial-2";

            const string C1 = "chip-1";
            const string C2 = "chip-2";

            var rd = GenRd();
            rd.manualEntry.mediaSerial = new string[] { MS1, MS2 };
            rd.manualEntry.romChipSerial = new string[] { C1, C2 };
            var xml = Serialize(rd);

            AssertAttributes(xml, new KeyValuePair<string, string>[]
            {
                Kv("mediaserial1", MS1 ),
                Kv("mediaserial2", MS2 ),
                Kv("romchipserial1", C1 ),
                Kv("romchipserial2", C2 ),
            });
        }

        static readonly TestCaseData[] versionCombo = new TestCaseData[]
        {
            new TestCaseData(new Header.Version(){ major = 1, minor = 0 }, "0"),
            new TestCaseData(new Header.Version(){ major = 1, minor = 1 }, "1"),
            new TestCaseData(new Header.Version(){ major = 2, minor = 0 }, "16"),
        };

        [Test]
        [TestCaseSource(nameof(versionCombo))]
        public void Version(Header.Version ver, string ex)
        {
            var rd = GenRd();
            rd.header.version = ver;

            var xml = Serialize(rd);

            Assert.That(xml, Contains.Substring("version=\"" + ex + "\""));
        }

        [Test]
        public void NoBoxSerials([Values(null, Misc.NotAvailable, Misc.Unknown)] string v)
        {
            var rd = GenRd();

            rd.manualEntry.boxBarcode = v;
            rd.manualEntry.boxSerial = v == null ? new string[0] : new string[] { v };

            var xml = Serialize(rd);

            foreach (var notHere in new string[] { "boxbarcode", "boxserial" })
                Assert.That(xml.Contains(notHere + "="), Is.False, notHere);
        }

        string Serialize(NoIntroRedumpSources src)
        {
            using (var mem = new MemoryStream())
            {
                var ser = new NoIntroXmlSerializer();
                ser.Serialize(mem, src);
                var xml = System.Text.Encoding.UTF8.GetString(mem.ToArray());
                Console.WriteLine(xml);
                return xml;
            }
        }
    }
}
