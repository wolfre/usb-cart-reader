﻿using System;
using System.Collections.Generic;
using CartReader.Interactive;
using CartReader.NoIntro;
using NSubstitute;
using NUnit.Framework;

namespace Tests.UnitTest.noIntro
{
    [TestFixture]
    class InteractiveCompletionTest
    {
        class StubDialog : IDialog
        {
            public char MultiValueSeparator { get; set; }

            public bool TrimSpacesFromAnswer { get; set; }

            public readonly List<string> questions = new List<string>();

            public string[] AskOneLineMultiEntryText(string[] defaultValue, string question, DialogConfirmation confirm)
            {
                return NextAnswer(question);
            }


            public string AskOneLineText(string defaultValue, string question, DialogConfirmation confirm)
            {
                var a = NextAnswer(question);
                Assert.That(a.Length, Is.EqualTo(1), "Answer to '" + question + "' was not a sinle entry answer, but: '" + string.Join("','", a) + "'");
                return a[0];
            }

            string[] NextAnswer(string question)
            {
                questions.Add(question);
                Assert.That(answers.Count, Is.GreaterThan(0), "No aswer for '" + question + "'");
                var a = answers[0];
                answers.RemoveAt(0);
                return a;
            }

            readonly List<string[]> answers = new List<string[]>();

            public string[] AddAnswer(string[] multiEntry)
            {
                answers.Add(multiEntry);
                return multiEntry;
            }

            public string AddAnswer(string oneEntry)
            {
                answers.Add(new string[] { oneEntry });
                return oneEntry;
            }
        }

        StubDialog dialog;

        [SetUp]
        public void Init()
        {
            dialog = new StubDialog();
        }

        [TearDown]
        public void Cleanup()
        {
            Console.WriteLine("All questions:");
            foreach (var q in dialog.questions)
                Console.WriteLine("Q: '" + q + "'");
        }

        [Test]
        public void CompleteMissingNoIntroRedumpSources()
        {
            var dumper = dialog.AddAnswer("Mr. Dump");
            var tool = dialog.AddAnswer("The Tool");
            var edition = dialog.AddAnswer("Some Edition");
            var mediaSerial = dialog.AddAnswer(new string[] { "NUS-006 (EUR)", "NUS-NSMP-EUR" });
            var mediaStamp = dialog.AddAnswer("14A");
            var romChipSerial = dialog.AddAnswer(new string[] { "NUS-NSME-0 1", "NUS-NSME-0 2" });
            var pcbSerial = dialog.AddAnswer(new string[] { "NUS-01A-01" });
            var boxSerial = dialog.AddAnswer(new string[] { "P-NWRP-NNOE", "somethingelse" });
            var boxBarcode = dialog.AddAnswer("045496870034");

            var cmp = new InteractiveCompletion(() => dialog);
            var rd = new NoIntroRedumpSources()
            {
                header = new CartReader.N64.Header()
                {
                    version = new CartReader.N64.Header.Version() { major = 1, minor = 1, },
                    mediaFormat = new CartReader.N64.Header.MediaFormat()
                    {
                        type = 0x50,
                        id = "AB",
                        region = 0x33,
                    }
                },
            };
            cmp.CompleteMissing(rd);

            Assert.That(rd.manualEntry.dumper, Is.EqualTo(dumper));
            Assert.That(rd.manualEntry.tool, Is.EqualTo(tool));
            Assert.That(rd.manualEntry.edition, Is.EqualTo(edition));
            Assert.That(rd.manualEntry.mediaSerial, Is.EqualTo(mediaSerial));
            Assert.That(rd.manualEntry.mediaStamp, Is.EqualTo(mediaStamp));
            Assert.That(rd.manualEntry.romChipSerial, Is.EqualTo(romChipSerial));
            Assert.That(rd.manualEntry.pcbSerial, Is.EqualTo(pcbSerial));
            Assert.That(rd.manualEntry.boxSerial, Is.EqualTo(boxSerial));
            Assert.That(rd.manualEntry.boxBarcode, Is.EqualTo(boxBarcode));
        }
    }
}
