﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CartReader.Interactive;
using NUnit.Framework;

namespace Tests.UnitTest.interactive
{
    [TestFixture]
    class TerminalDialogTest
    {
        TempFile tmpOut;
        TempFile tmpIn;

        [SetUp]
        public void Init()
        {
            tmpOut = new TempFile();
            tmpIn = new TempFile();
        }

        [TearDown]
        public void Cleanup()
        {
            tmpIn.Dispose();
            tmpOut.Dispose();
        }

        void SetAnswers(params string[] oneAnswerPerLine)
        {
            File.WriteAllLines(tmpIn.file.FullName, oneAnswerPerLine);
        }

        T DoDialog<T>(Func<TerminalDialog, T> doIt)
        {
            string[] ignore;
            return DoDialog(out ignore, doIt);
        }

        T DoDialog<T>(out string[] output, Func<TerminalDialog, T> doIt)
        {
            T r;

            using (var rdr = new StreamReader(tmpIn.file.FullName))
            using (var wrt = new StreamWriter(tmpOut.file.FullName))
            {
                var cut = new TerminalDialog(rdr, wrt);
                r = doIt(cut);
            }

            output = File.ReadAllLines(tmpOut.file.FullName);
            foreach (var l in output)
                Console.WriteLine("O> " + l);
            return r;
        }


        const string Question = "Some question";
        const string DefaultAnswer = "the default value";
        const string DefaultAnswer2 = "the other default value";
        const string OtherAnswer = "my answer";

        const string Enter = "";

        [Test]
        public void AskOneLineTextDefaultValue([Values("", "?")] string qEnd)
        {
            SetAnswers(Enter);

            string[] output;
            var txt = DoDialog(out output, cut => cut.AskOneLineText(DefaultAnswer, Question + qEnd, DialogConfirmation.None));

            Assert.That(txt, Is.EqualTo(DefaultAnswer));
            var outputOneLine = string.Join("\n", output);
            Assert.That(outputOneLine, Contains.Substring(Question).And.Contain(DefaultAnswer));
            Assert.That(outputOneLine.Contains("??"), Is.False);
        }

        [Test]
        public void AskOneLineText([Values("", " ")] string leading, [Values("", " ")] string trailing, [Values(true, false)] bool trim)
        {
            var answer = leading + OtherAnswer + trailing;
            SetAnswers(answer);

            var txt = DoDialog(cut => 
            {
                cut.TrimSpacesFromAnswer = trim;
                return cut.AskOneLineText(DefaultAnswer, Question, DialogConfirmation.None); 
            });

            Assert.That(txt, Is.EqualTo(trim ? OtherAnswer : answer));
        }

        static readonly string[] AllYes = new string[] { Enter, "y", "Y" };
        static readonly string[] AllNo = new string[] { "n", "no", "N", "NO" };

        static TestCaseData[] ConfirmYesData
        {
            get { return AllYes.Select(a => new TestCaseData(a)).ToArray(); }
        }

        static TestCaseData[] ConfirmNoYesData
        {
            get { return AllNo.SelectMany(n => AllYes.Select(y => new TestCaseData(n, y))).ToArray(); }
        }

        [Test]
        [TestCaseSource(nameof(ConfirmYesData))]
        public void AskOneLineTextWithConfirmYes(string yes)
        {
            SetAnswers(OtherAnswer, yes);

            string[] output;
            var txt = DoDialog(out output, cut => cut.AskOneLineText(DefaultAnswer, Question, DialogConfirmation.ConfirmationDefaultYes));

            Assert.That(txt, Is.EqualTo(OtherAnswer));
            var outputOneLine = string.Join("\n", output);
            Assert.That(outputOneLine, Contains.Substring("Is '" + OtherAnswer + "' correct? [y]"));
        }

        [Test]
        [TestCaseSource(nameof(ConfirmNoYesData))]
        public void AskOneLineTextWithConfirmNoYes(string no, string yes)
        {
            var secondAnswer = "some other answer";
            SetAnswers(OtherAnswer, no, secondAnswer, yes);

            string[] output;
            var txt = DoDialog(out output, cut => cut.AskOneLineText(DefaultAnswer, Question, DialogConfirmation.ConfirmationDefaultYes));

            Assert.That(txt, Is.EqualTo(secondAnswer));
        }

        [Test]
        public void MultiValueSeparator()
        {
            var cut = new TerminalDialog();
            Assert.That(cut.MultiValueSeparator, Is.EqualTo(','));
        }

        [Test]
        public void TrimSpacesFromAnswer()
        {
            var cut = new TerminalDialog();
            Assert.That(cut.TrimSpacesFromAnswer, Is.False);
        }

        [Test]
        public void AskOneLineMultiEntryTextDefaultValue([Values("", "?")] string qEnd, [Values('.', ';')] char sep, [Values(true, false)] bool trim)
        {
            SetAnswers(Enter);

            string[] output;
            var txt = DoDialog(out output, cut =>
            {
                cut.MultiValueSeparator = sep;
                cut.TrimSpacesFromAnswer = trim;
                return cut.AskOneLineMultiEntryText(new string[] { DefaultAnswer, DefaultAnswer2 }, Question + qEnd, DialogConfirmation.None); 
            });

            Assert.That(txt.Length, Is.EqualTo(2));
            Assert.That(txt[0], Is.EqualTo(DefaultAnswer));
            Assert.That(txt[1], Is.EqualTo(DefaultAnswer2));

            var outputOneLine = string.Join("\n", output);
            Assert.That(outputOneLine, Contains.Substring(Question).And.Contain(DefaultAnswer + sep + (trim ? " " : "") + DefaultAnswer2));
            Assert.That(outputOneLine, Contains.Substring("separated by '" + sep + "'"));
            Assert.That(outputOneLine.Contains("??"), Is.False);
        }

        [Test]
        public void AskOneLineMultiEntryText([Values("", " ")] string leading, [Values("", " ")] string trailing, [Values(true, false)] bool trim)
        {
            var a1 = "answer 1";
            var a1Pad = leading + a1 + trailing;
            var a2 = "answer 2";
            var a2Pad = leading + a2 + trailing;
            SetAnswers(a1Pad + "," + a2Pad);

            string[] output;
            var txt = DoDialog(out output, cut =>
            {
                cut.TrimSpacesFromAnswer = trim;
                return cut.AskOneLineMultiEntryText(new string[] { DefaultAnswer }, Question, DialogConfirmation.None);
            });

            Assert.That(txt.Length, Is.EqualTo(2));
            Assert.That(txt[0], Is.EqualTo(trim ? a1 : a1Pad));
            Assert.That(txt[1], Is.EqualTo(trim ? a2 : a2Pad));
        }

        [Test]
        [TestCaseSource(nameof(ConfirmYesData))]
        public void AskOneLineMultiEntryTextWithConfirmYes(string yes)
        {
            SetAnswers(OtherAnswer, yes);

            string[] output;
            var txt = DoDialog(out output, cut => cut.AskOneLineMultiEntryText(new string[] { DefaultAnswer }, Question, DialogConfirmation.ConfirmationDefaultYes));

            Assert.That(txt.Length, Is.EqualTo(1));
            Assert.That(txt[0], Is.EqualTo(OtherAnswer));
            var outputOneLine = string.Join("\n", output);
            Assert.That(outputOneLine, Contains.Substring("Is '" + OtherAnswer + "' correct? [y]"));
        }

        [Test]
        [TestCaseSource(nameof(ConfirmNoYesData))]
        public void AskOneLineMultiEntryTextWithConfirmNoYes(string no, string yes)
        {
            var secondAnswer = "some other answer";
            SetAnswers(OtherAnswer, no, secondAnswer, yes);

            string[] output;
            var txt = DoDialog(out output, cut => cut.AskOneLineMultiEntryText(new string[] { DefaultAnswer }, Question, DialogConfirmation.ConfirmationDefaultYes));

            Assert.That(txt.Length, Is.EqualTo(1));
            Assert.That(txt[0], Is.EqualTo(secondAnswer));
        }

    }
}
