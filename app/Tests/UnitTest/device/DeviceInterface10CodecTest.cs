﻿using System;
using System.Linq;
using CartReader;
using CartReader.Device;
using NUnit.Framework;

namespace Tests.UnitTest.device
{
    [TestFixture]
    class DeviceInterface10CodecTest
    {
        DeviceInterface10Codec codec;

        [SetUp]
        public void Init()
        {
            codec = new DeviceInterface10Codec();
        }

        [Test]
        public void EncodeU32()
        {
            var e = codec.Encode(0x12345678);
            Assert.That(e.Hex(), Is.EqualTo("0x12345678"));
        }

        [Test]
        public void EncodeU16()
        {
            var e = codec.Encode((ushort)0x1234);
            Assert.That(e.Hex(), Is.EqualTo("0x1234"));
        }

        [Test]
        public void DecodeDeviceInfo10()
        {

            var e = codec.DecodeDeviceInfo(new byte[] 
            { 
                2, 0x12, 0x34,

                20,
                0x01, 0x02, 0x03, 0x04,
                0x11, 0x12, 0x13, 0x14,
                0x21, 0x22, 0x23, 0x24,
                0x31, 0x32, 0x33, 0x34,
                0x41, 0x42, 0x43, 0x44,

                0x00
            });

            Assert.That(e.bufferSize, Is.EqualTo(0x1234));
            Assert.That(e.gitCommit.Hex(""), Is.EqualTo("0102030411121314212223243132333441424344"));
            Assert.That(e.unknownEntries.Length, Is.EqualTo(0));
        }

        [Test]
        public void DecodeDeviceInfo10Unknown()
        {
            Func<int, byte[]> dummy = l => (new byte[] { (byte)l }).Concat(new byte[l]).ToArray();

            var e = codec.DecodeDeviceInfo(
                dummy(2)
                .Concat(dummy(20))
                .Concat(new byte[] { 3, 0x11, 0x22, 0x33 })
                .Concat(new byte[] { 1, 0xaa })
                .Concat(new byte[] { 0 })
                .Concat(new byte[] { 1, 0xaa }) // should be ignored
                .ToArray());

            Assert.That(e.unknownEntries.Length, Is.EqualTo(2));
            Assert.That(e.unknownEntries[0].Hex(""), Is.EqualTo("112233"));
            Assert.That(e.unknownEntries[1].Hex(""), Is.EqualTo("aa"));
        }

        [Test]
        public void Nice()
        {
            var nfo = new DeviceInfo10()
            {
                bufferSize = 12345,
                gitCommit = new byte[20],
                unknownEntries = new byte[][]
                {
                    new byte[]{ 0x12, 0x34 },
                    new byte[]{ 0xab },
                },
            };

            var nice = "";
            codec.Nice(l => nice += l + "\n", nfo);

            Console.WriteLine(nice);

            Assert.That(nice, Contains.Substring("12345").And.Contains("buffer"));
            Assert.That(nice, Contains.Substring("000000000000").And.Contains("git"));
            Assert.That(nice, Contains.Substring("0x1234"));
            Assert.That(nice, Contains.Substring("0xab"));
        }
    }
}
