﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CartReader;
using CartReader.Device;
using CartReader.Usb;
using NSubstitute;
using NUnit.Framework;

namespace Tests.UnitTest.device
{
    [TestFixture]
    class LowLevelComTest
    {

        class UsbIf : IUsbInterface
        {
            public abstract class Tr
            {
                public byte ep;
                public TimeSpan tmo;
            }

            public class InTr : Tr { }

            public class OutTr : Tr
            {
                public byte[] data;
            }

            public readonly ArgStacker<InTr,byte[]> BulkInTransferCalls = new ArgStacker<InTr, byte[]>();

            public byte[] BulkInTransfer(byte endpoint, TimeSpan timeout)
            {
                return BulkInTransferCalls.Call(new InTr() { ep = endpoint, tmo = timeout });
            }

            public readonly ArgStacker<OutTr, int> BulkOutTransferCalls = new ArgStacker<OutTr, int>();

            public int BulkOutTransfer(byte endpoint, byte[] data, TimeSpan timeout)
            {
                if (BulkOutTransferCalls.ReturnAvailable == false)
                    BulkOutTransferCalls.ReturnNext(data.Length);

                return BulkOutTransferCalls.Call(new OutTr() { ep = endpoint, tmo = timeout, data = data.ToArray() });
            }

            #region unimplemented

            public UsbDeviceInterfaceInfo Info
            {
                get { throw new NotImplementedException(); }
            }

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            #endregion
        }

        const byte EpCmd = DeviceInterface10Consts.EndPointCommand;
        const byte EpData = DeviceInterface10Consts.EndPointData;

        LowLevelCom cut;
        UsbIf usb;



        [SetUp]
        public void Init()
        {
            usb = new UsbIf();
        }

        [Test]
        public void Version()
        {
            cut = new LowLevelCom(usb);

            Assert.That(cut.VersionMajor, Is.EqualTo(1));
            Assert.That(cut.VersionMinor, Is.EqualTo(0));
        }



        [Test]
        public void DoCommand()
        {
            usb.BulkInTransferCalls.ReturnNext(new byte[] { DeviceInterface10Consts.StatusOk });
            cut = new LowLevelCom(usb);

            cut.DoCommand(0x42);

            var o = usb.BulkOutTransferCalls.TakeNext();
            Assert.That(o.ep, Is.EqualTo(EpCmd));
            Assert.That(o.data.Hex(), Is.EqualTo("0x42"));
            Assert.That(usb.BulkInTransferCalls.TakeNext().ep, Is.EqualTo(EpCmd));
        }

        [Test]
        public void DoCommandArgs()
        {
            usb.BulkInTransferCalls.ReturnNext(new byte[] { DeviceInterface10Consts.StatusOk });
            cut = new LowLevelCom(usb);

            var args = new byte[] { 0x01, 0x02 };
            cut.DoCommand(0x42, args);

            Assert.That(usb.BulkOutTransferCalls.TakeNext().data.Hex(), Is.EqualTo("0x420102"));
        }


        [Test]
        public void DoCommandOverLongArgs()
        {
            cut = new LowLevelCom(usb);

            var exc = Assert.Throws<ArgumentException>(() => cut.DoCommand(33, new byte[DeviceInterface10Consts.PacketSize]));
            Console.WriteLine(exc.Message);
            Assert.That(exc.Message, Contains.Substring("command").And.Contain("" + (DeviceInterface10Consts.PacketSize + 1)));
        }

        [Test]
        public void DoCommandFail([Values(DeviceInterface10Consts.StatusFail, DeviceInterface10Consts.StatusUnknownCmd, 0x23)] int status)
        {
            usb.BulkInTransferCalls.ReturnNext(new byte[] { (byte)status });
            cut = new LowLevelCom(usb);

            var exc = Assert.Throws<IOException>(() => cut.DoCommand(0x42));
            Console.WriteLine(exc.Message);
            Assert.That(exc.Message, Contains.Substring("Command").And.Contain("fail"));
        }

        [Test]
        public void SendToBuffer()
        {
            cut = new LowLevelCom(usb);
            var data = Gen.Data(13);

            cut.SendToBuffer(data, data.Length);

            var o = usb.BulkOutTransferCalls.TakeNext();
            Assert.That(o.ep, Is.EqualTo(EpData));
            Assert.That(o.data.Hex(), Is.EqualTo(data.Hex()));
        }

        [Test]
        public void SendToBufferUnderChunk()
        {
            cut = new LowLevelCom(usb);
            var data = Gen.Data(10);

            cut.SendToBuffer(data, data.Length + 1);

            Assert.That(usb.BulkOutTransferCalls.TakeNext().data.Hex(), Is.EqualTo(data.Hex()));
            usb.BulkOutTransferCalls.NoCallsLeft();
        }

        [Test]
        public void SendToBufferOverChunk()
        {
            cut = new LowLevelCom(usb);
            var data = Gen.Data(10);
            var cs = 6;

            cut.SendToBuffer(data, cs);

            var c1 = usb.BulkOutTransferCalls.TakeNext();
            var c2 = usb.BulkOutTransferCalls.TakeNext();
            usb.BulkOutTransferCalls.NoCallsLeft();

            Assert.That(c1.data.Hex(), Is.EqualTo(data.Take(cs).ToArray().Hex()));
            Assert.That(c2.data.Hex(), Is.EqualTo(data.Skip(cs).ToArray().Hex()));
        }

        [Test]
        public void ReadFromBuffer()
        {
            cut = new LowLevelCom(usb);
            var data = Gen.Data(10);
            usb.BulkInTransferCalls.ReturnNext(data);

            var rx = cut.ReadFromBuffer(data.Length);

            Assert.That(rx.Hex(), Is.EqualTo(data.Hex()));
            Assert.That(usb.BulkInTransferCalls.TakeNext().ep, Is.EqualTo(EpData));
            usb.BulkInTransferCalls.NoCallsLeft();
        }

        [Test]
        public void ReadFromBufferChunks()
        {
            cut = new LowLevelCom(usb);
            var data1 = Gen.Data(1);
            var data2 = Gen.Data(5);
            var data3 = Gen.Data(30);

            usb.BulkInTransferCalls.ReturnNext(data1);
            usb.BulkInTransferCalls.ReturnNext(data2);
            usb.BulkInTransferCalls.ReturnNext(data3);

            var rx = cut.ReadFromBuffer(data1.Length + data2.Length + data3.Length);

            Assert.That(rx.Hex(), Is.EqualTo(data1.Hex() + data2.Hex("") + data3.Hex("")));
        }

        [Test]
        public void ReadFromBufferChunksOver()
        {
            cut = new LowLevelCom(usb);
            var data1 = Gen.Data(3);
            var data2 = Gen.Data(3);

            usb.BulkInTransferCalls.ReturnNext(data1);
            usb.BulkInTransferCalls.ReturnNext(data2);

            var rx = cut.ReadFromBuffer(4);

            Assert.That(rx.Hex(), Is.EqualTo(data1.Hex() + data2[0].Hex("")));
        }
    }
}
