﻿using System;
using System.Linq;
using CartReader;
using CartReader.Device;
using NUnit.Framework;

namespace Tests.UnitTest.device
{
    [TestFixture]
    class DeviceInterface10Test
    {
        class Low : ILowLevelCom
        {
            public class Cmd
            {
                public byte cmd;
                public byte[] data;
            }

            public ArgStacker<Cmd, int> DoCommandCalls = new ArgStacker<Cmd, int>();

            public void DoCommand(byte command, byte[] argumets = null)
            {
                DoCommandCalls.ReturnNext(0);
                DoCommandCalls.Call(new Cmd() { cmd = command, data = argumets });
            }

            public ArgStacker<int, byte[]> ReadFromBufferCalls = new ArgStacker<int, byte[]>();

            public byte[] ReadFromBuffer(int len)
            {
                return ReadFromBufferCalls.Call(len);
            }

            public ArgStacker<byte[], int> SendToBufferCalls = new ArgStacker<byte[], int>();

            public void SendToBuffer(byte[] data, int chunksize)
            {
                Assert.That(chunksize, Is.EqualTo(DeviceInterface10Consts.PacketSize));
                SendToBufferCalls.ReturnNext(0);
                SendToBufferCalls.Call(data);
            }

            #region unimplemented

            public byte VersionMajor
            {
                get { throw new NotImplementedException(); }
            }

            public byte VersionMinor
            {
                get { throw new NotImplementedException(); }
            }

            #endregion
        }

        DeviceInterface10 cut;
        Low ll;



        [SetUp]
        public void Init()
        {
            ll = new Low();
        }

        const ushort BufferSize = 0x12ac;

        DeviceInterface10 NewCut()
        {
            ll.ReadFromBufferCalls.ReturnNext(new byte[]
            {
                2, (byte)(BufferSize >> 8), (byte)(BufferSize & 0xff),
                20,
            }.Concat(new byte[21]).ToArray());

            var nCut = new DeviceInterface10(ll);

            var cmd = ll.DoCommandCalls.TakeNext();
            Assert.That(cmd.cmd, Is.EqualTo(DeviceInterface10Consts.CommandDeviceInfo));
            ll.DoCommandCalls.NoCallsLeft();
            Assert.That(nCut.Info.bufferSize, Is.EqualTo(BufferSize));
            Assert.That(nCut.Info.gitCommit.Hex(), Is.EqualTo(new byte[20].Hex()));
            Assert.That(ll.ReadFromBufferCalls.TakeNext(), Is.EqualTo(DeviceInterface10Consts.PacketSize));
            ll.ReadFromBufferCalls.NoCallsLeft();
            return nCut;
        }

        [Test]
        public void DeviceInfo()
        {
            cut = NewCut();
        }

        [Test]
        public void ReadFromBuffer()
        {
            cut = NewCut();
            var len = 42;
            ll.ReadFromBufferCalls.ReturnNext(new byte[len]);

            var data = cut.ReadFromBuffer(len);

            Assert.That(data.Length, Is.EqualTo(len));
            Assert.That(ll.ReadFromBufferCalls.TakeNext(), Is.EqualTo(len));
        }

        [Test]
        public void ReadFromBufferOor([Values(-1, 0, BufferSize+1 )] int len)
        {
            cut = NewCut();

            var exc = Assert.Throws<ArgumentOutOfRangeException>(() => cut.ReadFromBuffer(len));
            Console.WriteLine(exc.Message);
            Assert.That(exc.Message, Contains.Substring("" + len));
        }

        [Test]
        public void SendToBuffer()
        {
            cut = NewCut();
            var len = 42;

            cut.SendToBuffer(new byte[len]);

            Assert.That(ll.SendToBufferCalls.TakeNext().Length, Is.EqualTo(len));
        }

        [Test]
        public void SendToBufferOor([Values(-1, 0, BufferSize+1)] int len)
        {
            cut = NewCut();

            if (len < 0)
            {
                Assert.Throws<ArgumentNullException>(() => cut.SendToBuffer(null));
            }
            else
            {
                var exc = Assert.Throws<ArgumentOutOfRangeException>(() => cut.SendToBuffer(new byte[len]));
                Console.WriteLine(exc.Message);
                Assert.That(exc.Message, Contains.Substring("" + len));
            }
        }

        [Test]
        public void CommandBufferRead()
        {
            DoCommandTest(DeviceInterface10Consts.CommandBufferRead, i => i.CommandBufferRead());
        }

        [Test]
        public void CommandBufferWrite()
        {
            DoCommandTest(DeviceInterface10Consts.CommandBufferWrite, i => i.CommandBufferWrite());
        }

        [Test]
        public void CommandBusRead()
        {
            DoCommandTest(DeviceInterface10Consts.CommandBusRead, i => i.CommandBusRead(0x11223344, 0x10, 0x20), p =>
            {
                Assert.That(p.Hex(), Is.EqualTo("0x1122334400100020")); 
            });
        }

        [Test]
        public void CommandBusReadOor([Values(0, BufferSize+2)] int len)
        {
            cut = NewCut();

            var words = (ushort)(len / 2);
            var exc = Assert.Throws<ArgumentOutOfRangeException>(() => cut.CommandBusRead(0, words, 1));
            Assert.That(exc.Message, Contains.Substring("" + words));
        }

        [Test]
        public void CommandBusReadOorStride()
        {
            cut = NewCut();

            var exc = Assert.Throws<ArgumentOutOfRangeException>(() => cut.CommandBusRead(0, 2, 0));
            Assert.That(exc.Message, Contains.Substring("stride"));
        }

        [Test]
        public void CommandBusWrite()
        {
            DoCommandTest(DeviceInterface10Consts.CommandBusWrite, i => i.CommandBusWrite(0x11223344, 0x10, 0x20), p =>
            {
                Assert.That(p.Hex(), Is.EqualTo("0x1122334400100020"));
            });
        }

        [Test]
        public void CommandBusWriteOor([Values(0, BufferSize + 2)] int len)
        {
            cut = NewCut();

            var words = (ushort)(len / 2);
            var exc = Assert.Throws<ArgumentOutOfRangeException>(() => cut.CommandBusWrite(0, words, 1));
            Assert.That(exc.Message, Contains.Substring("" + words));
        }

        [Test]
        public void CommandBusWriteOorStride()
        {
            cut = NewCut();

            var exc = Assert.Throws<ArgumentOutOfRangeException>(() => cut.CommandBusWrite(0, 2, 0));
            Assert.That(exc.Message, Contains.Substring("stride"));
        }

        [Test]
        public void CommandReadEeprom()
        {
            DoCommandTest(DeviceInterface10Consts.CommandReadEeprom, i => i.CommandReadEeprom(0x42), p =>
            {
                Assert.That(p.Hex(), Is.EqualTo("0x42"));
            });
        }

        void DoCommandTest(byte cmd, Action<DeviceInterface10> doIt)
        {
            DoCommandTest(cmd, doIt, p => Assert.That(p, Is.Null));
        }

        void DoCommandTest(byte cmd, Action<DeviceInterface10> doIt, Action<byte[]> assertPayload)
        {
            cut = NewCut();

            doIt(cut);

            var c = ll.DoCommandCalls.TakeNext();
            Assert.That(c.cmd, Is.EqualTo(cmd));
            assertPayload(c.data);
        }
    }
}
