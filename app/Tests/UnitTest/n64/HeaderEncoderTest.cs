﻿using System;
using CartReader.N64;
using NUnit.Framework;

namespace Tests.UnitTest.n64
{
    [TestFixture]
    class HeaderEncoderTest
    {
        [Test]
        [TestCase(1, 0, 0)]
        [TestCase(0, 0, 0)]
        [TestCase(1, 1, 1)]
        [TestCase(2, 0, 16)]
        public void HeaderVersion(byte major, byte minor, byte raw)
        {
            var encoder = new HeaderEncoder();
            var encoded = encoder.Encode(new Header.Version() { major = major, minor = minor });
            Assert.That(encoded, Is.EqualTo(raw));
        }
    }
}
