﻿using System;
using System.Linq;
using CartReader;
using CartReader.N64;
using NUnit.Framework;

namespace Tests.UnitTest.n64
{
    [TestFixture]
    public class HeaderDecoderTest
    {
        [Test]
        public void ToRegionString()
        {
            var dec = new HeaderDecoder();
            Assert.That(dec.ToRegionString(0x4A, RegionConvention.General), Is.EqualTo("Japanese"));
            Assert.That(dec.ToRegionString(0x4A, RegionConvention.NoIntro), Is.EqualTo("Japan"));
        }

        [Test]
        public void ToRegionStringUnknown([Values(RegionConvention.General, RegionConvention.NoIntro)] RegionConvention c)
        {
            var dec = new HeaderDecoder();
            Assert.That(dec.ToRegionString(0x00, c), Is.EqualTo(HeaderDecoder.UnknownRegion));
        }

        [Test]
        public void ToMediaTypeString()
        {
            var dec = new HeaderDecoder();
            Assert.That(dec.ToMediaTypeString(0x4e), Is.EqualTo("Cart"));
        }

        [Test]
        public void ToMediaTypeStringUnknown()
        {
            var dec = new HeaderDecoder();
            Assert.That(dec.ToMediaTypeString(0x00), Is.EqualTo(HeaderDecoder.UnknownType));
        }

        static readonly byte[] HeaderBig = new byte[] 
        {
            0x80, 0xab, 0xcd, 0xef, 0x00, 0x00, 0x00, 0x0f, 0x80, 0x00, 0x04, 0x00,
            0x00, 0x00, 0x12, 0x34, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x53, 0x4f, 0x4d, 0x45,
            0x20, 0x47, 0x41, 0x4d, 0x45, 0x20, 0x54, 0x49, 0x54, 0x4c, 0x45, 0x20,
            0x20, 0x20, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4e,
            0x54, 0x54, 0x50, 0x12
        };

        [Test]
        public void DecodeHeaderBig()
        {
            var dec = new HeaderDecoder();
            var header = dec.Decode(HeaderBig);

            dec.PrintInfo(Console.WriteLine, header);
            Assert.That(header.endianessIndicator, Is.EqualTo(0x80));
            Assert.That(header.pi.piBsdDom1Rls, Is.EqualTo(0xa));
            Assert.That(header.pi.piBsdDom1Pgs, Is.EqualTo(0xb));
            Assert.That(header.pi.piBsdDom1Pwd, Is.EqualTo(0xcd));
            Assert.That(header.pi.piBsdDom1Lat, Is.EqualTo(0xef));
            Assert.That(header.clockRateOverride, Is.EqualTo(0x0000000f));
            Assert.That(header.bootAddress, Is.EqualTo(0x80000400));
            Assert.That(header.release, Is.EqualTo(0x00001234));
            Assert.That(header.crc1, Is.EqualTo(0x11223344));
            Assert.That(header.crc2, Is.EqualTo(0x55667788));
            Assert.That(header.name, Is.EqualTo("SOME GAME TITLE     "));
            Assert.That(header.mediaFormat.type, Is.EqualTo(0x4e));
            Assert.That(header.mediaFormat.id, Is.EqualTo("TT"));
            Assert.That(header.mediaFormat.region, Is.EqualTo(0x50));
            Assert.That(header.version.major, Is.EqualTo(2));
            Assert.That(header.version.minor, Is.EqualTo(2));
        }

        static byte[] Copy(byte[] d)
        {
            var cpy = new byte[d.Length];
            Array.Copy(d, cpy, cpy.Length);
            return cpy;
        }

        [Test]
        public void DecodeHeaderBigZeroPadName()
        {
            const string UnpaddedName = "SOME GAME TITLE";
            // Duplicate / Copy test data and replace the 0x20 spaces with 0x00
            var headerBigZero = Copy(HeaderBig);
            for (int i = 0; i < 5; ++i)
                headerBigZero[i + 0x20 + UnpaddedName.Length] = 0x00;

            Console.WriteLine(headerBigZero.Hex());

            var dec = new HeaderDecoder();
            var header = dec.Decode(headerBigZero);

            dec.PrintInfo(Console.WriteLine, header);

            Assert.That(header.name, Is.EqualTo(UnpaddedName));
        }
    }
}
