﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CartReader;
using CartReader.Device;
using CartReader.N64;
using NUnit.Framework;
using Tests.DeviceEmulation;

namespace Tests.UnitTest.n64
{
    [TestFixture]
    class CartDumperTest
    {
        UsbReaderDevice dev;
        CartDumper cut;
        TempFile tmp;

        [SetUp]
        public void Init()
        {
            dev = new UsbReaderDevice(1024);
            cut = new CartDumper(dev);
            tmp = new TempFile();
        }

        [TearDown]
        public void CleanUp()
        {
            tmp.Dispose();
        }

        const uint CustomBase = 0x3000000;

        [Test]
        public void DumpHeader()
        {
            var header = Gen.Data(N64Consts.RomHeaderSize);
            dev.Add(new RomDevice(CustomBase, header));

            var h = cut.DumpHeader(CustomBase);

            Assert.That(h.Hex(), Is.EqualTo(header.Hex()));
        }

        [Test]
        public void DumpRomToBigEndianFile()
        {
            var romData = Gen.Data(1024 * 1024 / 8); // 1Mbit
            dev.Add(new RomDevice(CustomBase, romData));

            cut.DumpRomToBigEndianFile(tmp.file, CustomBase, (uint)romData.Length);

            var dumped = File.ReadAllBytes(tmp.file.FullName);
            Assert.That(dumped, Is.EqualTo(romData));
        }

        [Test]
        public void DumpSRamToBigEndianFile()
        {
            var romData = Gen.Data(N64Consts.DefaultSRamSizeKbyte * 1024);
            dev.Add(new SRamDevice(CustomBase, romData));

            cut.DumpSRamToBigEndianFile(tmp.file, CustomBase, (uint)romData.Length);

            var dumped = File.ReadAllBytes(tmp.file.FullName);
            Assert.That(dumped, Is.EqualTo(romData));
        }

        const uint UnknownIdHeader = 0x11118001;

        static TestCaseData[] FramConfigs
        {
            get
            {
                // https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3283
                // https://github.com/mupen64plus/mupen64plus-core/blob/db0f7365f2c2b8bbae523b2503e660d09d4a2a85/src/device/cart/flashram.h#L32

                Func<FramInfo, FRamDeviceConfig[]> toCfg = nfo => 
                {
                    return nfo.ids.Select(id => new FRamDeviceConfig()
                    {
                        nfo = nfo,
                        id = new FramId() { unknown = UnknownIdHeader, id = id },
                    }).ToArray();
                };


                return N64Consts.KnownFramTypes
                    .SelectMany(toCfg)
                    .Select(cfg => new TestCaseData(cfg).SetName(string.Join("+", cfg.nfo.types) + " " + cfg.id.id.Hex()))
                    .ToArray();
            }
        }


        [Test]
        [TestCaseSource(nameof(FramConfigs))]
        public void DumpFRamToBigEndianFile(FRamDeviceConfig cfg)
        {
            var romData = Gen.Data(N64Consts.DefaultFRamSizeKbyte * 1024);
            dev.Add(new FRamDevice(cfg, CustomBase, romData));

            cut.DumpFRamToBigEndianFile(tmp.file, CustomBase, (uint)romData.Length);

            var dumped = File.ReadAllBytes(tmp.file.FullName);
            Assert.That(dumped, Is.EqualTo(romData));
        }

        [Test]
        [TestCaseSource(nameof(FramConfigs))]
        public void GetFRamId(FRamDeviceConfig cfg)
        {
            dev.Add(new FRamDevice(cfg, CustomBase, Gen.Data(N64Consts.DefaultFRamSizeKbyte * 1024)));

            var status = cut.GetFRamId(CustomBase);
            Assert.That(status.Length, Is.EqualTo(8));
            Assert.That(status.Hex(""), Is.EqualTo(cfg.id.unknown.Hex("") + cfg.id.id.Hex("")));
        }

        [Test]
        public void DumpEeprom([Values(64, 256)] int eepromWords, [Values(0.0, 0.05)] double readErrorQuota)
        {
            var eepromData = Gen.Data(eepromWords * N64Consts.EepromWordSizeBytes);
            dev.Add(new EepromDevice(eepromData, readErrorQuota));

            cut.DumpEeprom(tmp.file, (ushort)eepromWords);

            var dumped = File.ReadAllBytes(tmp.file.FullName);
            Assert.That(dumped, Is.EqualTo(eepromData));
        }
    }
}
