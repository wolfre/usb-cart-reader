﻿using System;
using CartReader;
using CartReader.N64;
using NUnit.Framework;

namespace Tests.UnitTest.n64
{
    [TestFixture]
    public class FramIdDecoderTest
    {
        [Test]
        public void DecodeWrongLength([Values(7, 9)] int l)
        {
            var dec = new FramIdDecoder();

            Assert.Throws<NotSupportedException>(() => dec.Decode(new byte[l]));
        }

        [Test]
        public void Decode()
        {
            var dec = new FramIdDecoder();
            var d = dec.Decode(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            Assert.That(d.unknown, Is.EqualTo(0x01020304));
            Assert.That(d.id, Is.EqualTo(0x05060708));
        }
    }
}
