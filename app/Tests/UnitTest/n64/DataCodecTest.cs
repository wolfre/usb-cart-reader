﻿using System;
using CartReader;
using CartReader.N64;
using NUnit.Framework;

namespace Tests.UnitTest.n64
{
    [TestFixture]
    class DataCodecTest
    {
        [Test]
        public void EncodeU32()
        {
            var c = new DataCodec();
            var e = c.Encode(0x12345678);
            Assert.That(e.Hex(""), Is.EqualTo("12345678"));
        }

        [Test]
        public void DecodeU32()
        {
            var c = new DataCodec();
            var u32 = c.DecodeU32(new byte[] { 0xab, 0x11, 0x22, 0x44 });
            Assert.That(u32, Is.EqualTo(0xab112244));
        }
    }
}
