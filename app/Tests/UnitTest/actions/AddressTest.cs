﻿using System;
using CartReader.Actions;
using NUnit.Framework;

namespace Tests.UnitTest.actions
{
    [TestFixture]
    class AddressTest
    {
        static TestCaseData[] ParseCliArgCases => new TestCaseData[]
        {
            new TestCaseData("0", (uint)0),
            new TestCaseData(" 10 ", (uint)10),
            new TestCaseData("0x12345678", (uint)0x12345678),
            new TestCaseData(" 0xabcdef01 ", (uint)0xabcdef01),
            new TestCaseData("0x12", (uint)0x12),
            new TestCaseData("0x12a", (uint)0x12a),
        };

        [Test]
        [TestCaseSource(nameof(ParseCliArgCases))]
        public void ParseCliArg(string arg, uint n)
        {
            var a = Address.ParseCliArg(arg);
            Assert.That(a.value, Is.EqualTo(n));
        }

        static TestCaseData[] ParseCliArgFailCases => new TestCaseData[]
        {
            new TestCaseData("bla"),
            new TestCaseData(""),
            new TestCaseData("0xkk"),
        };

        [Test]
        [TestCaseSource(nameof(ParseCliArgFailCases))]
        public void ParseCliArgFail(string arg)
        {
            var exc = Assert.Throws<ArgumentException>(() => Address.ParseCliArg(arg));
            Assert.That(exc.Message, Contains.Substring("'" + arg + "'"));
        }

        [Test]
        public void ToStringIsHEx()
        {
            var str = (new Address() { value = 0x876543ab }).ToString();
            Assert.That(str, Is.EqualTo("0x876543ab"));
        }
    }
}
