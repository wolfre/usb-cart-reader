﻿using System;
using System.Collections.Generic;
using System.IO;
using CartReader;
using CartReader.Actions;
using CartReader.Actions.RomInfoSerializer;
using CartReader.N64;
using NUnit.Framework;

namespace Tests.UnitTest.actions.romInfoSerializer
{
    class RomInfoSerializerTest
    {
        protected class GeneratedField
        {
            public string value;
            public string name;
        }

        protected List<GeneratedField> generatedField;

        [SetUp]
        public virtual void Init()
        {
            generatedField = new List<GeneratedField>();
        }

        [TearDown]
        public virtual void Cleanup()
        {
        }

        static Random rnd = new Random(DateTime.Now.Millisecond);

        protected const string Numbers = "0123456789";
        protected const string FullAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" + Numbers;
        protected const string HexNumbers = "0123456789abcdef";

        protected string GenField(string name, int len = 10, string alphabet = FullAlphabet)
        {
            var s = "";
            while (s.Length < len)
                s += alphabet[rnd.Next(0, alphabet.Length)];
            return AddField(name, s);
        }

        protected string GenDate(string name)
        {
            var d = "20" + rnd.Next(10, 20) + "-" + rnd.Next(1, 12).ToString("D2") + "-" + rnd.Next(1, 29).ToString("D2");
            AddField(name, d);
            return d;
        }

        protected string AddField(string name, string f)
        {
            generatedField.Add(new GeneratedField() { value = f, name = name });
            return f;
        }

        protected uint AddField(string name, uint f)
        {
            AddField(name, "" + f);
            return f;
        }

        CommonFileInfo GenCommonFileInfo(string filename)
        {
            return new CommonFileInfo()
            {
                crc32 = GenField(nameof(CommonFileInfo.crc32), 8, HexNumbers),
                md5 = GenField(nameof(CommonFileInfo.md5), 32, HexNumbers),
                sha1 = GenField(nameof(CommonFileInfo.sha1), 40, HexNumbers),
                sha256 = GenField(nameof(CommonFileInfo.sha256), 64, HexNumbers),
                file = new FileInfo(AddField(nameof(CommonFileInfo.file), filename)),
                size = AddField(nameof(CommonFileInfo.size), 8 * 1024 * 1024),
            };
        }

        protected N64RomInfo GenFullRomInfo()
        {
            return new N64RomInfo()
            {
                info = new Nfo()
                {
                    dumpingToolAndVersion = GenField(nameof(Nfo.dumpingToolAndVersion), 20),
                    dumper = GenField(nameof(Nfo.dumper)),
                    affiliation = GenField(nameof(Nfo.affiliation)),
                    dumpCreationDate = GenDate(nameof(Nfo.dumpCreationDate)),
                    dumpReleaseDate = GenDate(nameof(Nfo.dumpReleaseDate)),
                    links = new string[] { GenField(nameof(Nfo.links)), GenField(nameof(Nfo.links)) },
                    title = GenField(nameof(Nfo.title), 30),
                    region = GenField(nameof(Nfo.region)),
                    edition = GenField(nameof(Nfo.edition)),
                    languages = new string[] { GenField(nameof(Nfo.languages), 2), GenField(nameof(Nfo.languages), 2) },
                    languageSelect = GenField(nameof(Nfo.languageSelect), 3),
                    wikiDataId = GenField(nameof(Nfo.wikiDataId), 15),
                    romRegion = GenField(nameof(Nfo.romRegion)),
                    romRevision = AddField(nameof(Nfo.romRevision), "0x12 (2.3)"),
                    romSerial = GenField(nameof(Nfo.romSerial)),
                    physicalMediaSerial = new string[] { GenField(nameof(Nfo.physicalMediaSerial)), GenField(nameof(Nfo.physicalMediaSerial)) },
                    physicalMediaStamp = AddField(nameof(Nfo.physicalMediaStamp), "34C"),
                    pcbSerial = GenField(nameof(Nfo.pcbSerial)),
                    romChipSerial = new string[] { GenField(nameof(Nfo.romChipSerial)), GenField(nameof(Nfo.romChipSerial)) },
                    boxSerial = GenField(nameof(Nfo.boxSerial)),
                    boxBarcode = GenField(nameof(Nfo.boxBarcode), 15, Numbers),
                },
                bigEndian = GenCommonFileInfo("rom.z64"),
                byteSwapped = GenCommonFileInfo("rom.v64"),
                // NOTE how the header is used is highly format dep.
                romHeader = new Header()
                {
                    version = new Header.Version() { major = 1, minor = 2 },
                },
            };
        }

        protected N64RomInfo GenWaveRaceNfo()
        {
            var f = "Wave Race 64 - Kawasaki Jet Ski (Europe) (En,De)";
            uint s = 8388608;

            return new N64RomInfo()
            {
                info = new Nfo()
                {
                    dumpingToolAndVersion = AddField(nameof(Nfo.dumpingToolAndVersion), "Some Tool 1.0.0"),
                    dumper = AddField(nameof(Nfo.dumper), "Mr. Dump"),
                    affiliation = AddField(nameof(Nfo.affiliation), "No-Intro"),
                    dumpCreationDate = AddField(nameof(Nfo.dumpCreationDate), "2020-12-01"),
                    dumpReleaseDate = AddField(nameof(Nfo.dumpReleaseDate), "2020-12-02"),
                    links = new string[] { AddField(nameof(Nfo.links), "https://example.com") },
                    title = AddField(nameof(Nfo.title), "Wave Race 64"),
                    region = AddField(nameof(Nfo.region), "Europe"),
                    edition = AddField(nameof(Nfo.edition), "Retail"),
                    languages = new string[] { AddField(nameof(Nfo.languages), "En"), AddField(nameof(Nfo.languages), "De") },
                    languageSelect = AddField(nameof(Nfo.languageSelect), "yes"),
                    wikiDataId = AddField(nameof(Nfo.wikiDataId), "Q3142278"),
                    romRegion = AddField(nameof(Nfo.romRegion), "Europe"),
                    romRevision = AddField(nameof(Nfo.romRevision), "0x00 (1.0)"),
                    romSerial = AddField(nameof(Nfo.romSerial), "NWRP"),
                    physicalMediaSerial = new string[] { AddField(nameof(Nfo.physicalMediaSerial), "NUS-NWRP-EUR") },
                    physicalMediaStamp = AddField(nameof(Nfo.physicalMediaStamp), "19"),
                    pcbSerial = AddField(nameof(Nfo.pcbSerial), "NUS-01A-01"),
                    romChipSerial = new string[] { AddField(nameof(Nfo.romChipSerial), "NUS-NWRP-0") },
                    boxSerial = AddField(nameof(Nfo.boxSerial), "P-NWRP-NNOE"),
                    boxBarcode = AddField(nameof(Nfo.boxBarcode), "045496870034"),
                },
                bigEndian = new CommonFileInfo()
                {
                    crc32 = AddField(nameof(CommonFileInfo.crc32), "FB289893".ToLower()),
                    md5 = AddField(nameof(CommonFileInfo.md5), "310659115E9939F219A783ABDD456CE9".ToLower()),
                    sha1 = AddField(nameof(CommonFileInfo.sha1), "C20AA97DC25AEC7A9B9C6889286BBD216FC7CAA4".ToLower()),
                    sha256 = AddField(nameof(CommonFileInfo.sha256), "8897DA0CB6E286659AC4D72C5737F6FE97A51BCDFC6E2328AE7DFC0715BF1761".ToLower()),
                    file = new FileInfo(AddField(nameof(CommonFileInfo.file), f + ".z64")),
                    size = AddField(nameof(CommonFileInfo.size), s),
                },
                byteSwapped = new CommonFileInfo()
                {
                    crc32 = AddField(nameof(CommonFileInfo.crc32), "D74117B0".ToLower()),
                    md5 = AddField(nameof(CommonFileInfo.md5), "BAFF32EE50896E62DC713E23D1A589DA".ToLower()),
                    sha1 = AddField(nameof(CommonFileInfo.sha1), "7FFDD44890B1A00E0C13868A18B482FBF8C752F7".ToLower()),
                    sha256 = AddField(nameof(CommonFileInfo.sha256), "4C07E69B289777A3ECC3719A1970BA4287737BE03BB79536CCCCE52EB7049EA0".ToLower()),
                    file = new FileInfo(AddField(nameof(CommonFileInfo.file), f + ".v64")),
                    size = AddField(nameof(CommonFileInfo.size), s),
                },
                romHeader = new Header()
                {
                    version = new Header.Version() { major = 0, minor = 0 },
                },
            };
        }


        protected static string Serialize(IRomInfoSerializer ser, N64RomInfo info)
        {
            using (var mem = new MemoryStream())
            {
                ser.Serialize(mem, info);
                return System.Text.Encoding.UTF8.GetString(mem.ToArray());
            }
        }

        protected static readonly string[] ChecksumFields = new string[]
        {
            nameof(CommonFileInfo.crc32),
            nameof(CommonFileInfo.md5),
            nameof(CommonFileInfo.sha1),
            nameof(CommonFileInfo.sha256),
        };

    }

}
