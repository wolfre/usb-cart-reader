﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CartReader;
using CartReader.Actions;
using CartReader.Actions.RomInfoSerializer;
using CartReader.NoIntro;
using NUnit.Framework;

namespace Tests.UnitTest.actions.romInfoSerializer
{

    [TestFixture]
    class InfoTextTest : RomInfoSerializerTest
    {
        [Test]
        public void SerializeAll()
        {
            var nfo = GenFullRomInfo();
            var txt = Serialize(new InfoText(false), nfo);
            Console.WriteLine(txt);

            CheckOutput(txt);

            Assert.That(txt, Contains.Substring("Media Serial 2"));
            Assert.That(txt, Contains.Substring("ROM Chip Serial 2"));
        }

        [Test]
        public void SerializeWaveRavce()
        {
            var nfo = GenWaveRaceNfo();

            var txt = Serialize(new InfoText(false), nfo);
            Console.WriteLine(txt);

            CheckOutput(txt);
        }

        void CheckOutput(string txt)
        {
            foreach (var gen in generatedField)
            {
                if (ChecksumFields.Contains(gen.name))
                {
                    Assert.That(txt, Contains.Substring(gen.value.ToUpper()), gen.name);
                }
                else
                {
                    Assert.That(txt, Contains.Substring(gen.value), gen.name);
                }
            }

            Assert.That(txt, Contains.Substring("Title: "));
            Assert.That(txt, Contains.Substring("Original format: " + CartReader.NoIntro.NoIntroConsts.FormatBigEndian));
            Assert.That(txt, Contains.Substring(CartReader.NoIntro.NoIntroConsts.FormatBigEndian));
            Assert.That(txt, Contains.Substring(CartReader.NoIntro.NoIntroConsts.FormatByteSwapped));
            Assert.That(txt, Contains.Substring("Media Serial 1"));
            Assert.That(txt, Contains.Substring("ROM Chip Serial 1"));
        }

        [Test]
        public void SerializeOmit([Values("", Misc.NotAvailable, Misc.Unknown)] string absent)
        {
            var nfo = GenFullRomInfo();
            nfo.info.title = absent;


            var txt = Serialize(new InfoText(true), nfo);
            Console.WriteLine(txt);

            Assert.That(txt.Contains("Title: "), Is.False);
        }
    }
}
