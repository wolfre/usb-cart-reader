﻿using System;
using System.IO;
using System.Linq;
using CartReader;

namespace Tests
{
    public class Executable
    {
        public FileInfo Exe { get; private set; }

        public Executable(string exe)
        {
            this.Exe = new FileInfo(exe);
            if (this.Exe.Exists == false)
            {
                this.Exe = OsSpecifics.Path.Select(p => p.File(exe)).FirstOrDefault(ex => ex.Exists);
                if (this.Exe == null)
                    throw new FileNotFoundException(exe);
            }
        }

        public string Run(params string[] args)
        {
            var nfo = new System.Diagnostics.ProcessStartInfo()
            {
                Arguments = OsSpecifics.BuildArgs(args),
                CreateNoWindow = true,
                ErrorDialog = false,
                FileName = Exe.FullName,

                RedirectStandardError = false,
                RedirectStandardInput = false,
                RedirectStandardOutput = true,

                UseShellExecute = false,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
            };

            var p = System.Diagnostics.Process.Start(nfo);

            var output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            if (p.ExitCode != 0)
                throw new Exception(Exe.FullName + " quit with " + p.ExitCode);

            return output;
        }

    }
}
