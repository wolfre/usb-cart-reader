﻿using System;
using System.IO;

namespace Tests
{
    class TempFile : IDisposable
    {
        public readonly FileInfo file;

        public TempFile()
        {
            file = new FileInfo(Path.GetTempFileName());
            Delete();
            Console.WriteLine("Temp file is: '" + file.FullName + "'");
        }

        void Delete()
        {
            file.Refresh();
            if (file.Exists)
                file.Delete();
        }

        public void Dispose()
        {
            Delete();
        }
    }
}
