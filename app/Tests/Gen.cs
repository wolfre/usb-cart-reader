﻿using System;
namespace Tests
{
    static class Gen
    {
        public static byte[] Data(int len)
        {
            var d = new byte[len];
            for (int i = 0; i < d.Length; ++i)
                d[i] = (byte)((i + 7) & 0xff);
            return d;
        }
    }
}
