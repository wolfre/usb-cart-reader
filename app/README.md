# The PC host application

The host application is a .NET 4.5 application and should run on anything from [Windows XP](https://www.microsoft.com/de-de/download/details.aspx?id=42642) to [Windows 10](https://docs.microsoft.com/en-us/dotnet/framework/migration-guide/versions-and-dependencies).
Using the brilliant [mono](https://www.mono-project.com/) runtime any current Linux (AMD64/ARM/ARM64) or Mac is also supported.
The only platform dependent part of the app is the interface to the USB stack.
Currently this only supports libusb 1.0, which is primarily Linux, but there are ports for [Windows](https://github.com/mcuee/libusb-win32) available.
Supporting the native Windows USB stack is high on the priority list however, and should be coming soon.

The application consist roughly of these parts:
- The [Actions](CartReader/Actions) folder contains the command line interface.
- The [N64](CartReader/N64) folder contains all the N64 constants, cartridge header decoder and FRAM chip related things.
  It also includes the dumping logic.
- The [Device](CartReader/Device) folder contains the comunication with the USB cart reader (including selftest), building on a USB stack.
- The [Usb](CartReader/Usb) folder contains an abstraction layer to the USB stack (currently only libusb).
- The [Interactive](CartReader/Interactive) folder contains some code for interactive dialog with a user, used for the rom info file creation.

Besides the application (production code), there's also an NUnit 3 based [(unit) test project](Tests).

## Build it

Generally you can just load it in either Visual Studio, or [MonoDevelop](https://www.monodevelop.com/) and hack away!
Besides a .NET runtime, there are two more [dependencies](CartReader/packages.config) needed for the production side:
- [Binah](https://gitlab.com/wolfre/binah) which does the command line handling.
  NuGet packages are available [here](https://gitlab.com/wolfre/binah/-/releases).
- [Crc32.NET](https://www.nuget.org/packages/Crc32.NET/) for CRC32 calculation of dumps.


To build the Tests you need the respective [NUnit 3 package](Tests/packages.config).
To run them you need an NUnit 3 compatible runner, MonoDevelop has one build-in, or you can use [this one](https://www.nuget.org/packages/NUnit.ConsoleRunner).
