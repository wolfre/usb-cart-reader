﻿using System;
using System.IO;

namespace CartReader
{
    class CommonFileInfo
    {
        public FileInfo file;
        public uint size;
        public string crc32;
        public string md5;
        public string sha1;
        public string sha256;
    }

    static class CommonFileInfoGenerator
    {
        public static void CalcFileInfo(out CommonFileInfo bigEndian, out CommonFileInfo byteSwapped, FileInfo rom)
        {
            bigEndian = CommonFileInfoGenerator.CalcFileInfo(rom, false);
            byteSwapped = CommonFileInfoGenerator.CalcFileInfo(rom, true);


            byteSwapped.file = bigEndian.file.SwapExtension(NoIntro.NoIntroConsts.ExtensionByteSwapped);
        }

        public static CommonFileInfo CalcFileInfo(FileInfo rom, bool swapBytes)
        {
            const long MaxSize = 1024 * 1024 * 256;
            rom.Refresh();
            if (rom.Length > MaxSize)
                throw new NotSupportedException("Rom '" + rom.FullName + "' is larger than " + MaxSize);

            var buffer = Slurp(rom);

            if (swapBytes)
            {
                for (int i = 0; i < buffer.Length; i += 2)
                {
                    var h = buffer[i];
                    buffer[i] = buffer[i + 1];
                    buffer[i + 1] = h;
                }
            }

            using (var crc32 = new Force.Crc32.Crc32Algorithm())
            using (var md5 = System.Security.Cryptography.MD5.Create())
            using (var sha1 = System.Security.Cryptography.SHA1.Create())
            using (var sha256 = System.Security.Cryptography.SHA256.Create())
            {
                return new CommonFileInfo()
                {
                    file = rom,
                    size = (uint)buffer.Length,
                    crc32 = crc32.ComputeHash(buffer).Hex("").ToLower(),
                    md5 = md5.ComputeHash(buffer).Hex("").ToLower(),
                    sha1 = sha1.ComputeHash(buffer).Hex("").ToLower(),
                    sha256 = sha256.ComputeHash(buffer).Hex("").ToLower(),
                };
            }
        }

        static byte[] Slurp(FileInfo rom)
        {
            return File.ReadAllBytes(rom.FullName);
        }
    }
}
