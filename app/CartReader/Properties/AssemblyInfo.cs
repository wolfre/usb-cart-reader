﻿using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle(CartReader.About.LongName)]
[assembly: AssemblyDescription(CartReader.About.Info)]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct(CartReader.About.LongName)]
[assembly: AssemblyCopyright(CartReader.About.Copyright)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion(BuildInfo.VersionMajor + "." + BuildInfo.VersionMinor + "." + BuildInfo.VersionPatch + ".0")]
[assembly: AssemblyFileVersion(BuildInfo.VersionMajor + "." + BuildInfo.VersionMinor + "." + BuildInfo.VersionPatch + ".0")]
[assembly: AssemblyInformationalVersion(BuildInfo.VersionSemantic)]

// The following attributes are used to specify the signing key for the assembly, 
// if desired. See the Mono documentation for more information about signing.

//[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile("")]
[assembly: InternalsVisibleTo("Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]