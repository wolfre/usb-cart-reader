﻿using System;
using System.Runtime.InteropServices;

namespace CartReader.Usb
{
    class IntPtrPtr : IDisposable
    {
        readonly NativeBuffer buffer;

        public IntPtrPtr()
        {
            buffer = new NativeBuffer(IntPtr.Size);
        }

        public void Dispose()
        {
            buffer.Dispose();
        }

        public IntPtr Value
        {
            get
            {
                if (buffer.Buffer.Length == 4)
                {
                    int address = BitConverter.ToInt32(buffer.Buffer, 0);
                    return new IntPtr(address);
                }

                if (buffer.Buffer.Length == 8)
                {
                    long address = BitConverter.ToInt64(buffer.Buffer, 0);
                    return new IntPtr(address);
                }

                throw new NotSupportedException("IntPtr size " + buffer.Buffer.Length);
            }
        }

        public IntPtr Ptr { get { return buffer.Address; } }
    }
}
