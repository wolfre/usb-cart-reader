﻿using System;
namespace CartReader.Usb.LibUsb
{
    class DeviceInterface : IUsbInterface
    {
        IntPtr deviceHandle;
        Action onDispose;
        readonly byte number;

        public DeviceInterface(IntPtr deviceHandle, byte number, Action onDispose)
        {
            this.number = number;
            this.onDispose = onDispose;
            this.deviceHandle = deviceHandle;

            var result = LibUsbNative.libusb_claim_interface(deviceHandle, number);
            if (result != 0)
            {
                deviceHandle = IntPtr.Zero;
                throw new System.IO.IOException("Claiming interface " + number + " failed with " + result);
            }
        }

        public UsbDeviceInterfaceInfo Info
        {
            get { return new UsbDeviceInterfaceInfo() { number = number }; }
        }

        public void Dispose()
        {
            if (deviceHandle == IntPtr.Zero)
                LibUsbNative.libusb_release_interface(deviceHandle, number);

            deviceHandle = IntPtr.Zero;

            if (onDispose != null)
                onDispose();

            onDispose = null;
        }

        public int BulkOutTransfer(byte endpoint, byte[] data, TimeSpan timeout)
        {
            return BulkOutTransfer(endpoint, data, ToNativeTimeout(timeout));
        }

        public byte[] BulkInTransfer(byte endpoint, TimeSpan timeout)
        {
            return BulkInTransfer(endpoint, ToNativeTimeout(timeout), tempBuffer.Length);
        }

        enum Direction
        {
            In,
            Out
        }

        static byte ToNativeEndpoint(byte enpoint, Direction direction)
        {
            // https://libusb.sourceforge.io/api-1.0/group__libusb__desc.html
            return (byte)((enpoint & 0x7f) | (direction == Direction.In ? 0x80 : 0x00));
        }

        static uint ToNativeTimeout(TimeSpan timeout)
        {
            if (timeout.Ticks < 0)
                throw new ArgumentException(nameof(timeout) + " was negative (" + timeout.Ticks + ")");

            if (timeout.Ticks == 0)
                throw new ArgumentException(nameof(timeout) + " was 0");

            var timeoutInMs = (uint)timeout.TotalMilliseconds;
            if (timeoutInMs < 1)
                timeoutInMs = 1;
            return timeoutInMs;
        }

        int BulkOutTransfer(byte endpoint, byte[] data, uint timeoutInMs)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            using (var sentBytes = new NativeBuffer(sizeof(int)))
            using (var dataNative = new NativeBuffer(data))
            {
                var result = LibUsbNative.libusb_bulk_transfer(
                    deviceHandle, 
                    ToNativeEndpoint(endpoint, Direction.Out), 
                    dataNative.Address, 
                    data.Length, 
                    sentBytes.Address, 
                    timeoutInMs);

                if (result != 0)
                    throw new System.IO.IOException(nameof(LibUsbNative.libusb_bulk_transfer) + " resturned " + result);

                return BitConverter.ToInt32(sentBytes.Buffer, 0);
            }
        }

        readonly byte[] tempBuffer = new byte[4 * 1024];

        byte[] BulkInTransfer(byte endpoint, uint timeoutInMs, int len)
        {
            using (var receivedBytes = new NativeBuffer(sizeof(int)))
            using (var dataNative = new NativeBuffer(tempBuffer))
            {
                var result = LibUsbNative.libusb_bulk_transfer(
                    deviceHandle,
                    ToNativeEndpoint(endpoint, Direction.In),
                    dataNative.Address,
                    len,
                    receivedBytes.Address,
                    timeoutInMs);

                if (result != 0)
                    throw new System.IO.IOException(nameof(LibUsbNative.libusb_bulk_transfer) + " resturned " + result);

                var rxBytes = BitConverter.ToInt32(receivedBytes.Buffer, 0);
                if( rxBytes < 0 )
                    throw new System.IO.IOException(nameof(LibUsbNative.libusb_bulk_transfer) + " transfered " + rxBytes + " bytes?");

                var data = new byte[rxBytes];
                Array.Copy(tempBuffer, data, data.Length);
                return data;
            }
        }
    }
}
