﻿using System;
using System.Collections.Generic;

namespace CartReader.Usb.LibUsb
{
    class Stack : IUsbStack
    {
        IntPtr context;

        public Stack()
        {
            using (var ptrptr = new IntPtrPtr())
            {
                var result = LibUsbNative.libusb_init(ptrptr.Ptr);

                if (result != 0)
                    throw new System.IO.IOException("Init on libusb failed with " + result);

                context = ptrptr.Value;
            }
        }

        readonly List<Device> ourDevices = new List<Device>();

        public string Info
        {
            get { return "libusb"; }
        }

        public void Dispose()
        {
            var removeMe = ourDevices.ToArray();
            ourDevices.Clear();

            foreach (var d in removeMe)
                d.Dispose();

            if( context != IntPtr.Zero )
                LibUsbNative.libusb_exit(context);

            context = IntPtr.Zero;
        }

        public IUsbDevice TryFind(ushort vendorId, ushort deviceId)
        {
            var nativeDevHandle = LibUsbNative.libusb_open_device_with_vid_pid(context, vendorId, deviceId);
            if (nativeDevHandle == IntPtr.Zero)
                return null;

            Device nDev = null;
            nDev = new Device(nativeDevHandle, () => 
            {
                if (ourDevices.Contains(nDev))
                    ourDevices.Remove(nDev);
            });

            ourDevices.Add(nDev);
            return nDev;
        }
    }
}
