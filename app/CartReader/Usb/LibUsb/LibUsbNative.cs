﻿using System;
using System.Runtime.InteropServices;

namespace CartReader.Usb.LibUsb
{
    static class LibUsbNative
    {
        const string Lib = "libusb-1.0";


        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__lib.html#ga9517c37281bba0b51cc62eba728be48b
        /// </summary>
        /// <param name="context">pointer to a pointer, for context</param>
        [DllImport(Lib)]
        public static extern int libusb_init(IntPtr context);

        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__lib.html#gadc174de608932caeb2fc15d94fa0844d
        /// </summary>
        /// <param name="context">Context</param>
        [DllImport(Lib)]
        public static extern void libusb_exit(IntPtr context);

        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__dev.html#ga11ba48adb896b1492bbd3d0bf7e0f665
        /// </summary>
        /// <returns>a device handle, <see langword="null"/> if device not found</returns>
        /// <param name="context">Context.</param>
        /// <param name="vendor_id">Vendor identifier.</param>
        /// <param name="product_id">Product identifier.</param>
        [DllImport(Lib)]
        public static extern IntPtr libusb_open_device_with_vid_pid(IntPtr context, ushort vendor_id, ushort product_id);

        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__dev.html#ga779bc4f1316bdb0ac383bddbd538620e
        /// </summary>
        /// <param name="dev_handle">Dev handle.</param>
        [DllImport(Lib)]
        public static extern void libusb_close(IntPtr dev_handle);

        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__dev.html#gaee5076addf5de77c7962138397fd5b1a
        /// </summary>
        /// <returns>The claim interface.</returns>
        /// <param name="dev_handle">Device handle.</param>
        /// <param name="interface_number">Interface number.</param>
        [DllImport(Lib)]
        public static extern int libusb_claim_interface(IntPtr dev_handle, int interface_number);

        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__dev.html#ga49b5cb0d894f6807cd1693ef29aecbfa
        /// </summary>
        /// <returns>The release interface.</returns>
        /// <param name="dev_handle">Dev handle.</param>
        /// <param name="interface_number">Interface number.</param>
        [DllImport(Lib)]
        public static extern int libusb_release_interface(IntPtr dev_handle, int interface_number);

        /// <summary>
        /// https://libusb.sourceforge.io/api-1.0/group__libusb__syncio.html#ga2f90957ccc1285475ae96ad2ceb1f58c
        /// </summary>
        /// <returns>The bulk transfer.</returns>
        /// <param name="dev_handle">Dev handle.</param>
        /// <param name="endpoint">Endpoint.</param>
        /// <param name="data">Data.</param>
        /// <param name="length">Length.</param>
        /// <param name="transfered">Transfered.</param>
        /// <param name="timeout">Timeout.</param>
        [DllImport(Lib)]
        public static extern int libusb_bulk_transfer(IntPtr dev_handle, byte endpoint, IntPtr data, int length, IntPtr transfered, uint timeout);
    }
}
