﻿using System;
namespace CartReader.Usb.LibUsb
{
    class Device : IUsbDevice
    {
        IntPtr deviceHandle;
        Action onDispose;

        public Device(IntPtr deviceHandle, Action onDispose)
        {
            this.deviceHandle = deviceHandle;
            this.onDispose = onDispose;
        }

        public UsbDeviceInterfaceInfo[] AvailableInterfaces
        {
            get { throw new NotImplementedException("TODO"); }
        }

        DeviceInterface currentlyClaimed = null;

        public IUsbInterface CurrentlyClaimed
        {
            get { return currentlyClaimed; }
        }

        public IUsbInterface ClaimInterface(byte interfaceNumber)
        {
            if (currentlyClaimed != null)
                throw new NotSupportedException("There is already an interface active, must be disposed before a new once can be claimed!");

            currentlyClaimed = new DeviceInterface(deviceHandle, interfaceNumber, () => currentlyClaimed = null);
            return currentlyClaimed;
        }

        public void Dispose()
        {
            var currentlyClaimed = this.currentlyClaimed;
            this.currentlyClaimed = null;

            var onDispose = this.onDispose;
            this.onDispose = null;

            if (currentlyClaimed != null)
                currentlyClaimed.Dispose();

            if (deviceHandle != IntPtr.Zero)
                LibUsbNative.libusb_close(deviceHandle);

            deviceHandle = IntPtr.Zero;

            if (onDispose != null)
                onDispose();
        }
    }
}
