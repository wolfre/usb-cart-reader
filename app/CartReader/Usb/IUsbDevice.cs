﻿using System;
namespace CartReader.Usb
{
    class UsbDeviceInterfaceInfo
    {
        public byte number;
    }
         

    interface IUsbDevice : IDisposable
    {
        UsbDeviceInterfaceInfo[] AvailableInterfaces { get; }
        IUsbInterface CurrentlyClaimed { get; }
        IUsbInterface ClaimInterface(byte interfaceNumber);
    }
}
