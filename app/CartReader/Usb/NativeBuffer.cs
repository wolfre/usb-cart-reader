﻿using System;
using System.Runtime.InteropServices;

namespace CartReader.Usb
{
    class NativeBuffer : IDisposable
    {
        readonly byte[] buffer;
        readonly GCHandle handle;

        public NativeBuffer(int size) : this(new byte[size])
        {
        }

        public NativeBuffer(byte[] buffer)
        {
            this.buffer = buffer;
            handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
        }

        public byte[] Buffer { get { return buffer; } }
        public IntPtr Address { get { return handle.AddrOfPinnedObject(); } }


        public void Dispose()
        {
            handle.Free();
        }
    }
}
