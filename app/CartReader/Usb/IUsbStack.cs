﻿using System;
namespace CartReader.Usb
{
    interface IUsbStack : IDisposable
    {
        string Info { get; }
        IUsbDevice TryFind(ushort vendorId, ushort deviceId);
    }
}
