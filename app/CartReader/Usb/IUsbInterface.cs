﻿using System;
namespace CartReader.Usb
{
    interface IUsbInterface : IDisposable
    {
        UsbDeviceInterfaceInfo Info { get; }

        byte[] BulkInTransfer(byte endpoint, TimeSpan timeout);
        int BulkOutTransfer(byte endpoint, byte[] data, TimeSpan timeout);
    }
}
