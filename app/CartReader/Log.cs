﻿using System;
namespace CartReader
{
    enum Severity
    {
        Debug,
        Info,
        Warning,
        Error
    }

    static class Log
    {
        static string TsNow
        {
            get { return DateTime.Now.ToString("HH:mm:ss.fff"); }
        }

        public static void That(string message)
        {
            That(Severity.Debug, message);
        }

        public static void That(Severity level, string message)
        {
            Console.WriteLine("[" + level + " " + TsNow + "] " + message);
        }

        public static void That(Exception message)
        {
            That(Severity.Error, message);
        }

        public static void That(Severity level, Exception x)
        {
            if (x == null)
                return;

            That(level, x.GetType().Name + " - " + x.Message);
            That(level, x.StackTrace);
            That(level, x.InnerException);
        }

        public static void That(Severity level, CommonFileInfo nfo, string name)
        {
            Action<string, string> sum = (type, s) => That(level, "  " + type + ": " + s);

            That(level, name);
            sum("CRC32", nfo.crc32);
            sum("MD5", nfo.md5);
            sum("SHA1", nfo.sha1);
            sum("SHA256", nfo.sha256);
        }
    }
}
