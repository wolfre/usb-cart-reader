﻿using System;
using System.IO;
using System.Linq;

namespace CartReader
{
    static class Extensions
    {
        public static string Hex(this byte me, string prefix = "0x")
        {
            return prefix + me.ToString("x2");
        }

        public static string Hex(this ushort me, string prefix = "0x")
        {
            return prefix + me.ToString("x4");
        }

        public static string Hex(this uint me, string prefix = "0x")
        {
            return prefix + me.ToString("x8");
        }

        public static string Hex(this byte[] me, string prefix = "0x")
        {
            return prefix + BitConverter.ToString(me).Replace("-", "").ToLower();
        }

        public static byte[] Md5(this FileInfo f)
        {
            using (var stm = f.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var md5 = System.Security.Cryptography.MD5.Create())
                return md5.ComputeHash(stm);
        }

        public static byte[] Sha1(this FileInfo f)
        {
            using (var stm = f.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var md5 = System.Security.Cryptography.SHA1.Create())
                return md5.ComputeHash(stm);
        }

        public static FileInfo SwapExtension(this FileInfo file, string newExtenstion)
        {
            var nfoName = file.Name.Substring(0, file.Name.Length - file.Extension.Length) + "." + newExtenstion;
            return new FileInfo(Path.Combine(file.Directory.FullName, nfoName));
        }

        public static T[] Append<T>(this T[] me, T newEntry)
        {
            return me.Concat(new T[] { newEntry }).ToArray();
        }

        public static N64.Header DecodeN64RomHeader(this FileInfo bigEndianRom)
        {
            var headerRaw = new byte[N64.N64Consts.RomHeaderSize];
            using (var s = bigEndianRom.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                if (s.Read(headerRaw, 0, headerRaw.Length) != headerRaw.Length)
                    throw new IOException("Was not able to read the header from '" + bigEndianRom.FullName + "'");

            var headerDecoder = new N64.HeaderDecoder();
            var header = headerDecoder.Decode(headerRaw);

            headerDecoder.PrintInfo(l => Log.That(Severity.Info, l), header);
            if (header.endianessIndicator != N64.N64Consts.HeaderByte0)
                throw new NotSupportedException("The rom was not in BigEndian format");

            return header;
        }

        public static DirectoryInfo Dir(this DirectoryInfo me, string name)
        {
            return new DirectoryInfo(Path.Combine(me.FullName, name));
        }

        public static FileInfo File(this DirectoryInfo me, string name)
        {
            return new FileInfo(Path.Combine(me.FullName, name));
        }
    }
}
