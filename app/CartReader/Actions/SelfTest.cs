﻿using System;
namespace CartReader.Actions
{
    class SelfTestArgs : UsbArgs
    {
    }

    class SelfTest : UsbAction, Binah.IAction<SelfTestArgs>
    {
        public string Switch => "self-test";

        public string Info => "Runs some self tests on a adapter";

        public void Run(SelfTestArgs args, string[] unconsumend)
        {
            WithDeviceLowLevel(args, iface =>
            {
                var lowLevel = new Device.LowLevelCom(iface);
                var selftest = new Device.SelfTest(lowLevel);
                Log.That(Severity.Info, "Will perform selftest now!");
                selftest.DoIt();
                Log.That(Severity.Info, "All selftests passed, communication with device is fine :)");
            });
        }
    }
}
