﻿using System;
using System.IO;
using Binah;
using CartReader.Interactive;
using CartReader.N64;

namespace CartReader.Actions
{
    // https://dumping.guide/submission/nintendo-carts
    class Nfo
    {
        [CommandLine("--dump-tool", "The dump tool (this programm probably)")]
        public string dumpingToolAndVersion = NoIntro.Misc.ThisTool;

        [CommandLine("--dumper", "That's you!")]
        public string dumper;

        public const string DefaultAffiliation = "No-Intro";
        public const string QAffiliation = "A project or site you are affiliated with, probably '" + DefaultAffiliation + "'";
        [CommandLine("--affiliation", QAffiliation)]
        public string affiliation;

        const string ExampleIso = "something like '2020-11-28'";

        public const string QDumpDate = "The date of the dump, in iso format, " + ExampleIso;
        [CommandLine("--dump-creation-date", QDumpDate)]
        public string dumpCreationDate = null;

        [CommandLine("--dump-release-date", "The date the dump was / will be released, " + ExampleIso)]
        public string dumpReleaseDate = NoIntro.Misc.NotAvailable;

        [CommandLine("--links", "Additional links")]
        public string[] links = new string[0];

        public const string QTitle = "The title of the game (as advertised on the cart / box)";
        [CommandLine("--title", QTitle)]
        public string title;

        public const string QRegion = "The region of the game, e.g. 'Japan'";
        [CommandLine("--region", QRegion)]
        public string region;

        public const string DefaultEdition = "Retail";
        public const string QEdition = "The edition of the game, e.g. 'Collectors Edition'";
        [CommandLine("--edition", QEdition)]
        public string edition;

        public const string QLang = "The supported languages of the game in short ISO format, e.g. 'En' for English, see https://wiki.no-intro.org/index.php?title=Naming_Convention#Languages";
        [CommandLine("--languages", QLang)]
        public string[] languages = new string[0];

        [CommandLine("--language-select", "Whether the game features a language select")]
        public string languageSelect = NoIntro.Misc.NotAvailable;

        [CommandLine("--wiki-data-id", "The wiki data ID of the game, e.g. 'Q216995', see https://www.wikidata.org")]
        public string wikiDataId = NoIntro.Misc.NotAvailable;

        public const string QRomRegion = "The region of the rom from the header";
        [CommandLine("--rom-region", QRomRegion)]
        public string romRegion;

        public const string QRomRevision = "The revision of the rom from the header";
        [CommandLine("--rom-revision", QRomRevision)]
        public string romRevision;

        public const string QRomSerial = "The serial of the rom from the header, e.g. 'NSME'";
        [CommandLine("--rom-serial", QRomSerial)]
        public string romSerial;

        public const string QPhyMediaSerial = "The serial on the cart, something like 'NUS-006 (EUR)' and 'NUS-NSMP-EUR'";
        [CommandLine("--physical-media-serial", QPhyMediaSerial)]
        public string[] physicalMediaSerial;

        public const string QPhyMediaStamp = "The 2 or 3 letter / digit code stamped into the back of the cart, something like '29B'";
        [CommandLine("--physical-media-stamp", QPhyMediaStamp)]
        public string physicalMediaStamp;

        public const string QPcbSerial = "The serial on the pcb inside the cart, something like 'NUS-01A-01'";
        [CommandLine("--pcb-serial", QPcbSerial)]
        public string pcbSerial;

        public const string PlaceholderRomChipSerial = "NUS-NSME-0";
        public const string QRomChipSerial = "The label / serial on the physical rom chip, something like '" + PlaceholderRomChipSerial + "'";
        [CommandLine("--rom-chip-serial", QRomChipSerial)]
        public string[] romChipSerial;

        [CommandLine("--box-serial", "The serial on the box the game was sold in")]
        public string boxSerial = NoIntro.Misc.NotAvailable;

        [CommandLine("--box-barcode", "The barcode on the box the game was sold in")]
        public string boxBarcode = NoIntro.Misc.NotAvailable;
    }

    class RomInfoArgs : Nfo
    {
        [CommandLine("--rom-file", "The rom to produce the info on", false)]
        public FileInfo romFile;
        [CommandLine("--info-file", "The (text) file to write the info to. If omitted, a file will be automatically chosen based on the rom file name.")]
        public FileInfo nfoFile;

        [CommandLine("--exclude-missing", "Exclude missing or unkonw fields from the report file")]
        public object excludeMissing = null;
    }

    class RomInfo : ActionBase, IAction<RomInfoArgs>
    {
        public string Switch => "rom-info";

        public string Info => "Collects some info on a rom and saves a nice overview that ready for submission. If not supplied on the command line you will be asked for them.";

        class OutputBundle
        {
            public RomInfoSerializer.IRomInfoSerializer serializer;
            public FileInfo file;
        }

        OutputBundle[] GetOutputs(RomInfoArgs args)
        {
            var nfoSerializer = new RomInfoSerializer.InfoText(args.excludeMissing != null);

            var outputs = new OutputBundle[]
            {
                new OutputBundle()
                {
                    file = args.nfoFile ?? args.romFile.SwapExtension(nfoSerializer.DefaultExtension),
                    serializer = nfoSerializer
                },
            };

            foreach (var o in outputs)
            {
                CheckOutputFile(o.file, o.serializer.DefaultExtension, o.serializer.ShortName);
                Log.That(Severity.Info, "Will write " + o.serializer.ShortName + " to '" + o.file.FullName + "'");
            }

            return outputs;
        }

        public void Run(RomInfoArgs args, string[] unconsumend)
        {
            MustExist(args.romFile);

            // NOTE we do this here to complain early about extensions.
            //   This helps the user to correct things before entering all the stuff below ...
            var outputs = GetOutputs(args);

            var info = new RomInfoSerializer.N64RomInfo()
            {
                info = args,
                romHeader = args.romFile.DecodeN64RomHeader(),
            };

            Log.That(Severity.Info, "Will calculate checksums ...");
            CommonFileInfoGenerator.CalcFileInfo(out info.bigEndian, out info.byteSwapped, args.romFile);

            var complete = new NoIntro.InteractiveCompletion(() => new Interactive.TerminalDialog());
            complete.CompleteMissing(args, info.romHeader, args.romFile);

            foreach (var p in outputs)
                WriteFile(p, info);
        }

        void WriteFile(OutputBundle pkg, RomInfoSerializer.N64RomInfo info)
        {
            using (var o = pkg.file.Open(FileMode.Create, FileAccess.Write, FileShare.Read))
                pkg.serializer.Serialize(o, info);

            Log.That(Severity.Info, "Info successfully written to '" + pkg.file.FullName + "'");
        }

    }
}
