﻿using System;
using Binah;

namespace CartReader.Actions
{
    class AutoDetectRomSizeArgs : UsbArgs
    {
        [CommandLine(DumpRomArgs.RomBaseSwitch, DumpRomArgs.RomBaseInfo)]
        public Address romBase = new Address() { value = N64.N64Consts.RomBase };
    }

    class AutoDetectRomSize : UsbAction, IAction<AutoDetectRomSizeArgs>
    {
        public string Switch => "auto-detect-rom-size";

        public string Info => "Attempts to autodetect the rom size, this is possibly unreliable though ...";

        public void Run(AutoDetectRomSizeArgs args, string[] unconsumend)
        {
            WithDevice(args, iface =>
            {
                var dumper = new N64.CartDumper(iface);
                Log.That(Severity.Info, "Using " + args.romBase + " as rom base address");

                var header = dumper.DumpHeader(args.romBase.value);
                Log.That("Header raw: " + header.Hex());
                var dec = new N64.HeaderDecoder();
                var headerDecode = dec.Decode(header);
                dec.PrintInfo(l => Log.That(Severity.Info, l), headerDecode);

                Log.That(Severity.Info, "Will attempt autodetect of rom size ...");
                var sizeInMbit = dumper.AutoDetectSizeInMBit(args.romBase.value);
                Log.That(Severity.Info, "... my guess is " + sizeInMbit + " MBit?!");
            });
        }
    }
}
