﻿using System;
using System.IO;
using CartReader.NoIntro;

namespace CartReader.Actions.RomInfoSerializer
{
    class InfoText : IRomInfoSerializer
    {
        readonly bool excludeMissing;

        public InfoText(bool excludeMissing)
        {
            this.excludeMissing = excludeMissing;
        }

        public string DefaultExtension => "nfo";
        public string ShortName => "Info Text";

        public void Serialize(Stream output, N64RomInfo info)
        {
            using (var wrt = new StreamWriter(output, System.Text.Encoding.UTF8, 1024, true))
            {
                Serialize(wrt, info.info, info.bigEndian, info.byteSwapped);
                wrt.Flush();
            }
        }

        const string Indent = "  ";

        void Write(TextWriter txt, string label, string content)
        {
            var missing = string.IsNullOrWhiteSpace(content) || content == Misc.Unknown || content == Misc.NotAvailable;
            if (excludeMissing == false || missing == false)
                txt.WriteLine(label + ": " + content);
        }

        void Write(TextWriter txt, string what, CommonFileInfo fno)
        {
            txt.WriteLine(what + ":");
            Write(txt, Indent + "File", "" + fno.file.Name);
            Write(txt, Indent + "Size", "" + fno.size);
            Write(txt, Indent + "CRC32", fno.crc32.ToUpper());
            Write(txt, Indent + "MD5", fno.md5.ToUpper());
            Write(txt, Indent + "SHA-1", fno.sha1.ToUpper());
            Write(txt, Indent + "SHA-256", fno.sha256.ToUpper());
        }

        void Serialize(TextWriter txt, Nfo nfo, CommonFileInfo bigEndian, CommonFileInfo littleEndian)
        {
            Action<string, string> w = (label, content) => Write(txt, label, content);

            w("Dumping tool and version", nfo.dumpingToolAndVersion);
            w("Dumper", nfo.dumper);
            w("Affiliation", nfo.affiliation);
            w("Dump creation date", nfo.dumpCreationDate);
            w("Dump release date", nfo.dumpReleaseDate);
            w("Link(s)", string.Join(" ", nfo.links));
            txt.WriteLine("");

            w("Title", nfo.title);
            w("Region", nfo.region);
            w("Edition", nfo.edition);
            w("Languages", string.Join(", ", nfo.languages));
            w("Language Select", nfo.languageSelect);
            w("WikiData ID", nfo.wikiDataId);
            txt.WriteLine("");

            w("ROM Region", nfo.romRegion);
            w("ROM Revision", nfo.romRevision);
            w("ROM Serial", nfo.romSerial);
            txt.WriteLine("");

            w("Original format", NoIntro.NoIntroConsts.FormatBigEndian);
            txt.WriteLine("");


            Write(txt, NoIntro.NoIntroConsts.FormatBigEndian, bigEndian);
            Write(txt, NoIntro.NoIntroConsts.FormatByteSwapped, littleEndian);
            txt.WriteLine("");

            if (nfo.physicalMediaSerial == null || nfo.physicalMediaSerial.Length == 0)
            {
                w("Media Serial", Misc.Unknown);
            }
            else
            {
                for (int i = 0; i < nfo.physicalMediaSerial.Length; ++i)
                    w("Media Serial " + (i + 1), nfo.physicalMediaSerial[i]);
            }

            w("Media Stamp", nfo.physicalMediaStamp);
            w("PCB Serial", nfo.pcbSerial);

            if (nfo.romChipSerial == null || nfo.romChipSerial.Length == 0)
            {
                w("ROM Chip Serial", Misc.Unknown);
            }
            else
            {
                for (int i = 0; i < nfo.romChipSerial.Length; ++i)
                    w("ROM Chip Serial " + (i + 1), nfo.romChipSerial[i]);
            }


            w("Box Serial", nfo.boxSerial);
            w("Box Barcode", nfo.boxBarcode);
        }
    }
}
