﻿using System;
using System.IO;

namespace CartReader.Actions.RomInfoSerializer
{
    class N64RomInfo
    {
        public Nfo info;
        public CommonFileInfo bigEndian;
        public CommonFileInfo byteSwapped;
        public N64.Header romHeader;
    }

    interface IRomInfoSerializer
    {
        string DefaultExtension { get; }
        string ShortName { get; }

        void Serialize(Stream output, N64RomInfo info);
    }
}
