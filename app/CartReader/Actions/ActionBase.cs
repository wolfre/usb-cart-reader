﻿using System;
using System.IO;

namespace CartReader.Actions
{
    abstract class ActionBase
    {
        protected static void LogCheckSums(FileInfo file)
        {
            CommonFileInfo bigEndian;
            CommonFileInfo byteSwapped;
            CommonFileInfoGenerator.CalcFileInfo(out bigEndian, out byteSwapped, file);

            Log.That(Severity.Info, bigEndian, NoIntro.NoIntroConsts.FormatBigEndian);
            Log.That(Severity.Info, byteSwapped, NoIntro.NoIntroConsts.FormatByteSwapped);
        }

        protected static void CheckOutputFile(FileInfo file, string extension, string what)
        {
            if (file.Extension != "." + extension)
                Log.That(Severity.Warning, what + " usually have a '." + extension + "' extension, not '" + file.Extension + "')");

            file.Refresh();
            if (file.Exists)
                throw new IOException("File already exists, will not overwrite '" + file.FullName + "'");
        }

        protected static void MustExist(FileInfo f)
        {
            f.Refresh();
            if (f.Exists == false)
                throw new FileNotFoundException(f.FullName);
        }
    }
}
