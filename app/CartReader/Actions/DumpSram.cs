﻿using System;
using Binah;

namespace CartReader.Actions
{
    class DumpSramArgs : FileOutArgs
    {
        [CommandLine("--size-kbyte", "Size of the SRAM in kbyte")]
        public ushort sizeInKbyte = N64.N64Consts.DefaultSRamSizeKbyte;

        [CommandLine("--sram-base", "Base address of the sram space in the cart")]
        public Address sramBase = new Address() { value = N64.N64Consts.SRamBase };
    }

    class DumpSram : UsbAction, IAction<DumpSramArgs>
    {
        public string Switch => "dump-sram";

        public string Info => "Dumps the SRAM of a cart to a file";

        public void Run(DumpSramArgs args, string[] unconsumend)
        {
            if (args.sizeInKbyte < 1)
                throw new ArgumentException("Need a size bigger than 0");

            // https://github.com/networkfusion/altra64/blob/0d70330de5bd326ddd28b4c051060dc6d413a86a/inc/rom.h#L132
            if (args.sizeInKbyte > 128)
                Log.That(Severity.Warning, "An SRAM size of " + args.sizeInKbyte + " KByte is highly unlikely, but will try");

            CheckOutputFile(args.outputFile, "srm", "SRAM dumps");

            WithDevice(args, iface =>
            {
                var dumper = new N64.CartDumper(iface);
                Log.That(Severity.Info, "Using " + args.sramBase + " as sram base address");
                Log.That(Severity.Info, "Will start dump of " + args.sizeInKbyte + " KByte to '" + args.outputFile.FullName + "' in bigendian format");

                dumper.DumpSRamToBigEndianFile(args.outputFile, args.sramBase.value, (uint)(args.sizeInKbyte * 1024));
                LogCheckSums(args.outputFile);
            });
        }
    }
}
