﻿using System;
namespace CartReader.Actions
{
    class ShowBuildInfo : Binah.IAction<object>
    {
        public string Switch => "show-build-info";

        public string Info => "Shows information on the build of this version";

        public void Run(object args, string[] unconsumend)
        {
            Log.That(Severity.Info, About.LongName + " v" + BuildInfo.VersionSemantic);
            Log.That(Severity.Info, "Build commit: " + BuildInfo.Commit);
            Log.That(Severity.Info, "Build job: " + BuildInfo.BuildJob);
            Log.That(Severity.Info, "Build time: " + BuildInfo.BuildTime);
        }
    }
}
