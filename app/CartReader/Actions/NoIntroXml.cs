﻿using System;
using System.IO;
using System.Linq;
using Binah;
using CartReader.N64;

namespace CartReader.Actions
{
    class NoIntroXmlArgs
    {
        [CommandLine("--rom-file", "The rom to produce the info on", false)]
        public FileInfo romFile;

        [CommandLine("--no-intro-db", "A current no-intro db text file. Download from here https://datomatic.no-intro.org/index.php?page=download&s=24&op=db", false)]
        public FileInfo noIntroDbFile;

        [CommandLine("--no-intro-dat", "A current no-intro (BigEndian) dat file. Download from here https://datomatic.no-intro.org/index.php?page=download&s=24&op=dat", false)]
        public FileInfo noIntroDatFile;

        [CommandLine("--no-intro-xml", "The no-intro xml file to write the info to. If omitted, a file will be automatically chosen.")]
        public FileInfo noIntroXmlFile;
    }

    class NoIntroXml : ActionBase, IAction<NoIntroXmlArgs>
    {
        public string Switch => "no-intro-xml";

        public string Info => "Collects some info on a rom and produces an xml ready to submit to no-intro. Needs current db files to verfiy and auto fill info.";

        public void Run(NoIntroXmlArgs args, string[] unconsumend)
        {
            MustExist(args.romFile);
            MustExist(args.noIntroDbFile);
            MustExist(args.noIntroDatFile);

            var completeAuto = new NoIntro.DatabaseCompletion(args.noIntroDatFile, args.noIntroDbFile);
            var redumpSources = completeAuto.GetSources(args.romFile);

            if (args.noIntroXmlFile == null)
                args.noIntroXmlFile = args.romFile.Directory.File(redumpSources.datGame.name + ".xml");

            CheckOutputFile(args.noIntroXmlFile, "xml", "No-Intro xmls");

            var complete = new NoIntro.InteractiveCompletion(() => new Interactive.TerminalDialog());
            complete.CompleteMissing(redumpSources);

            using (var strm = args.noIntroXmlFile.Open(FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                var ser = new NoIntro.NoIntroXmlSerializer();
                ser.Serialize(strm, redumpSources);
            }

            Log.That(Severity.Info, "Written No-Intro xml to '" + args.noIntroXmlFile.FullName + "'");
        }
    }
}
