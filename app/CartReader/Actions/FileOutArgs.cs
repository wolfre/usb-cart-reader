﻿using System;
using System.IO;
using Binah;

namespace CartReader.Actions
{
    class FileOutArgs : UsbArgs
    {
        [CommandLine("--output-file", "File to write dump to, will be in bigendian format", false)]
        public FileInfo outputFile = null;
    }
}
