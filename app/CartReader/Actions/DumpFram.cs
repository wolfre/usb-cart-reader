﻿using System;
using System.Linq;
using Binah;

namespace CartReader.Actions
{
    class DumpFramArgs : FileOutArgs
    {
        [CommandLine("--size-kbyte", "Size of the FRAM in kbyte")]
        public ushort sizeInKbyte = N64.N64Consts.DefaultFRamSizeKbyte;

        [CommandLine("--fram-base", "Base address of the fram space in the cart")]
        public Address framBase = new Address() { value = N64.N64Consts.FRamBase };

        [CommandLine("--ignore-unknown-type", "Will ignore unknown FRAM types and continue dumping")]
        public object ignoreUnknownType = null;
    }

    class DumpFram : UsbAction, IAction<DumpFramArgs>
    {
        public string Switch => "dump-fram";

        public string Info => "Dumps the FRAM of a cart to a file";

        public void Run(DumpFramArgs args, string[] unconsumend)
        {
            if (args.sizeInKbyte < 1)
                throw new ArgumentException("Need a size bigger than 0");

            // https://github.com/networkfusion/altra64/blob/0d70330de5bd326ddd28b4c051060dc6d413a86a/inc/rom.h#L132
            if (args.sizeInKbyte != N64.N64Consts.DefaultFRamSizeKbyte)
                Log.That(Severity.Warning, "An FRAM size of " + args.sizeInKbyte + " KByte is highly unlikely, but will try");

            CheckOutputFile(args.outputFile, "fla", "FRAM dumps");

            WithDevice(args, iface =>
            {
                var dumper = new N64.CartDumper(iface);

                var idRaw = dumper.GetFRamId(args.framBase.value);
                Log.That(Severity.Info, "FRAM id: " + idRaw.Hex());

                var idDec = new N64.FramIdDecoder();
                var id = idDec.Decode(idRaw);
                var info = N64.N64Consts.KnownFramTypes.FirstOrDefault(nfo => nfo.ids.Contains(id.id));

                if (info != null)
                {
                    Log.That("Detected FRAM type " + string.Join(" / ", info.types));
                }
                else
                {
                    var msg = "Unknown FRAM type with id " + id.id.Hex();
                    if (args.ignoreUnknownType == null)
                        throw new System.IO.IOException(msg);

                    Log.That(Severity.Warning, msg + ", but will continue ...");
                }

                Log.That(Severity.Info, "Using " + args.framBase + " as fram base address");
                Log.That(Severity.Info, "Will start dump of " + args.sizeInKbyte + " KByte to '" + args.outputFile.FullName + "' in bigendian format");

                dumper.DumpFRamToBigEndianFile(args.outputFile, args.framBase.value, (uint)(args.sizeInKbyte * 1014));
                LogCheckSums(args.outputFile);
            });
        }
    }
}
