﻿using System;
namespace CartReader.Actions
{
    abstract class UsbAction : ActionBase
    {
        protected Usb.IUsbStack GetStack()
        {
            // TODO detect os and support other stacks
            return new Usb.LibUsb.Stack();
        }

        protected Usb.IUsbDevice GetDevice(Usb.IUsbStack stack, UsbArgs findMe)
        {
            var device = stack.TryFind(findMe.vid, findMe.pid);
            if (device == null)
                throw new System.IO.IOException("Could not find device " + findMe.vid.Hex() + " " + findMe.pid.Hex());

            return device;
        }

        protected Usb.IUsbInterface GetInterface(Usb.IUsbDevice dev)
        {
            // FIXME this is something that probably should be done by Device.BootStrap or Device.LowLevel?!
            return dev.ClaimInterface(Device.DeviceInterface10Consts.UsbInterfaceNumber);
        }

        public void WithDeviceLowLevel(UsbArgs args, Action<Usb.IUsbInterface> doThings)
        {
            using (var stack = GetStack())
            {
                Log.That("Using " + stack.Info + " to look for dumper device");
                using (var dev = GetDevice(stack, args))
                using (var iface = GetInterface(dev))
                {
                    Log.That("Found the dumper device");
                    doThings(iface);
                }
            }
        }

        public void WithDevice(UsbArgs args, Action<Device.IDeviceInterface10> doThings)
        {
            WithDeviceLowLevel(args, iFace => 
            {
                var ll = new Device.LowLevelCom(iFace);
                var device = Device.BootStrap.Device(ll);
                doThings(device);
            });
        }
    }
}
