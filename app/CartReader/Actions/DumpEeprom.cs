﻿using System;
using Binah;

namespace CartReader.Actions
{

    class DumpEepromArgs : FileOutArgs
    {
        [CommandLine("--eeprom-words", "Size of the EEPROM in words, probably 64 (4Kbit), maybe 256 (16Kbit)", false)]
        public ushort eepromWords;
    }

    class DumpEeprom : UsbAction, IAction<DumpEepromArgs>
    {
        public string Switch => "dump-eeprom";

        public string Info => "Dumps the EEPROM of a cart to a file";

        public void Run(DumpEepromArgs args, string[] unconsumend)
        {
            if (args.eepromWords < 1)
                throw new ArgumentException("Need a size bigger than 0");

            if (args.eepromWords > 256)
                throw new ArgumentException("Max supported eeprom size is 256");

            if (args.eepromWords < 64)
                Log.That(Severity.Warning, "An EEPROM size of " + args.eepromWords + " words is highly unlikely, but will try");


            CheckOutputFile(args.outputFile, "eep", "EEPROM dumps");

            WithDevice(args, iface =>
            {
                var dumper = new N64.CartDumper(iface);
                Log.That(Severity.Info, "Will start dump of " + args.eepromWords + " words (" + (args.eepromWords * N64.N64Consts.EepromWordSizeBytes) + " Byte) to '" + args.outputFile.FullName + "'");

                dumper.DumpEeprom(args.outputFile, args.eepromWords);
                LogCheckSums(args.outputFile);
            });
        }
    }
}
