﻿using System;
namespace CartReader.Actions
{
    class Address
    {
        public uint value;

        public static Address ParseCliArg(string arg)
        {
            var clean = arg.Replace(" ", "").Replace("\t", "").ToLower();

            if (clean.StartsWith("0x"))
            {
                var hex = clean.Substring(2);
                try
                {
                    return new Address() { value = Convert.ToUInt32(hex, 16) };
                }
                catch (Exception)
                {
                    // NOOP
                }
            }
            else
            {
                uint n;
                if (uint.TryParse(clean, out n))
                    return new Address() { value = n };
            }

            throw new ArgumentException("Cannot parse '" + arg + "' to an address");
        }

        public override string ToString()
        {
            return "0x" + value.ToString("x8");
        }
    }
}
