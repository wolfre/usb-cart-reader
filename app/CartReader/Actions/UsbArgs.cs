﻿using System;
namespace CartReader.Actions
{
    abstract class UsbArgs
    {
        [Binah.CommandLine("--usb-vid", "USB vendor ID of the dumper")]
        public ushort vid = 0x16c0;
        [Binah.CommandLine("--usb-pid", "USB product ID of the dumper")]
        public ushort pid = 0x05dc;
    }
}
