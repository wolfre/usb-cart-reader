﻿using System;
using System.IO;
using System.Linq;
using Binah;

namespace CartReader.Actions
{
    class DumpRomArgs : FileOutArgs
    {
        [CommandLine("--size-mbit", "Size of the cart in megabit (probably 64 or more)", false)]
        public ushort sizeInMbit = 0;

        [CommandLine("--ignore-wrong-header", "Will ignore headers that look wrong, and continue dumping")]
        public object ignoreWrongHeader = null;

        public const string RomBaseSwitch = "--rom-base";
        public const string RomBaseInfo = "Base address of the rom space in the cart";

        [CommandLine(RomBaseSwitch, RomBaseInfo)]
        public Address romBase = new Address() { value = N64.N64Consts.RomBase };
    }

    class DumpRom : UsbAction, Binah.IAction<DumpRomArgs>
    {
        public string Switch => "dump-rom";

        public string Info => "Dumps the ROM of a cart to a file";

        public void Run(DumpRomArgs args, string[] unconsumend)
        {
            if (args.sizeInMbit < 1)
                throw new ArgumentException("Need a size bigger than 0");

            if (N64.N64Consts.KnownRomSizesMbit.Contains(args.sizeInMbit) == false)
                Log.That(Severity.Warning, "A cart size of " + args.sizeInMbit + " MBit is highly unlikely, but will try");

            CheckOutputFile(args.outputFile, NoIntro.NoIntroConsts.ExtensionBigEndian, "Big endian dumps");

            WithDevice(args, iface =>
            {
                var dumper = new N64.CartDumper(iface);
                Log.That(Severity.Info, "Using " + args.romBase + " as rom base address");

                var header = dumper.DumpHeader(args.romBase.value);
                Log.That("Header raw: " + header.Hex());
                var dec = new N64.HeaderDecoder();
                var headerDecode = dec.Decode(header);
                dec.PrintInfo(l => Log.That(Severity.Info, l), headerDecode);
                var headerOk = dec.HeaderMakesSense(headerDecode);

                if (headerOk)
                {
                    Log.That("Header looks ok!");
                }
                else
                {
                    if (args.ignoreWrongHeader == null)
                        throw new System.IO.IOException("Header looks wrong, will bail: " + header.Hex());

                    Log.That(Severity.Warning, "Header looks wrong, but will continue ...");
                }

                Log.That(Severity.Info, "Will start dump of " + args.sizeInMbit + " MBit to '" + args.outputFile.FullName + "' in bigendian format");

                dumper.DumpRomToBigEndianFile(args.outputFile, args.romBase.value, MegabitToBytes(args.sizeInMbit));
                LogCheckSums(args.outputFile);
            });
        }

        static uint MegabitToBytes(uint mbit)
        {
            return (mbit * 1024 * 1024) / 8;
        }
    }
}
