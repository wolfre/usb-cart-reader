﻿using System;
namespace CartReader.N64
{
    static class N64Consts
    {
        #region ROM

        /// <summary>
        /// http://en64.shoutwiki.com/wiki/Memory_map_detailed#Cartridge_Domain_1.28Address_2.29
        /// </summary>
        public const uint RomBase = 0x10000000;

        /// <summary>
        /// The size of the rom header in bytes.
        /// </summary>
        public const int RomHeaderSize = 64;

        /// <summary>
        /// https://www.romhacking.net/forum/index.php?topic=19524.0
        /// </summary>
        public const byte HeaderByte0 = 0x80;

        /// <summary>
        /// Gets the code page used for strings in the rom header.
        /// https://www.romhacking.net/forum/index.php?topic=19524.0
        /// </summary>
        /// <value>The rom header code page.</value>
        public static System.Text.Encoding RomHeaderCodePage
        {
            get { return System.Text.Encoding.GetEncoding(932); }
        }

        /// <summary>
        /// After this many words a new address cycle is needed. 
        /// If not the ROM internal address counter wraps around.
        /// </summary>
        public const ushort StrideWordsRom = 256;

        /// <summary>
        /// The largest known game in mbit, e.g. conkers bfd
        /// </summary>
        public const uint LargestKnownGameMbit = 512;

        /// <summary>
        /// A list of known rom sizes in ascending order.
        /// https://en.wikipedia.org/wiki/Nintendo_64
        /// https://boardgamestips.com/faq/how-big-are-n64-roms/
        /// https://www.nintendo64ever.com/Nintendo-64-128-Mb-Cartridge-Games.html
        /// </summary>
        public static uint[] KnownRomSizesMbit => new uint[]
        {

            // eg Dr. Mario 64
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0185
            32,

            // eg Super Mario 64
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0714
            64,

            // eg Cruis'n World 
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0148
            96,

            // eg Banjo-Kazooie
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0058
            128, 

            // eg Donald Duck - Quack Attack  
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0172
            160,

            // eg Tony Hawk's Pro Skater (USA) (Beta)
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=1044
            192,

            // 224 MBit is listed by nintendo64ever.com for:
            // - Bass Rush: Ecogear Powerworm Championship
            // - Jet Force Gemini
            // - Nushi Tsuri 64: Shiokaze Ni Notte
            // - WWF Wrestlemania 2000
            //
            // All of these games have 256 MBit according to datomatic.no-intro.org,
            // in all listed releases. As of Nov 2021 none of the games in datomatic
            // have 224 MBit, so we'll not include this size here.


            // eg Ocarina Of Time
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0374
            256, 

            // eg Paper Mario (USA)
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0537
            320,

            // eg Paper Mario (Europe)
            // https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0536
            384,

            LargestKnownGameMbit,
        };

        #endregion

        #region SRAM

        /// <summary>
        /// http://en64.shoutwiki.com/wiki/Memory_map_detailed#Cartridge_Domain_1.28Address_1.29
        /// </summary>
        public const uint SRamBase = 0x08000000;

        /// <summary>
        /// http://micro-64.com/database/gamesave.shtml
        /// </summary>
        public const ushort DefaultSRamSizeKbyte = 32;

        /// <summary>
        /// After this many words a new address cycle is needed. 
        /// If not the SRAM internal address counter wraps around.
        /// </summary>
        public const ushort StrideWordsSRam = StrideWordsRom;

        #endregion

        #region EEPROM

        /// <summary>
        /// https://github.com/sanni/cartreader/blob/d8cb1246f8aa9504f6de271730a8a451117fe138/Cart_Reader/N64.ino#L2065
        /// </summary>
        public const int EepromWordSizeBytes = 8;

        #endregion

        #region FRAM

        /// <summary>
        /// https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3223
        /// </summary>
        public const uint FRamBase = SRamBase;

        /// <summary>
        /// Command the FRAM into read mode.
        /// https://github.com/mupen64plus/mupen64plus-core/blob/db0f7365f2c2b8bbae523b2503e660d09d4a2a85/src/device/cart/flashram.c#L112
        /// https://github.com/networkfusion/altra64/blob/0d70330de5bd326ddd28b4c051060dc6d413a86a/inc/rom.h#L146
        /// </summary>
        public const uint FRamCommandReadMode = 0xF0000000;

        /// <summary>
        /// Command the FRAM to output its internal ID from the status register.
        /// https://github.com/mupen64plus/mupen64plus-core/blob/db0f7365f2c2b8bbae523b2503e660d09d4a2a85/src/device/cart/flashram.c#L106
        /// https://github.com/networkfusion/altra64/blob/0d70330de5bd326ddd28b4c051060dc6d413a86a/inc/rom.h#L141
        /// </summary>
        public const uint FRamCommandId = 0xE1000000;

        /// <summary>
        /// The bus address of the FRAM status register.
        /// https://github.com/networkfusion/altra64/blob/0d70330de5bd326ddd28b4c051060dc6d413a86a/inc/rom.h#L148
        /// https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3292
        /// </summary>
        public const uint FRamRegisterStatus = 0x08000000;

        /// <summary>
        /// The bus address of the FRAM command register.
        /// https://github.com/networkfusion/altra64/blob/0d70330de5bd326ddd28b4c051060dc6d413a86a/inc/rom.h#L149
        /// https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3087
        /// </summary>
        public const uint FRamRegisterCommand = 0x08010000;

        /// <summary>
        /// http://micro-64.com/database/gamesave.shtml
        /// </summary>
        public const ushort DefaultFRamSizeKbyte = 128;


        /// <summary>
        /// After this many words a new address cycle is needed. 
        /// If not the FRAM internal address counter wraps around.
        /// </summary>
        public const ushort StrideWordsFRam = 64;

        /// <summary>
        /// A list with info on known FRAM types.
        /// Collected from these sources:
        /// https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3355
        /// https://forum.arduino.cc/t/rom-reader-for-super-nintendo-super-famicom-game-cartridges/155260/126
        /// https://github.com/mupen64plus/mupen64plus-core/blob/db0f7365f2c2b8bbae523b2503e660d09d4a2a85/src/device/cart/flashram.h#L35
        /// </summary>
        /// <value>The known fram types.</value>
        public static FramInfo[] KnownFramTypes => new FramInfo[]
        {
            // NOTE not entirely sure if this exists, Mupen64Plus lists them, but nothing on google or in sannis cart reader
            new FramInfo(){ ids = new uint[]{ 0x00c20000 }, types = new string[] { "MX29L0000" } },
            // NOTE not entirely sure if this exists, Mupen64Plus lists them, but nothing on google or in sannis cart reader
            new FramInfo(){ ids = new uint[]{ 0x00c20001 }, types = new string[] { "MX29L0001" } },

            new FramInfo(){ ids = new uint[]{ 0x00c2001e }, types = new string[] { "MX29L1100", "29L1100KC-15B0" } },
            new FramInfo(){ ids = new uint[]{ 0x00c2001d }, types = new string[] { "MX29L1101", "29L1101KC-15B0" } },
            new FramInfo(){ ids = new uint[]{ 0x003200f1 }, types = new string[] { "MN63F81MPN" } },

            // NOTE Unclear what the full id really is for 29L1100KC-15B0 / 29L1101KC-15B0:
            // Mupen64Plus does not seem to explicitly support those 29L1100KC-15B0, but sannis reader does.
            // However sannies reader only lists the last byte of the expected response:
            // https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3376
            // Here's a list for a couple of known games with this chip: 
            // https://circuit-board.de/forum/index.php/Thread/16149-Suche-jemanden-der-NBA-Courtside-2-und-einen-Gamebit-hat/
            // So it was unclear what the full response was supposed to be, I checked out the full IDs on these two games:
            // - Jet Force Gemini (Europe) NUS-NJFP-EUR
            //   - FRAM chip: MX J992627 29L1101KC-15B0 1A6683
            //   - Response (right after power on): 0x1111800100c2001d
            // - Pokemon Stadium (Germany) NUS-NPOD-NOE
            //   - FRAM chip: MX S001227 29L1100KC-15B0 1B5238
            //   - Response (right after power on): 0x1111800100c2001e
            // This lines up with the 29L1100KC-15B0 being a compatible replacement / variant of the MX29L1100
            // and 29L1101KC-15B0 of the MX29L1101 respectively. Maybe they really are the same, and there's just confusion
            // of the full name of the chip. Also the values on sannis reader are used for writing to the FRAM. Maybe the 
            // response also includes some status flags.
            //
            // However, assuming the IDs in sannis code are real, I also include them here as an extra variant.
            // The following values are a combination of the MX29L1100 / MX29L1101 and the two options listed in sannis reader.
            new FramInfo(){ ids = new uint[]{ 0x00c20000 | 0x8e, 0x00c20000 | 0x84 }, types = new string[] { "29L1100KC-15B0", "29L1101KC-15B" } },
        };

        #endregion

        /// <summary>
        /// An address that is not used by (regular) carts, and should return 0s
        /// </summary>
        public const uint UnMapped = 0x00000100;
    }
}
