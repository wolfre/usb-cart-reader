﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CartReader.Device;

namespace CartReader.N64
{
    class CartDumper
    {
        readonly DataCodec codec = new DataCodec();
        readonly IDeviceInterface10 device;

        public CartDumper(IDeviceInterface10 device)
        {
            this.device = device;
        }

        public byte[] DumpHeader(uint romBase)
        {
            return ReadSingleRomBlock(romBase, N64Consts.RomHeaderSize / 2);
        }

        public uint AutoDetectSizeInMBit(uint romBase)
        {
            var blockSamples = SampleMBitBlocks(romBase);

            var sizeWithData = LastMbitWithData(blockSamples);
            Log.That("Last segment with data seems to be around " + sizeWithData + " MBit");

            var blockRepeat = BlockRepeat(blockSamples);
            if (blockRepeat == null)
            {
                Log.That("Couldn't find any repeated blocks");
            }
            else
            {
                Log.That("Data repeated after " + blockRepeat + " MBit");
            }


            var size = blockRepeat == null ? sizeWithData : (uint)blockRepeat;

            var fittingKnownSize = N64Consts.KnownRomSizesMbit.FirstOrDefault(s => s >= size);
            Log.That("I'll go with " + fittingKnownSize + " MBit");

            return fittingKnownSize;
        }

        uint LastMbitWithData(SampledBlock[] scores)
        {
            const int StopHoleSize = 3;

            int lastData = 0;
            for (int i = 0; i < scores.Length; ++i)
            {
                if (scores[i].score >= 0.5)
                    lastData = i;

                if (i - lastData >= StopHoleSize)
                    return (uint)(lastData + 1);
            }

            return (uint)scores.Length;
        }

        uint? BlockRepeat(SampledBlock[] scores)
        {
            var sums = new Dictionary<string, SampledBlock>();
            for (int i = 0; i < scores.Length; ++i)
            {
                if (sums.ContainsKey(scores[i].checksum))
                    return (uint)i;

                sums.Add(scores[i].checksum, scores[i]);
            }

            return null;
        }

        class SampledBlock
        {
            public double score;
            public string checksum;
        }

        byte[] CheckSum(IEnumerable<byte> data)
        {
            var sum = new byte[4];
            for (int i = 0; i < data.Count(); ++i)
                sum[i % sum.Length] ^= data.ElementAt(i);
            return sum;
        }

        SampledBlock[] SampleMBitBlocks(uint romBase)
        {
            const ushort SampleWords = N64Consts.RomHeaderSize / 2;

            var scorePerMbit = new List<SampledBlock>();

            for (uint sizeInMBit = 0; sizeInMBit < N64Consts.LargestKnownGameMbit; ++sizeInMBit)
            {
                uint off = (sizeInMBit * 1024 * 1024) / 8;
                uint address = romBase + off;

                var sampleBlocks = new byte[][]
                {
                    ReadSingleRomBlock(address + 0 * 1024, SampleWords),
                    ReadSingleRomBlock(address + 1 * 1024, SampleWords),
                    ReadSingleRomBlock(address + 2 * 1024, SampleWords),
                    ReadSingleRomBlock(address + 3 * 1024, SampleWords),
                };

                double score = sampleBlocks.SelectMany(sb => sb).Select(b => b != 0 && b != 0xff).Count(b => b);
                score /= sampleBlocks.Select(sb => sb.Length).Sum();
                Log.That(sizeInMBit + " Mbit data-ish score: " + score);

                scorePerMbit.Add(new SampledBlock()
                {
                    score = score,
                    checksum = CheckSum(sampleBlocks.SelectMany(b => b)).Hex(""),
                });
            }

            return scorePerMbit.ToArray();
        }

        public void DumpRomToBigEndianFile(FileInfo output, uint startAddress, uint sizeInBytes)
        {
            DumpAddressToBigEndianFile(output, startAddress, sizeInBytes, N64Consts.StrideWordsRom);
        }

        public void DumpSRamToBigEndianFile(FileInfo output, uint startAddress, uint sizeInBytes)
        {
            DumpAddressToBigEndianFile(output, startAddress, sizeInBytes, N64Consts.StrideWordsSRam);
        }

        public void DumpFRamToBigEndianFile(FileInfo output, uint startAddress, uint sizeInBytes)
        {
            FramCommand(startAddress, N64Consts.FRamCommandReadMode);
            DumpAddressToBigEndianFile(output, startAddress, sizeInBytes, N64Consts.StrideWordsFRam);
        }

        ushort MaxWords { get { return (ushort)(device.Info.bufferSize / 2); } }

        void DumpAddressToBigEndianFile(FileInfo output, uint startAddress, uint sizeInBytes, ushort strideWords)
        {
            if (startAddress % 2 != 0)
                throw new ArgumentException(nameof(startAddress) + " is " + startAddress + " which is not divisble by 2");
            if (sizeInBytes % 2 != 0)
                throw new ArgumentException(nameof(sizeInBytes) + " is " + sizeInBytes + " which is not divisble by 2");

            var start = DateTime.Now;
            using (var strm = output.Open(FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                uint off = 0;
                while (off < sizeInBytes)
                {
                    uint words = (sizeInBytes - off) / 2;
                    if (words > MaxWords)
                        words = MaxWords;

                    device.CommandBusRead(off + startAddress, (ushort)words, strideWords);
                    var block = device.ReadFromBuffer((int)(words*2));

                    strm.Write(block, 0, block.Length);

                    if (off % (128 * 1024) == 0)
                        PrintStats(sizeInBytes, off, DateTime.Now - start);

                    off += (uint)block.Length;
                }
            }

            var time = DateTime.Now - start;
            Log.That(Severity.Info, "Dumping done after " + time.TotalSeconds + " sec.");
        }

        static void PrintStats(uint sizeInBytes, uint off, TimeSpan time)
        {
            var kbytePerSecond = off / time.TotalSeconds;
            kbytePerSecond /= 1024.0;

            var timeLeftSec = (double)(sizeInBytes - off);
            timeLeftSec /= 1024.0;
            timeLeftSec /= kbytePerSecond;

            var percent = off * 100;
            percent /= sizeInBytes;

            Log.That(Severity.Info, "Written " + off + " bytes of " + sizeInBytes + " (" + percent + "%) at " + Math.Round(kbytePerSecond, 1) + " kbyte/s (ETA " + Math.Round(timeLeftSec) + " sec.)");
        }

        class EepromWord
        {
            const int MinSame = 2;
            readonly List<byte[]> samples = new List<byte[]>();

            byte[] MostLikely(out int count)
            {
                count = 0;
                byte[] best = null;
                var stats = new Dictionary<string, int>();

                for (int i = 0; i < samples.Count; ++i)
                {
                    var h = samples[i].Hex();
                    if (stats.ContainsKey(h) == false)
                        stats.Add(h, 1);
                    else
                        stats[h] += 1;

                    if (count < stats[h])
                    {
                        best = samples[i];
                        count = stats[h];
                    }
                }

                return best;
            }

            public byte[] Data
            {
                get
                {
                    int c;
                    return MostLikely(out c);
                }
            }

            public bool Confident
            {
                get
                {
                    int same;
                    MostLikely(out same);
                    var different = samples.Count - same;
                    return same >= MinSame && (same / 2) > different;
                }
            }

            public void AddSample(byte[] word)
            {
                if (word.Length != N64Consts.EepromWordSizeBytes)
                    throw new ArgumentException(nameof(word) + ".Length = " + word.Length);

                samples.Add(word);
            }
        }

        public void DumpEeprom(FileInfo output, ushort words)
        {
            if (words < 1)
                throw new ArgumentException(nameof(words) + " was 0");
            if (words > 256)
                throw new ArgumentException("Max supported eeprom size is 256 (" + words + ")");


            var start = DateTime.Now;
            var round = 0;
            var wordContainers = Enumerable.Range(0, words).Select(i => new EepromWord()).ToArray();
            while (wordContainers.Any(c => c.Confident == false))
            {
                var todo = wordContainers.Count(c => c.Confident == false);
                Log.That("Reading EEPROM round " + round + ", " + todo + " words to go");
                for (int i = 0; i < wordContainers.Length; ++i)
                {
                    var co = wordContainers[i];
                    device.CommandReadEeprom((byte)i);
                    var wordData = device.ReadFromBuffer(N64Consts.EepromWordSizeBytes);
                    co.AddSample(wordData);
                }
                ++round;
            }

            Log.That("Done reading, I'm confident about all " + wordContainers.Length + " now"); 

            using (var strm = output.Open(FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                for (int i = 0; i < wordContainers.Length; ++i)
                {
                    var wordData = wordContainers[i].Data;
                    strm.Write(wordData, 0, wordData.Length);
                }
            }

            var time = DateTime.Now - start;
            Log.That(Severity.Info, "Dumping done after " + time.TotalSeconds + " sec.");
        }

        public byte[] GetFRamId(uint framBase)
        {
            FramCommand(framBase, N64Consts.FRamCommandId);
            device.CommandBusRead(framBase + StatusRegisterOffset, 4, N64Consts.StrideWordsFRam);
            return device.ReadFromBuffer(8);
        }

        byte[] ReadSingleRomBlock(uint address, ushort words)
        {
            if (address % 2 != 0)
                throw new IOException("Unaligned read at " + address.Hex());

            device.CommandBusRead(address, words, N64Consts.StrideWordsRom);
            return device.ReadFromBuffer(words * 2);
        }

        const uint CommandRegisterOffset = N64Consts.FRamRegisterCommand - N64Consts.FRamBase;
        const uint StatusRegisterOffset = N64Consts.FRamRegisterStatus - N64Consts.FRamBase;

        void FramCommand(uint framBase, uint command)
        {
            device.CommandBufferWrite();
            device.SendToBuffer(codec.Encode(command));
            device.CommandBusWrite(framBase + CommandRegisterOffset, 2, N64Consts.StrideWordsFRam);
        }
    }
}
