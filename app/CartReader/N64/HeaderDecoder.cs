﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CartReader.N64
{
    public enum RegionConvention
    {
        General,
        NoIntro
    }

    class Header
    {
        public class Pi
        {
            public byte piBsdDom1Rls;
            public byte piBsdDom1Pgs;
            public byte piBsdDom1Pwd;
            public byte piBsdDom1Lat;
        }

        public class MediaFormat
        {
            public byte type;
            public string id;
            public byte region;
        }

        public class Version
        {
            public byte major;
            public byte minor;
        }

        public byte endianessIndicator;
        public Pi pi;
        public uint clockRateOverride;
        public uint bootAddress;
        public uint release;
        public uint crc1;
        public uint crc2;
        public byte[] unknown0x18;
        public string name;
        public byte[] unknown0x34;
        public MediaFormat mediaFormat;
        public Version version;
    }

    class HeaderDecoder
    {
        // http://en64.shoutwiki.com/wiki/ROM
        // https://www.romhacking.net/forum/index.php?topic=19524.0
        // https://www.emutalk.net/threads/n64-card-questions.54293/

        class Ctx
        {
            int pos = 0;
            readonly byte[] data;

            public Ctx(byte[] data)
            {
                this.data = data;
            }

            public byte U8()
            {
                if (pos >= data.Length)
                    throw new ArgumentException("Cant decode u8, end of data");
                var n = data[pos];
                ++pos;
                return n;
            }

            public ushort U16()
            {
                ushort n = U8();
                n <<= 8;
                n |= (ushort)U8();
                return n;
            }

            public uint U32()
            {
                uint n = U16();
                n <<= 16;
                n |= (uint)U16();
                return n;
            }

            public byte[] Data(int len)
            {
                var d = new byte[len];
                for (int i = 0; i < d.Length; ++i)
                    d[i] = U8();
                return d;
            }

            public string String(int len, bool removeNull = false)
            {
                var str = Data(len);

                if (removeNull)
                    str = str.Where(b => b != 0).ToArray();

                return N64Consts.RomHeaderCodePage.GetString(str);
            }
        }


        public Header Decode(byte[] data)
        {
            if (data.Length < N64Consts.RomHeaderSize)
                throw new ArgumentException("Header is " + N64Consts.RomHeaderSize + " byte, but only got " + data.Length);

            var h = new Header();
            Decode(h, new Ctx(data));
            return h;
        }

        void Decode(Header h, Ctx ctx)
        {
            h.endianessIndicator = ctx.U8();
            h.pi = new Header.Pi();
            Decode(h.pi, ctx);
            h.clockRateOverride = ctx.U32();
            h.bootAddress = ctx.U32();
            h.release = ctx.U32();
            h.crc1 = ctx.U32();
            h.crc2 = ctx.U32();
            h.unknown0x18 = ctx.Data(8);
            h.name = ctx.String(20, true);
            h.unknown0x34 = ctx.Data(7);
            h.mediaFormat = new Header.MediaFormat();
            Decode(h.mediaFormat, ctx);
            h.version = new Header.Version();
            Decode(h.version, ctx);
        }

        void Decode(Header.Version version, Ctx ctx)
        {
            var lower = ctx.U8();
            var upper = lower >> 4;
            lower &= 0x0f;
            version.major = (byte)(upper + 1);
            version.minor = (byte)lower;
        }

        void Decode(Header.MediaFormat mediaFormat, Ctx ctx)
        {
            mediaFormat.type = ctx.U8();
            mediaFormat.id = ctx.String(2);
            mediaFormat.region = ctx.U8();
        }

        void Decode(Header.Pi pi, Ctx ctx)
        {
            var pi1 = ctx.U8();
            pi.piBsdDom1Rls = (byte)(pi1 >> 4);
            pi.piBsdDom1Pgs = (byte)(pi1 & 0x0f);
            pi.piBsdDom1Pwd = ctx.U8();
            pi.piBsdDom1Lat = ctx.U8();
        }

        #region header check

        public bool HeaderMakesSense(Header h)
        {
            return
                h.endianessIndicator == 0x80 &&
                ToRegionString(h.mediaFormat.region, RegionConvention.General) != UnknownRegion &&
                ToMediaTypeString(h.mediaFormat.type) != UnknownType;
        }

        #endregion

        #region info

        static string H(byte b)
        {
            return "0x" + b.ToString("x2");
        }
        static string H(ushort b)
        {
            return "0x" + b.ToString("x4");
        }
        static string H(uint b)
        {
            return "0x" + b.ToString("x8");
        }
        static string H(byte[] b)
        {
            return "0x" + BitConverter.ToString(b).Replace("-", "");
        }

        public void PrintInfo(Action<string> writeLine, Header h)
        {
            writeLine("Header:");

            Func<string, string, KeyValuePair<string, string>> kv = (k, v) =>
            {
                var caps = k.Substring(0, 1).ToUpper() + k.Substring(1);
                return new KeyValuePair<string, string>(caps, v);
            };

            var parts = new KeyValuePair<string, string>[]
            {
                kv(nameof(h.endianessIndicator), H(h.endianessIndicator)),
                kv(nameof(h.pi), ToString(h.pi)),
                kv(nameof(h.clockRateOverride), H(h.clockRateOverride)),
                kv(nameof(h.bootAddress), H(h.bootAddress)),
                kv(nameof(h.release), H(h.release)),
                kv(nameof(h.crc1), H(h.crc1)),
                kv(nameof(h.crc2), H(h.crc2)),
                kv("Reserved[" + h.unknown0x18.Length + "]", H(h.unknown0x18)),
                kv(nameof(h.name), "'" + h.name + "'"),
                kv("Reserved[" + h.unknown0x34.Length + "]", H(h.unknown0x34)),
                kv(nameof(h.mediaFormat), ToString(h.mediaFormat)),
                kv(nameof(h.version), ToNiceVersion(h.version)),
            };

            foreach (var e in parts)
                writeLine("  " + e.Key + ": " + e.Value);
        }

        string ToString(Header.MediaFormat h)
        {
            var reg = ToRegionString(h.region, RegionConvention.General);
            var typ = ToMediaTypeString(h.type);

            var nfo =
                (typ == UnknownType ? "?" : StringFrom(h.type)) +
                h.id +
                (reg == UnknownRegion ? "?" : StringFrom(h.region));

            nfo += " (" +
                (typ == UnknownType ? "Unknown type 0x" + H(h.type) : typ) +
                ", " +
                (reg == UnknownRegion ? "Unknown region 0x" + H(h.region) : reg) +
                ")";

            return nfo;
        }

        string StringFrom(byte c)
        {
            return N64Consts.RomHeaderCodePage.GetString(new byte[] { c });
        }

        string ToString(Header.Pi h)
        {
            return
                "RLS 0x" + h.piBsdDom1Rls.ToString("x1") +
                "  PGS 0x" + h.piBsdDom1Pgs.ToString("x1") +
                "  PWD " + H(h.piBsdDom1Pwd) +
                "  LAT " + H(h.piBsdDom1Lat);
        }

        class Region
        {
            /// <summary>
            /// http://n64devkit.square7.ch/info/submission/pal/01-01.html
            /// http://en64.shoutwiki.com/wiki/ROM
            /// 
            /// </summary>
            public string full;
            /// <summary>
            /// https://wiki.no-intro.org/index.php?title=Naming_Convention#Region
            /// </summary>
            public string noIntro;
        }

        static readonly Dictionary<byte, Region> region = new Dictionary<byte, Region>()
        {
            // 7 does not seem to exist, as of Nov 2021 no-intro does not list a game with such a serial / region
            // However we keep it here as it does appear in some sources.
            { 0x37, new Region() { full = "Beta", noIntro = "Beta" } },
            // A == "Japan, USA" => https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0007
            { 0x41, new Region() { full = "Asian (NTSC)", noIntro = "Japan, USA" } },
            { 0x42, new Region() { full = "Brazilian", noIntro = "Brazil" } },
            { 0x43, new Region() { full = "Chinese", noIntro = "China" } },
            { 0x44, new Region() { full = "German", noIntro = "Germany" } },
            { 0x45, new Region() { full = "North America", noIntro = "USA" } },
            { 0x46, new Region() { full = "French", noIntro = "France" } },
            { 0x47, new Region() { full = "Gateway 64 (NTSC)", noIntro = "World" } },
            { 0x48, new Region() { full = "Dutch", noIntro = "Denmark" } },
            { 0x49, new Region() { full = "Italian", noIntro = "Italy" } },
            { 0x4A, new Region() { full = "Japanese", noIntro = "Japan" } },
            { 0x4B, new Region() { full = "Korean", noIntro = "Korea" } },
            { 0x4C, new Region() { full = "Gateway 64 (PAL)", noIntro = "World" } },
            { 0x4E, new Region() { full = "Canadian", noIntro = "Canada" } },
            { 0x50, new Region() { full = "European (basic spec.)", noIntro = "Europe" } },
            { 0x53, new Region() { full = "Spanish", noIntro = "Spain" } },
            // T => "Turok 3 - Shadow of Oblivion (Europe) (Beta) (2000-07-16)"
            { 0x54, new Region() { full = "European", noIntro = "Europe" } },
            { 0x55, new Region() { full = "Australian", noIntro = "Australia" } },
            { 0x57, new Region() { full = "Scandinavian", noIntro = "Scandinavia" } },
            // X == Europe => https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0659
            { 0x58, new Region() { full = "European", noIntro = "Europe" } },
            // Y == Europe => https://datomatic.no-intro.org/index.php?page=show_record&s=24&n=0658
            { 0x59, new Region() { full = "European", noIntro = "Europe" } },
            // Z does not seem to exist, as of Nov 2021 no-intro does not list a game with such a serial / region
            // However we keep it here as it does appear in some sources, but keep it as "other" for now
            { 0x5a, new Region() { full = "Others", noIntro = "Other" } },
        };

        public const string UnknownRegion = null;

        public string ToRegionString(byte mediaFormatRegion, RegionConvention convention)
        {
            if (region.ContainsKey(mediaFormatRegion) == false)
                return UnknownRegion;

            return convention == RegionConvention.General ? region[mediaFormatRegion].full : region[mediaFormatRegion].noIntro;
        }

        static byte Ascii(char c)
        {
            return System.Text.Encoding.ASCII.GetBytes(new char[] { c })[0];
        }

        static readonly Dictionary<byte, string> type = new Dictionary<byte, string>()
        {
            { Ascii('N'), "Cart" },
            { Ascii('D'), "64DD disk" },
            { Ascii('C'), "Cartridge part of expandable game" },
            { Ascii('E'), "64DD expansion for cart" },
            { Ascii('Z'), "Aleck64 cart" },
        };

        public const string UnknownType = null;

        public string ToMediaTypeString(byte mediaFormatType)
        {
            return type.ContainsKey(mediaFormatType) ? type[mediaFormatType] : UnknownType;
        }

        public string ToNiceVersion(Header.Version h)
        {
            var ov = h.major - 1;
            ov <<= 4;
            ov |= h.minor & 0xf;
            return "0x" + ov.ToString("x2") + " (" + h.major + "." + h.minor + ")";
        }

        public string ToNiceSerial(Header.MediaFormat h)
        {
            return StringFrom(h.type) + h.id + StringFrom(h.region);
        }

        #endregion
    }
}
