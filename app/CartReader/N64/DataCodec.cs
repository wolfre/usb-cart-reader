﻿using System;
namespace CartReader.N64
{
    class DataCodec
    {
        public byte[] Encode(uint u32)
        {
            var addressArg = new byte[4];

            addressArg[0] = (byte)((u32 >> 24) & 0xff);
            addressArg[1] = (byte)((u32 >> 16) & 0xff);
            addressArg[2] = (byte)((u32 >> 8) & 0xff);
            addressArg[3] = (byte)((u32 >> 0) & 0xff);

            return addressArg;
        }

        public uint DecodeU32(byte[] data)
        {
            if (data.Length < 4) 
                throw new ArgumentException(nameof(data));

            uint d = data[0];
            d <<= 8;
            d |= data[1];
            d <<= 8;
            d |= data[2];
            d <<= 8;
            d |= data[3];
            return d;
        }
    }
}
