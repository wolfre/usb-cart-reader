﻿using System;
namespace CartReader.N64
{
    class HeaderEncoder
    {
        public byte Encode(Header.Version v)
        {
            var enc = v.major & 0x0f;

            if (enc > 0)
                enc -= 1;

            enc <<= 4;
            enc |= v.minor & 0x0f;

            return (byte)enc;
        }
    }
}
