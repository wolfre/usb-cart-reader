﻿using System;
using System.Linq;

namespace CartReader.N64
{
    class FramId
    {
        public uint unknown;
        public uint id;
    }

    class FramIdDecoder
    {
        readonly DataCodec codec = new DataCodec();

        public FramId Decode(byte[] statusRegister)
        {
            if (statusRegister.Length != 8)
                throw new NotSupportedException("Don't know how to decode " + statusRegister.Hex());


            // https://github.com/sanni/cartreader/blob/bd3eaa106b6e4cbdffbfa89a3fb5fedf3e028b17/Cart_Reader/N64.ino#L3281
            // https://github.com/mupen64plus/mupen64plus-core/blob/db0f7365f2c2b8bbae523b2503e660d09d4a2a85/src/device/cart/flashram.h#L35

            return new FramId()
            {
                unknown = codec.DecodeU32(statusRegister.Take(4).ToArray()),
                id = codec.DecodeU32(statusRegister.Skip(4).Take(4).ToArray()),
            };
        }
    }
}
