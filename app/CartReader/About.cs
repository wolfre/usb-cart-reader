﻿using System;
namespace CartReader
{
    static class About
    {
        public const string LongName = "USB Cart Reader";
        public const string Info = "An app to dump N64 rom carts using an USB adapter";
        public const string Copyright = "Rene Wolf 2021";
    }
}
