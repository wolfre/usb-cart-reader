﻿using System;
namespace CartReader.Interactive
{
    enum DialogConfirmation
    {
        None,
        ConfirmationDefaultYes,
    }

    interface IDialog
    {
        char MultiValueSeparator { get; set; }
        bool TrimSpacesFromAnswer { get; set; }

        string AskOneLineText(string defaultValue, string question, DialogConfirmation confirm);
        string[] AskOneLineMultiEntryText(string[] defaultValue, string question, DialogConfirmation confirm);
    }
}
