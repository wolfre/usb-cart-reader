﻿using System;
using System.Linq;

namespace CartReader.Interactive
{
    class TerminalDialog : IDialog
    {
        readonly System.IO.TextReader rdr;
        readonly System.IO.TextWriter wrt;

        public TerminalDialog(System.IO.TextReader rdr, System.IO.TextWriter wrt)
        {
            this.rdr = rdr;
            this.wrt = wrt;
            MultiValueSeparator = ',';
            TrimSpacesFromAnswer = false;
        }

        public TerminalDialog()
            : this(Console.In, Console.Out)
        {
        }

        string ReadOneLine()
        {
            return rdr.ReadLine().Replace("\n", "").Replace("\r", "");
        }

        public char MultiValueSeparator { get; set; }
        public bool TrimSpacesFromAnswer { get; set; }

        public string AskOneLineText(string defaultValue, string question, DialogConfirmation confirm)
        {
            while (true)
            {
                var answer = AskOneLineAnswer(defaultValue, question, false);

                if (IsJustEnter(answer))
                    answer = defaultValue;
                else
                    answer = PostProcessAnswer(answer);

                if (confirm == DialogConfirmation.None)
                    return answer;

                if (ConfirmDefaultYes(answer))
                    return answer;
            }
        }

        public string[] AskOneLineMultiEntryText(string[] defaultValue, string question, DialogConfirmation confirm)
        {
            Func<string[], string> oneLine = a => string.Join(MultiValueSeparator + (TrimSpacesFromAnswer ? " " : ""), a);

            while (true)
            {
                var answerRaw = AskOneLineAnswer(oneLine(defaultValue), question, true);

                var answer = answerRaw
                    .Split(MultiValueSeparator)
                    .Select(PostProcessAnswer)
                    .ToArray();

                if (IsJustEnter(answerRaw))
                    answer = defaultValue;

                if (confirm == DialogConfirmation.None)
                    return answer;

                if (ConfirmDefaultYes(oneLine(answer)))
                    return answer;
            }
        }

        static bool IsJustEnter(string answer)
        {
            return string.IsNullOrWhiteSpace(answer);
        }

        bool ConfirmDefaultYes(string answer)
        {
            wrt.Write("Is '" + answer + "' correct? [y]: ");
            wrt.Flush();
            var accept = ReadOneLine();
            return accept.ToLower() == "y" || IsJustEnter(accept);
        }

        string AskOneLineAnswer(string defaultValue, string question, bool multi)
        {
            wrt.WriteLine();
            wrt.Write(question.TrimEnd('?', ' ') + "?" + (multi ? " (multiple entries, separated by '" + MultiValueSeparator + "') " : " ") + "[" + defaultValue + "]: ");
            wrt.Flush();
            var answer = ReadOneLine();
            return answer;
        }

        string PostProcessAnswer(string a)
        {
            return TrimSpacesFromAnswer ? a.Trim(' ') : a;
        }
    }
}
