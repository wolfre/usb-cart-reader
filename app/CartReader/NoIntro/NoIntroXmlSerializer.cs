﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using CartReader.Actions;
using CartReader.Actions.RomInfoSerializer;
using CartReader.Interactive;
using CartReader.N64;

namespace CartReader.NoIntro
{

#pragma warning disable CS1591 // Missing XML comment for publicly visible type

    // NOTE needs to be public for xml serializer to work
    // Recreates https://datomatic.no-intro.org/stuff/example_upload_custom.xml
    // Was discussed here: https://forum.no-intro.org/viewtopic.php?f=2&t=5064
    // Also See 
    // - https://wiki.no-intro.org/index.php?title=Source_Convention
    // - https://wiki.no-intro.org/index.php?title=Naming_Convention 
    // - https://wiki.no-intro.org/index.php?title=Template:Submittinginfo
    // - https://forum.no-intro.org/viewtopic.php?f=4&t=2554
    // - https://forum.no-intro.org/viewtopic.php?t=2901

    [XmlRoot(ElementName = "datafile")]
    public class NoIntroXmlScheme1
    {
        public class Header { }

        public class Archive
        {
            [XmlAttribute("name")]
            public string name = "";

            [XmlAttribute("namealt")]
            public string namealt = "";

            [XmlAttribute("region")]
            public string region = "";

            [XmlAttribute("languages")]
            public string languages = "";

            [XmlAttribute("showlang")]
            public string showlang = "";

            [XmlAttribute("version")]
            public string version = "";

            [XmlAttribute("devstatus")]
            public string devstatus = "";

            [XmlAttribute("additional")]
            public string additional = "";

            [XmlAttribute("special1")]
            public string special1 = "";

            [XmlAttribute("special2")]
            public string special2 = "";

            [XmlAttribute("gameid")]
            public string gameid = "";

            [XmlAttribute("clone")]
            public string clone = "";

            [XmlAttribute("regionalparent")]
            public string regionalparent = "";

            [XmlAttribute("mergeof")]
            public string mergeof = "";

            [XmlAttribute("datternote")]
            public string datternote = "";

            [XmlAttribute("stickynote")]
            public string stickynote = "";
        }

        public class Flags
        {
            [XmlAttribute("bios")]
            public int bios = 0;

            [XmlAttribute("licensed")]
            public int licensed = 0;

            [XmlAttribute("pirate")]
            public int pirate = 0;

            [XmlAttribute("physical")]
            public int physical = 0;

            [XmlAttribute("complete")]
            public int complete = 0;

            [XmlAttribute("nodump")]
            public int nodump = 0;

            [XmlAttribute("public")]
            public int pub = 0;

            [XmlAttribute("dat")]
            public int dat = 0;
        }

        public class Rom
        {
            [XmlAttribute("forcename")]
            public string forcename = "";

            [XmlAttribute("emptydir")]
            public int emptydir = 0;

            [XmlAttribute("extension")]
            public string extension = "";

            [XmlAttribute("item")]
            public string item = "";

            [XmlAttribute("date")]
            public string date = "";

            [XmlAttribute("format")]
            public string format = "";

            [XmlAttribute("version")]
            public byte version = 0;

            [XmlAttribute("utype")]
            public string utype = "";

            [XmlAttribute("size")]
            public uint size = 0;

            [XmlAttribute("crc")]
            public string crc = "";

            [XmlAttribute("md5")]
            public string md5 = "";

            [XmlAttribute("sha1")]
            public string sha1 = "";

            [XmlAttribute("sha256")]
            public string sha256 = "";

            [XmlAttribute("serial")]
            public string serial = "";

            [XmlAttribute("bad")]
            public int bad = 0;

            [XmlAttribute("unique")]
            public int unique = 0;

            [XmlAttribute("mergename")]
            public string mergename = "";
        }

        public class Details
        {
            [XmlAttribute("section")]
            public string section = "";

            [XmlAttribute("rominfo")]
            public string rominfo = "";

            [XmlAttribute("dumpdate")]
            public string dumpdate = "";

            [XmlAttribute("originalformat")]
            public string originalformat = "";

            [XmlAttribute("knowndumpdate")]
            public int knowndumpdate = 0;

            // NOTE not really needed and can be left out
            //   https://forum.no-intro.org/viewtopic.php?p=28055#p28055
            // [XmlAttribute("releasedate")]
            // public string releasedate = "";
            // [XmlAttribute("knownreleasedate")]
            // public int knownreleasedate = 0;

            [XmlAttribute("dumper")]
            public string dumper = "";

            [XmlAttribute("project")]
            public string project = "";

            [XmlAttribute("tool")]
            public string tool = "";

            [XmlAttribute("origin")]
            public string origin = "";

            [XmlAttribute("comment1")]
            public string comment1 = "";

            [XmlAttribute("comment2")]
            public string comment2 = "";

            [XmlAttribute("link1")]
            public string link1 = "";

            [XmlAttribute("link2")]
            public string link2 = "";

            [XmlAttribute("region")]
            public string region = "";

            // NOTE not really needed any more
            //   https://forum.no-intro.org/viewtopic.php?p=28055#p28055
            // [XmlAttribute("mediatitle")]
            // public string mediatitle = "";
        }

        public class Serials
        {
            [XmlAttribute("mediaserial1")]
            public string mediaserial1 = "";

            [XmlAttribute("mediaserial2")]
            public string mediaserial2 = "";

            [XmlAttribute("pcbserial")]
            public string pcbserial = "";

            [XmlAttribute("romchipserial1")]
            public string romchipserial1 = "";

            [XmlAttribute("romchipserial2")]
            public string romchipserial2 = "";

            [XmlAttribute("lockoutserial")]
            public string lockoutserial = "";

            [XmlAttribute("savechipserial")]
            public string savechipserial = "";

            // NOTE chipserial is depricated and replaced by romchipserial1 and romchipserial2
            //   https://forum.no-intro.org/viewtopic.php?p=28055#p28055
            // [XmlAttribute("chipserial")]
            // public string chipserial = "";

            [XmlAttribute("boxserial")]
            public string boxserial = "";

            [XmlAttribute("mediastamp")]
            public string mediastamp = "";

            [XmlAttribute("boxbarcode")]
            public string boxbarcode = "";

            // NOTE digitalserial/2 is for digital dats (wiiware etc)
            //   https://forum.no-intro.org/viewtopic.php?p=28055#p28055
            [XmlAttribute("digitalserial1")]
            public string digitalserial1 = "";

            [XmlAttribute("digitalserial2")]
            public string digitalserial2 = "";
        }

        public class Source
        {
            [XmlElement("details")]
            public Details details;
            [XmlElement("serials")]
            public Serials serials;
            [XmlElement("rom")]
            public Rom[] rom;
        }

        public class Game
        {
            [XmlAttribute("name")]
            public string name;

            [XmlElement("archive")]
            public Archive archive;

            [XmlElement("flags")]
            public Flags flags;

            [XmlElement("source")]
            public Source source;
        }

        [XmlElement("header")]
        public Header header;

        // https://stackoverflow.com/questions/2006482/xml-serialization-disable-rendering-root-element-of-array
        [XmlElement("game")]
        public Game game;

    }

#pragma warning restore CS1591 // Missing XML comment for publicly visible type


    class NoIntroXmlSerializer
    {
        readonly HeaderEncoder enc = new HeaderEncoder();
        readonly HeaderDecoder dec = new HeaderDecoder();
        readonly bool excludeMissing;

        public NoIntroXmlSerializer(bool excludeMissing = true)
        {
            this.excludeMissing = excludeMissing;
        }

        public void Serialize(Stream output, NoIntroRedumpSources src)
        {
            var ser = new XmlSerializer(typeof(NoIntroXmlScheme1));
            var s = ToScheme(src);
            if (excludeMissing)
                NullEmptyStrings(s);
            ser.Serialize(output, s);
            output.Flush();
        }

        void NullEmptyStrings(object o)
        {
            foreach (var f in o.GetType().GetFields())
            {
                var value = f.GetValue(o);
                if (f.FieldType == typeof(string))
                {
                    if (string.IsNullOrWhiteSpace((string)value))
                    {
                        Log.That("Omitting unfilled " + o.GetType().Name + "." + f.Name);
                        f.SetValue(o, null);
                    }
                }
                else if (f.FieldType.IsArray && value != null)
                {
                    var ar = (Array)value;
                    for (int i = 0; i < ar.Length; ++i)
                        NullEmptyStrings(ar.GetValue(i));
                }
                else
                {
                    if (f.FieldType.IsClass)
                        NullEmptyStrings(value);
                }
            }
        }

        string ToEmpty(string s, string defaultEmpty = "")
        {
            return (string.IsNullOrWhiteSpace(s) || s == Misc.NotAvailable || s == Misc.Unknown) ? defaultEmpty : s;
        }

        string ToEmpty(int index, string[] s, string defaultEmpty = "")
        {
            if (s == null)
                return defaultEmpty;

            if (index >= s.Length)
                return defaultEmpty;

            return ToEmpty(s[index]);
        }

        NoIntroXmlScheme1 ToScheme(NoIntroRedumpSources src)
        {
            return new NoIntroXmlScheme1()
            {
                header = new NoIntroXmlScheme1.Header(),
                game = ToGame(src),
            };
        }

        NoIntroXmlScheme1.Game ToGame(NoIntroRedumpSources src)
        {
            return new NoIntroXmlScheme1.Game()
            {
                name = src.datGame.name,
                archive = ToArchive(src),
                flags = ToFlags(src),
                source = ToSource(src),
            };
        }

        NoIntroXmlScheme1.Archive ToArchive(NoIntroRedumpSources src)
        {
            return new NoIntroXmlScheme1.Archive()
            {
                // that should be enough for redumps
                gameid = src.dbGame.number.ToString("D4"),
            };
        }

        NoIntroXmlScheme1.Flags ToFlags(NoIntroRedumpSources src)
        {
            return new NoIntroXmlScheme1.Flags()
            {
                licensed = 1,
                physical = 1,
                complete = 1,
                pub = 1,
                dat = 1,
            };
        }

        NoIntroXmlScheme1.Source ToSource(NoIntroRedumpSources src)
        {
            Func<string, int> toKnownDate = date => (string.IsNullOrWhiteSpace(date) || date == Misc.NotAvailable || date == Misc.Unknown) ? 0 : 1;

            return new NoIntroXmlScheme1.Source()
            {
                details = new NoIntroXmlScheme1.Details()
                {
                    originalformat = NoIntroConsts.FormatBigEndian,
                    dumpdate = Misc.ToIsoDate(src.bigEndian.file.CreationTime),
                    knowndumpdate = 1,
                    dumper = ToEmpty(src.manualEntry.dumper),
                    tool = ToEmpty(src.manualEntry.tool),
                    comment1 = ToEmpty(""),
                    comment2 = ToEmpty(src.manualEntry.edition == Nfo.DefaultEdition ? "" : src.manualEntry.edition),
                    // TODO no-intro forum thread goes into link1 actually https://wiki.no-intro.org/index.php?title=Source_Convention#Link_1
                    link1 = ToEmpty(""),
                    link2 = ToEmpty(""),
                    region = dec.ToRegionString(src.header.mediaFormat.region, RegionConvention.NoIntro),
                    project = "No-Intro",
                    rominfo = ToEmpty(""),
                },
                rom = new NoIntroXmlScheme1.Rom[]
                {
                    ToRom(src.header, src.bigEndian, NoIntroConsts.FormatBigEndian),
                    ToRom(src.header, src.byteSwapped, NoIntroConsts.FormatByteSwapped),
                },
                serials = new NoIntroXmlScheme1.Serials()
                {
                    mediaserial1 = ToEmpty(0, src.manualEntry.mediaSerial),
                    mediaserial2 = ToEmpty(1, src.manualEntry.mediaSerial),
                    pcbserial = ToEmpty(0, src.manualEntry.pcbSerial),
                    romchipserial1 = ToEmpty(0, src.manualEntry.romChipSerial),
                    romchipserial2 = ToEmpty(1, src.manualEntry.romChipSerial),
                    boxserial = ToEmpty(0, src.manualEntry.boxSerial),
                    boxbarcode = ToEmpty(src.manualEntry.boxBarcode),
                    mediastamp = ToEmpty(src.manualEntry.mediaStamp),

                    lockoutserial = "", // TODO
                    savechipserial = "", // TODO
                },
            };
        }

        NoIntroXmlScheme1.Rom ToRom(Header header, CommonFileInfo nfo, string format)
        {
            return new NoIntroXmlScheme1.Rom()
            {
                crc = nfo.crc32.ToLower(),
                md5 = nfo.md5.ToLower(),
                sha1 = nfo.sha1.ToLower(),
                sha256 = nfo.sha256.ToLower(),
                size = nfo.size,
                // is auto generated by datomatic https://forum.no-intro.org/viewtopic.php?p=28055#p28055
                forcename = ToEmpty(""),
                bad = 0,
                unique = 1,
                format = format,
                extension = format == NoIntroConsts.FormatBigEndian ? NoIntroConsts.ExtensionBigEndian : NoIntroConsts.ExtensionByteSwapped,
                // is just the raw value from header https://forum.no-intro.org/viewtopic.php?p=28055#p28055
                version = enc.Encode(header.version),
                emptydir = 0,
                serial = dec.ToNiceSerial(header.mediaFormat),
                // supposed to be blank https://forum.no-intro.org/viewtopic.php?p=28055#p28055
                date = ToEmpty(""),
            };
        }
    }

}
