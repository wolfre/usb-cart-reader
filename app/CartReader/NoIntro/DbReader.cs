﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CartReader.NoIntro
{
    class DbGameEntry
    {

        public class Dump
        {
            public uint size;
            public string crc32;
            public string md5;
            public string sha1;
            public string sha256;
            public string serial;
        }

        public class TrustedDump
        {
            public string dumper;
            public Dump bigEndian;
            public Dump byteSwapped;
        }

        /// <summary>
        /// The full name, e.g. "007 - The World Is Not Enough (Europe) (En,Fr,De)"
        /// </summary>
        public string fullName;
        /// <summary>
        /// The number, or id of the game, e.g "123"
        /// </summary>
        public uint number;
        /// <summary>
        /// The (short) name of the game, e.g. "007 - The World Is Not Enough"
        /// </summary>
        public string name;

        /// <summary>
        /// The trusted dumps of this game (if any)
        /// </summary>
        public TrustedDump[] trustedDumps;
    }

    class DbReader : IDisposable
    {
        readonly bool debug;
        StreamReader rdr;

        public DbReader(FileInfo txtDb, bool debug = false)
        {
            rdr = new StreamReader(txtDb.FullName, System.Text.Encoding.UTF8);
            this.debug = debug;
        }

        public void Dispose()
        {
            rdr.Dispose();
        }

        class LineRaw
        {
            public string indent;
            public string key;
            public string value;

            public override string ToString()
            {
                return "{i:'" + indent + "',k:" + (key == null ? "null" : "\"" + key + "\"") + ",v:\"" + value + "\",l:" + IndentLevel + ",e:" + Empty + "}";
            }

            public bool Empty
            {
                get
                {
                    return string.IsNullOrWhiteSpace(indent) && string.IsNullOrWhiteSpace(key) && string.IsNullOrWhiteSpace(value);
                }
            }

            public int IndentLevel
            {
                get
                {
                    return indent.Select(c => c == '\t' ? 4 : 1).Sum();
                }
            }
        }

        #region LineReading

        readonly List<LineRaw> buffer = new List<LineRaw>();

        LineRaw ReadNextLine()
        {
            if (buffer.Count > 0)
            {
                var b = buffer[0];
                buffer.RemoveAt(0);
                return b;
            }

            if (rdr.EndOfStream)
                return null;

            var l = rdr.ReadLine();

            // This is a bug in DB export files, they sometimes contain HTML content
            if (l.StartsWith("<br>"))
                return ReadNextLine();

            l.TrimEnd('\n', '\r');

            var parsed = new LineRaw();

            // "Dump date: 2020-09-22 (not confirmed)"
            const string Sep = ": ";
            var kvSepIndex = l.IndexOf(Sep);

            if (kvSepIndex >= 0)
            {
                parsed.key = l.Substring(0, kvSepIndex);
                parsed.value = l.Substring(kvSepIndex + Sep.Length);
                parsed.key = SplitIndent(out parsed.indent, parsed.key).Trim(' ', '\t');
            }
            else
            {
                parsed.value = SplitIndent(out parsed.indent, l);
                parsed.key = null;
            }

            parsed.value = parsed.value.Trim(' ', '\t');

            Trace("R: " + parsed);
            return parsed;
        }



        void UnRead(LineRaw l)
        {
            buffer.Add(l);
        }

        string SplitIndent(out string indent, string raw)
        {
            var clean = raw.TrimStart(' ', '\t');
            indent = raw.Substring(0, raw.Length - clean.Length);
            return clean;
        }

        #endregion

        DbGameEntry.Dump ReadNextDump(LineRaw header)
        {
            Trace(nameof(ReadNextDump) + "(" + header + ")");
            var e = new DbGameEntry.Dump();

            while (true)
            {
                var l = ReadNextLine();
                if (l == null || l.Empty || l.IndentLevel <= header.IndentLevel)
                {
                    UnRead(l);
                    return e;
                }

                if (l.key == "Size")
                    e.size = MustParseU32(l.value);
                else if (l.key == "CRC32")
                    e.crc32 = l.value;
                else if (l.key == "MD5")
                    e.md5 = l.value;
                else if (l.key == "SHA-1")
                    e.sha1 = l.value;
                else if (l.key == "SHA-256")
                    e.sha256 = l.value;
                else if (l.key == "Serial")
                    e.serial = l.value;
                else
                    Skip(l, nameof(ReadNextDump));
            }

        }

        const string TrustedDumpHeader = "Trusted Dump";

        DbGameEntry.TrustedDump ReadNextTrustedDump(LineRaw header)
        {
            Trace(nameof(ReadNextTrustedDump) + "(" + header + ")");
            var e = new DbGameEntry.TrustedDump();

            while (true)
            {
                var l = ReadNextLine();
                if (l == null || l.Empty || l.IndentLevel <= header.IndentLevel)
                {
                    UnRead(l);
                    return e;
                }

                if (l.key == null && l.value == "BigEndian")
                    e.bigEndian = ReadNextDump(l);
                else if (l.key == null && l.value == "ByteSwapped")
                    e.byteSwapped = ReadNextDump(l);
                else if (l.key == "Dumper")
                    e.dumper = l.value;
                else
                    Skip(l, nameof(ReadNextTrustedDump));
            }
        }



        DbGameEntry ReadNextDbGameEntry(LineRaw fullName)
        {
            Trace(nameof(ReadNextDbGameEntry) + "(" + fullName + ")");
            var e = new DbGameEntry()
            {
                fullName = fullName.value,
                trustedDumps = new DbGameEntry.TrustedDump[0],
            };

            while (true)
            {
                var l = ReadNextLine();
                if (l == null || l.Empty || l.IndentLevel == 0)
                {
                    UnRead(l);
                    return e;
                }

                if (l.key == "Number")
                    e.number = MustParseU32(l.value);
                else if (l.key == "Name")
                    e.name = l.value;
                else if (l.key == null && l.value == TrustedDumpHeader)
                    e.trustedDumps = e.trustedDumps.Append(ReadNextTrustedDump(l));
                else
                    Skip(l, nameof(ReadNextDbGameEntry));
            }
        }

        uint MustParseU32(string value)
        {
            uint v;
            if (uint.TryParse(value, out v))
                return v;
            
            throw new ArgumentException("Cannot parse '" + value + "' into a uint");
        }

        void Skip(LineRaw l, string where)
        {
            Trace("Skip (" + where + ") " + l);
        }

        public DbGameEntry ReadNext()
        {
            while (true)
            {
                var l = ReadNextLine();
                if (l == null)
                    return null;

                if (l.IndentLevel == 0 && l.key == null && string.IsNullOrWhiteSpace(l.value) == false)
                {
                    // its a new game (title);
                    return ReadNextDbGameEntry(l);
                }

                Skip(l, nameof(ReadNext));
            }
        }

        void Trace(string l)
        {
            if (debug)
                Log.That(Severity.Debug, l);
        }

    }
}
