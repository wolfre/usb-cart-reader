﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace CartReader.NoIntro
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type

    [XmlRoot(ElementName = "datafile")]
    public class DataFile
    {
        public class Header { }

        public class Rom
        {
            [XmlAttribute("name")]
            public string name;
            [XmlAttribute("size")]
            public uint size;
            [XmlAttribute("crc")]
            public string crc;
            [XmlAttribute("md5")]
            public string md5;
            [XmlAttribute("sha1")]
            public string sha1;
            [XmlAttribute("status")]
            public string status;
            [XmlAttribute("serial")]
            public string serial;
        }

        public class Game
        {
            [XmlAttribute("name")]
            public string name;

            [XmlElement("description")]
            public string description;

            [XmlElement("rom")]
            public Rom[] rom;
        }

        [XmlElement("header")]
        public Header header;

        [XmlElement("game")]
        public Game[] game;
    }

#pragma warning restore CS1591 // Missing XML comment for publicly visible type

    class DatReader
    {
        public DataFile Read(FileInfo datFile)
        {
            var ser = new XmlSerializer(typeof(DataFile));
            using (var s = datFile.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                return (DataFile)ser.Deserialize(s);
        }
    }
}
