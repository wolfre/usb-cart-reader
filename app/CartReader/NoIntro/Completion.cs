﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CartReader.Actions;
using CartReader.Interactive;
using CartReader.N64;

namespace CartReader.NoIntro
{
    class NoIntroRedumpSources
    {
        public DataFile.Game datGame;
        public DbGameEntry dbGame;
        public CommonFileInfo bigEndian;
        public CommonFileInfo byteSwapped;
        public Header header;

        public AdditionalManualInfo manualEntry;

        public class AdditionalManualInfo
        {
            public string dumper;
            public string tool;
            public string edition;

            public string[] mediaSerial = new string[0];
            public string mediaStamp;

            public string[] romChipSerial = new string[0];
            public string[] pcbSerial = new string[0];

            public string[] boxSerial = new string[0];
            public string boxBarcode;
        }
    }

    class DatabaseCompletion
    {
        readonly FileInfo datFile;
        readonly FileInfo dbFile;

        readonly DataFile dat;
        readonly DbGameEntry[] db;

        public DatabaseCompletion(FileInfo datFile, FileInfo dbFile)
        {
            this.datFile = datFile;
            this.dbFile = dbFile;

            var reader = new DatReader();
            dat = reader.Read(datFile);
            Log.That(Severity.Debug, "Read " + dat.game.Length + " games from '" + datFile.FullName + "'");

            var db = new List<DbGameEntry>();

            using (var dbReader = new DbReader(dbFile))
            {
                DbGameEntry entry;
                do
                {
                    entry = dbReader.ReadNext();
                    if (entry != null)
                        db.Add(entry);
                }
                while (entry != null);
            }

            this.db = db.ToArray();
            Log.That(Severity.Debug, "Read " + this.db.Length + " games from '" + dbFile.FullName + "'");
        }

        public NoIntroRedumpSources GetSources(FileInfo rom)
        {
            var rd = new NoIntroRedumpSources()
            {
                header = rom.DecodeN64RomHeader(),
                manualEntry = new NoIntroRedumpSources.AdditionalManualInfo(),
            };

            Log.That(Severity.Info, "Will calculate checksums ...");
            CommonFileInfoGenerator.CalcFileInfo(out rd.bigEndian, out rd.byteSwapped, rom);

            Log.That(Severity.Info, rd.bigEndian, NoIntroConsts.FormatBigEndian);
            Log.That(Severity.Info, rd.byteSwapped, NoIntroConsts.FormatByteSwapped);

            var rd2 = GetSources(rd.bigEndian);
            rd.datGame = rd2.datGame;
            rd.dbGame = rd2.dbGame;
            return rd;
        }

        public NoIntroRedumpSources GetSources(CommonFileInfo bigEndian)
        {
            var rd = new NoIntroRedumpSources()
            {
                datGame = FindGameInDat(bigEndian)
            };

            if (rd.datGame == null)
            {
                Log.That(Severity.Error, "Was not able to find your rom in DAT file, this may be a completely new entry! Only re-dumps are supported at the moment, sorry ...");
                throw new NotSupportedException("Only re-dumps are supported");
            }

            Log.That(Severity.Info, "Found your game in DAT: '" + rd.datGame.name + "'");

            rd.dbGame = FindGameInDb(bigEndian);
            if (rd.dbGame == null)
            {
                Log.That(Severity.Warning, "Was not able to find your game in DB by checksum, this may be an unverified game, will search using title ...");
                rd.dbGame = FindGameInDb(rd.datGame);
                if (rd.dbGame == null)
                {
                    Log.That(Severity.Error, "Was not able to find your game by title either, maybe its a new entry, or your DB file is outdated ...");
                    throw new NotSupportedException("DB is outdated");
                }
            }

            Log.That(Severity.Info, "Found your game in DB: '" + rd.dbGame.fullName + "', ID " + rd.dbGame.number.ToString("D4"));
            return rd;
        }

        DataFile.Game FindGameInDat(CommonFileInfo bigEndian)
        {
            return dat.game.FirstOrDefault(g =>
            {
                if (g.rom == null || g.rom.Length == 0)
                    return false;

                return g.rom.Any(r =>
                {
                    if (r.md5 != null && r.md5.ToLower() == bigEndian.md5.ToLower())
                        return true;
                    if (r.sha1 != null && r.sha1.ToLower() == bigEndian.sha1.ToLower())
                        return true;
                    if (r.crc != null && r.crc.ToLower() == bigEndian.crc32.ToLower())
                        return true;
                    return false;
                });
            });
        }

        DbGameEntry FindGameInDb(CommonFileInfo bigEndian)
        {
            foreach (var entry in db)
            {
                var match = entry.trustedDumps.FirstOrDefault(d =>
                {
                    if (d.bigEndian == null)
                        return false;
                    if (d.bigEndian.md5 != null)
                        return d.bigEndian.md5.ToLower() == bigEndian.md5.ToLower();
                    if (d.bigEndian.sha1 != null)
                        return d.bigEndian.sha1.ToLower() == bigEndian.sha1.ToLower();
                    if (d.bigEndian.crc32 != null)
                        return d.bigEndian.crc32.ToLower() == bigEndian.crc32.ToLower();
                    return false;
                });

                if (match != null)
                    return entry;
            }

            return null;
        }

        DbGameEntry FindGameInDb(DataFile.Game game)
        {
            foreach (var entry in db)
                if (entry.fullName.ToLower() == game.name.ToLower())
                    return entry;

            return null;
        }
    }

    class InteractiveCompletion
    {
        readonly HeaderDecoder dec = new HeaderDecoder();
        readonly Func<IDialog> newDialogEngine;

        public InteractiveCompletion(Func<IDialog> newDialogEngine)
        {
            this.newDialogEngine = newDialogEngine;
        }

        public void CompleteMissing(NoIntroRedumpSources redump)
        {
            if (redump.manualEntry == null)
                redump.manualEntry = new NoIntroRedumpSources.AdditionalManualInfo();

            var m = redump.manualEntry;


            Q(ref m.dumper, "The dumper (that's you)", "Anonymous");
            Q(ref m.tool, "The tool used to create the dump", Misc.ThisTool);
            Q(ref m.edition, ToINeed(Nfo.QEdition), Nfo.DefaultEdition);

            Q(ref m.mediaSerial, ToINeed(Nfo.QPhyMediaSerial));
            Q(ref m.mediaStamp, ToINeed(Nfo.QPhyMediaStamp));

            Q(ref m.romChipSerial, ToINeed(Nfo.QRomChipSerial), GuessRomchipSerial(redump.header));
            Q(ref m.pcbSerial, ToINeed(Nfo.QPcbSerial));

            Q(ref m.boxSerial, ToINeed("The serial on the physical box, e.g. 'P-NWRP-NNOE'"), Misc.Unknown);
            Q(ref m.boxBarcode, ToINeed("The barcode on the physical box, e.g. '045496870034'"), Misc.Unknown);
        }

        public void CompleteMissing(Nfo nfo, Header header, FileInfo romFile)
        {
            Log.That(Severity.Info, "Will now ask you for some additional information (unless you already specified it on the command line)");
            Log.That(Severity.Info, "Have a look over here https://dumping.guide/carts/nintendo/n64 and here https://dumping.guide/submission/nintendo-carts and https://wiki.no-intro.org/index.php?title=Source_Convention for more info on dumping");
            // And here for the outdated version https://wiki.no-intro.org/index.php?title=Nintendo_64_Dumping_Guide
            Log.That(Severity.Info, "If you are unsure on the file name, check here https://wiki.no-intro.org/index.php?title=Naming_Convention and search for your game here https://datomatic.no-intro.org/index.php?page=search&op=datset&s=24");


            Q(ref nfo.dumper, "The dumper (that's you)", "Anonymous");
            Q(ref nfo.affiliation, Nfo.QAffiliation, Nfo.DefaultAffiliation);
            Q(ref nfo.dumpCreationDate, ToINeed(Nfo.QDumpDate), Misc.ToIsoDate(romFile.CreationTime));
            Q(ref nfo.title, ToINeed(Nfo.QTitle), header.name.Trim(' '));
            Q(ref nfo.languages, ToINeed(Nfo.QLang), Misc.Unknown);
            Q(ref nfo.region, ToINeed(Nfo.QRegion), dec.ToRegionString(header.mediaFormat.region, RegionConvention.NoIntro));
            Q(ref nfo.edition, ToINeed(Nfo.QEdition), Nfo.DefaultEdition);

            Q(ref nfo.romRegion, ToINeed(Nfo.QRomRegion), "0x" + header.mediaFormat.region.ToString("x2") + " (" + dec.ToRegionString(header.mediaFormat.region, RegionConvention.NoIntro) + ")");
            Q(ref nfo.romRevision, ToINeed(Nfo.QRomRevision), dec.ToNiceVersion(header.version));
            Q(ref nfo.romSerial, ToINeed(Nfo.QRomSerial), dec.ToNiceSerial(header.mediaFormat));

            Q(ref nfo.physicalMediaSerial, ToINeed(Nfo.QPhyMediaSerial), Misc.Unknown);
            Q(ref nfo.physicalMediaStamp, ToINeed(Nfo.QPhyMediaStamp), Misc.Unknown);
            Q(ref nfo.pcbSerial, ToINeed(Nfo.QPcbSerial), Misc.Unknown);
            Q(ref nfo.romChipSerial, ToINeed(Nfo.QRomChipSerial), GuessRomchipSerial(header));
        }

        static string ToINeed(string what)
        {
            return "I need " + what.Substring(0, 1).ToLower() + what.Substring(1);
        }

        static string GuessRomchipSerial(Header h)
        {
            return "NUS-" + ToN64String(h.mediaFormat.type) + h.mediaFormat.id + ToN64String(h.mediaFormat.region) + "-" + h.version.minor;
        }

        static string ToN64String(byte b)
        {
            return N64Consts.RomHeaderCodePage.GetString(new byte[] { b });
        }

        void Q(ref string[] value, string what)
        {
            var eng = newDialogEngine();
            eng.TrimSpacesFromAnswer = true;

            value = eng.AskOneLineMultiEntryText(value, what, DialogConfirmation.ConfirmationDefaultYes);
        }

        void Q(ref string[] value, string what, string defaultValue)
        {
            if (value == null || value.Length == 0)
            {
                value = new string[] { defaultValue };
                Q(ref value, what);
            }
        }

        void Q(ref string value, string what)
        {
            value = newDialogEngine().AskOneLineText(value, what, DialogConfirmation.ConfirmationDefaultYes);
        }

        void Q(ref string value, string what, string defaultValue)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                value = defaultValue;
                Q(ref value, what);
            }
        }
    }

}
