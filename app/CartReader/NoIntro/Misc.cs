﻿using System;
namespace CartReader.NoIntro
{
    static class Misc
    {
        public const string Unknown = "unknown";
        public const string NotAvailable = "n/a";

        public static string ToIsoDate(DateTime t)
        {
            return t.ToString("yyyy-MM-dd");
        }

        public static string ThisTool
        {
            get { return About.LongName + " v" + BuildInfo.VersionSemantic + " (" + BuildInfo.CommitShort + ")"; }
        }
    }
}
