﻿using System;
namespace CartReader.NoIntro
{
    static class NoIntroConsts
    {
        public const string FormatBigEndian = "BigEndian";
        public const string FormatByteSwapped = "ByteSwapped";

        public const string ExtensionBigEndian = "z64";
        // no-intro uses "v64" as swapped extension
        public const string ExtensionByteSwapped = "v64";
    }
}
