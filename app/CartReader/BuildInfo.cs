﻿// This will be filled in by build script from CI pipeline, do not edit
static class BuildInfo
{
    public const string Commit = "0000000000000000000000000000000000000000";
    public const string CommitShort = "00000000";
    public const string BuildJob = "https://example.com/job/1234";
    public const string BuildTime = "0000-00-00T00:00:00+00:00";

    public const string VersionMajor = "0";
    public const string VersionMinor = "0";
    public const string VersionPatch = "0";

    public const string VersionSemantic = "0.0.0";
}
