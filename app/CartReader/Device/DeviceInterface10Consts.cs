﻿using System;
namespace CartReader.Device
{
    /// <summary>
    /// Usb interface v1.0 consts, basically adapted from stm32f1/n64-usb-protocol.h
    /// </summary>
    static class DeviceInterface10Consts
    {
        public static readonly TimeSpan TimeOut = TimeSpan.FromSeconds(0.5);

        public const int PacketSize = 64; // That's the max USB 2.0 FullSpeed packet size
        public const int UsbInterfaceNumber = 0;
        public const byte EndPointCommand = 1;
        public const byte EndPointData = 2;

        public const byte StatusOk = 1;
        public const byte StatusFail = 2;
        public const byte StatusUnknownCmd = 3;

        public const byte CommandDeviceInfo = 0;
        public const byte CommandBufferRead = 1;
        public const byte CommandBufferWrite = 2;
        public const byte CommandBusRead = 3;
        public const byte CommandBusWrite = 4;
        public const byte CommandReadEeprom = 5;
    }
}
