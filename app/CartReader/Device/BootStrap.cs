﻿using System;
namespace CartReader.Device
{
    static class BootStrap
    {
        public static IDeviceInterface10 Device(ILowLevelCom lowLevel)
        {
            var vStr = "v" + lowLevel.VersionMajor + "." + lowLevel.VersionMinor;

            if (lowLevel.VersionMajor != 1)
                throw new NotSupportedException("I don't support USB interface " + vStr);

            if (lowLevel.VersionMinor > 0)
                Log.That(Severity.Warning, "The dumper firmware claims to implement USB interface " + vStr + " but we only support up to 1.0 ... will continue, but you should check for an update");

            return new DeviceInterface10(lowLevel);
        }
    }
}
