﻿using System;
namespace CartReader.Device
{
    interface ILowLevelCom
    {
        /// <summary>
        /// Announced major device interface version from usb descriptor
        /// </summary>
        /// <value>Version major</value>
        byte VersionMajor { get; }

        /// <summary>
        /// Announced minor device interface version from usb descriptor
        /// </summary>
        /// <value>Version minor</value>
        byte VersionMinor { get; }

        /// <summary>
        /// Does a command exchange with the device.
        /// Throws if command failed.
        /// </summary>
        /// <param name="command">Command.</param>
        /// <param name="argumets">Argumets.</param>
        void DoCommand(byte command, byte[] argumets = null);

        /// <summary>
        /// Sends data to the device internal buffer.
        /// </summary>
        /// <param name="data">Data to send</param>
        /// <param name="chunksize">USB packet size</param>
        void SendToBuffer(byte[] data, int chunksize);

        /// <summary>
        /// Reads from buffer
        /// </summary>
        /// <returns>Buffer data</returns>
        /// <param name="len">Length of data to read, can be shorter than USB packet size</param>
        byte[] ReadFromBuffer(int len);
    }
}
