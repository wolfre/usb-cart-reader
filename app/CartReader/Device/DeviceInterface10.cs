﻿using System;
using System.Linq;

namespace CartReader.Device
{
    class DeviceInterface10 : IDeviceInterface10
    {
        readonly ILowLevelCom lowLevel;
        readonly DeviceInterface10Codec codec = new DeviceInterface10Codec();

        public DeviceInterface10(ILowLevelCom lowLevel)
        {
            this.lowLevel = lowLevel;

            lowLevel.DoCommand(DeviceInterface10Consts.CommandDeviceInfo);
            var nfoRaw = lowLevel.ReadFromBuffer(DeviceInterface10Consts.PacketSize);

            Info = codec.DecodeDeviceInfo(nfoRaw);
            Log.That(Severity.Info, "Device info:");
            codec.Nice(l => Log.That(Severity.Info, "  " + l), Info);
        }

        public DeviceInfo10 Info { get; private set; }

        public void CommandBufferRead()
        {
            lowLevel.DoCommand(DeviceInterface10Consts.CommandBufferRead);
        }

        public void CommandBufferWrite()
        {
            lowLevel.DoCommand(DeviceInterface10Consts.CommandBufferWrite);
        }

        public void CommandBusRead(uint address, ushort words, ushort stride)
        {
            if (stride < 1)
                throw new ArgumentOutOfRangeException(nameof(stride) + " " + stride);
            if (words < 1 || words > (Info.bufferSize / 2))
                throw new ArgumentOutOfRangeException(nameof(words) + " " + words);

            lowLevel.DoCommand(DeviceInterface10Consts.CommandBusRead,
                codec.Encode(address)
                .Concat(codec.Encode(words))
                .Concat(codec.Encode(stride))
                .ToArray());
        }

        public void CommandBusWrite(uint address, ushort words, ushort stride)
        {
            if (stride < 1)
                throw new ArgumentOutOfRangeException(nameof(stride) + " " + stride);
            if (words < 1 || words > (Info.bufferSize / 2))
                throw new ArgumentOutOfRangeException(nameof(words) + " " + words);

            lowLevel.DoCommand(DeviceInterface10Consts.CommandBusWrite,
                codec.Encode(address)
                .Concat(codec.Encode(words))
                .Concat(codec.Encode(stride))
                .ToArray());
        }

        public void CommandReadEeprom(byte word)
        {
            lowLevel.DoCommand(DeviceInterface10Consts.CommandReadEeprom, new byte[] { word });
        }

        public byte[] ReadFromBuffer(int len)
        {
            if (len > Info.bufferSize || len < 1)
                throw new ArgumentOutOfRangeException(nameof(len) + " " + len);

            return lowLevel.ReadFromBuffer(len);
        }

        public void SendToBuffer(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            if (data.Length > Info.bufferSize || data.Length < 1)
                throw new ArgumentOutOfRangeException(nameof(data) + ".Length " + data.Length);

            lowLevel.SendToBuffer(data, DeviceInterface10Consts.PacketSize);
        }
    }
}
