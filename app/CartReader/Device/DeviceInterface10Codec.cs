﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CartReader.Device
{
    class DeviceInfo10
    {
        public ushort bufferSize;
        public byte[] gitCommit;
        public byte[][] unknownEntries;
    }

    class DeviceInterface10Codec
    {
        const int GitCommitSize = 20;

        public byte[] Encode(uint u32)
        {
            var addressArg = new byte[4];

            addressArg[0] = (byte)((u32 >> 24) & 0xff);
            addressArg[1] = (byte)((u32 >> 16) & 0xff);
            addressArg[2] = (byte)((u32 >> 8) & 0xff);
            addressArg[3] = (byte)((u32 >> 0) & 0xff);

            return addressArg;
        }

        public byte[] Encode(ushort u16)
        {
            var addressArg = new byte[2];

            addressArg[0] = (byte)((u16 >> 8) & 0xff);
            addressArg[1] = (byte)((u16 >> 0) & 0xff);

            return addressArg;
        }

        public ushort DecodeU16(byte[] u16)
        {
            if (u16.Length < 2)
                throw new ArgumentOutOfRangeException(nameof(u16) + ".Length " + u16.Length);

            var n = (ushort)u16[0];
            n <<= 8;
            n |= u16[1];

            return n;
        }

        public DeviceInfo10 DecodeDeviceInfo(byte[] nfoResponse)
        {
            var fields = DecodeFields(nfoResponse);

            int next = 0;
            Func<int, byte[]> getNext = (expected) => 
            {
                if (next >= fields.Length)
                    throw new Exception("Cannot decode device info, field " + next + " is missing");

                if (fields[next].Length != expected)
                    throw new Exception("Cannot decode device info, field " + next + " has " + fields[next].Length + " bytes, but was exptected to have " + expected);

                return fields[next++];
            };

            var nfo = new DeviceInfo10();
            nfo.bufferSize = DecodeU16(getNext(sizeof(ushort)));
            nfo.gitCommit = getNext(GitCommitSize);
            nfo.unknownEntries = fields.Skip(next).ToArray();
            return nfo;
        }

        byte[][] DecodeFields(byte[] nfoResponse)
        {
            var f = new List<byte[]>();

            var off = 0;
            while (off < nfoResponse.Length)
            {
                var l = (int)nfoResponse[off];
                ++off;

                // Premature end
                if (l == 0)
                    return f.ToArray();

                if (l > (nfoResponse.Length - off))
                {
                    Log.That(Severity.Error, "Length of field " + f.Count + " was too big (" + l + ") will treat everything as one field");
                    l = nfoResponse.Length - off;
                }

                var field = new byte[l];
                Array.Copy(nfoResponse, off, field, 0, field.Length);
                f.Add(field);
                off += field.Length;
            }

            return f.ToArray();
        }

        public void Nice(Action<string> writeLine, DeviceInfo10 nfo)
        {
            writeLine("Internal buffer size: " + nfo.bufferSize);
            writeLine("Firmware build git commit: " + nfo.gitCommit.Hex(""));

            if (nfo.unknownEntries != null)
                foreach (var u in nfo.unknownEntries)
                    writeLine("Additional unknown entry: " + u.Hex());
        }
    }
}
