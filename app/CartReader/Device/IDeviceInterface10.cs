﻿using System;
namespace CartReader.Device
{
    interface IDeviceInterface10
    {
        DeviceInfo10 Info { get; }

        void CommandBufferWrite();
        void CommandBufferRead();
        void CommandBusRead(uint address, ushort words, ushort stride);
        void CommandBusWrite(uint address, ushort words, ushort stride);
        void CommandReadEeprom(byte word);

        /// <summary>Sends data to the device internal buffer.</summary>
        /// <param name="data">Data to send</param>
        void SendToBuffer(byte[] data);
        /// <summary>
        /// Reads from buffer, len must be a multiple of <see cref="DeviceInterface10Consts.PacketSize"/>
        /// </summary>
        /// <returns>Buffer data</returns>
        /// <param name="len">Length</param>
        byte[] ReadFromBuffer(int len);
    }
}
