﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CartReader.Device
{
    class LowLevelCom : ILowLevelCom
    {
        readonly Usb.IUsbInterface dumperInterface;
        readonly byte commandEndPoint;
        readonly byte dataEndPoint;
        readonly TimeSpan timeout;

        public LowLevelCom(Usb.IUsbInterface dumperInterface)
        {

            this.dumperInterface = dumperInterface;

            // TODO check actual interface version in device descriptor
            VersionMajor = 1;
            VersionMinor = 0;
            dataEndPoint = DeviceInterface10Consts.EndPointData;
            commandEndPoint = DeviceInterface10Consts.EndPointCommand;
            timeout = DeviceInterface10Consts.TimeOut;
        }

        public byte VersionMajor { get; private set; }

        public byte VersionMinor { get; private set; }

        static readonly Dictionary<byte, string> cmdName = new Dictionary<byte, string>()
        {
            { DeviceInterface10Consts.CommandDeviceInfo, "device info" },
            { DeviceInterface10Consts.CommandBufferRead, "buffer read" },
            { DeviceInterface10Consts.CommandBufferWrite, "buffer write" },
            { DeviceInterface10Consts.CommandBusRead, "bus read" },
            { DeviceInterface10Consts.CommandBusWrite, "bus write" },
            { DeviceInterface10Consts.CommandReadEeprom, "EEPROM read" },
        };

        public void DoCommand(byte command, byte[] argumets = null)
        {
            var cmd = new byte[(argumets == null ? 0 : argumets.Length) + 1];
            cmd[0] = command;
            if (argumets != null)
                Array.Copy(argumets, 0, cmd, 1, argumets.Length);

            if (cmd.Length > DeviceInterface10Consts.PacketSize)
                throw new ArgumentException("Attempted to write command " + command + " with " + cmd.Length + " bytes");

            SendOrDie(commandEndPoint, cmd);
            var resp = dumperInterface.BulkInTransfer(commandEndPoint, timeout);
            if (resp.Length < 1)
                throw new IOException("Received garbled response");

            if (resp[0] == DeviceInterface10Consts.StatusOk)
                return;

            var cmdNice = cmdName.ContainsKey(command) ? cmdName[command] : "0x" + command.Hex();

            if (resp[0] == DeviceInterface10Consts.StatusFail)
                throw new IOException("Command " + cmdNice + " failed");

            if (resp[0] == DeviceInterface10Consts.StatusUnknownCmd)
                throw new IOException("Command " + cmdNice + " failed because it was unknown to the device");

            throw new IOException("Command " + cmdNice + " failed with an unknown status: " + resp[0]);
        }

        public byte[] ReadFromBuffer(int len)
        {
            if (len < 1)
                throw new ArgumentOutOfRangeException(nameof(len) + " " + len);

            var data = new byte[len];
            var off = 0;
            while (off < data.Length)
            {
                var next = dumperInterface.BulkInTransfer(dataEndPoint, timeout);

                var read = data.Length - off;
                if (read > next.Length)
                    read = next.Length;

                Array.Copy(next, 0, data, off, read);
                off += read;
            }

            return data;
        }

        public void SendToBuffer(byte[] data, int chunksize)
        {
            if (chunksize < 1 || chunksize > DeviceInterface10Consts.PacketSize)
                throw new ArgumentOutOfRangeException(nameof(chunksize) + " " + chunksize);

            var buf = new byte[chunksize];
            var off = 0;

            while (off < data.Length)
            {
                var len = data.Length - off;
                if (len > buf.Length)
                    len = buf.Length;

                Array.Copy(data, off, buf, 0, len);

                if (len == buf.Length)
                {
                    SendOrDie(dataEndPoint, buf);
                }
                else
                {
                    SendOrDie(dataEndPoint, buf.Take(len).ToArray());
                }

                off += len;
            }
        }

        void SendOrDie(byte endpoint, byte[] data)
        {
            var sent = dumperInterface.BulkOutTransfer(endpoint, data, timeout);
            if (sent != data.Length)
                throw new IOException("Wanted to send " + data.Length + " bytes to endpoint " + endpoint + " but only sent " + sent);
        }
    }
}
