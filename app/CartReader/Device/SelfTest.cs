﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CartReader.Device
{
    class SelfTest
    {
        readonly ILowLevelCom lowLevel;

        public SelfTest(ILowLevelCom lowLevel)
        {
            this.lowLevel = lowLevel;
        }

        public void DoIt()
        {
            var device = BootStrap.Device(lowLevel);
            var testDataSend = GenTestData(device.Info.bufferSize);

            // Do tests with various chunk sizes
            foreach (var chunksize in new int[] { DeviceInterface10Consts.PacketSize, 16, 4 })
                TestChunkedReadWrite(device, testDataSend, chunksize);

            TestUnmappedBusRead(device, testDataSend);
            TestUnmappedBusWrite(device);
            TestEepromMissingRead(device, testDataSend);
            TestReadSpeed(device);
        }

        void TestReadSpeed(IDeviceInterface10 device)
        {
            int rx = 0;

            var start = DateTime.Now;
            while (DateTime.Now - start < TimeSpan.FromSeconds(5))
            {
                device.CommandBusRead(N64.N64Consts.UnMapped, (ushort)(device.Info.bufferSize / 2), N64.N64Consts.StrideWordsRom);
                var buf = device.ReadFromBuffer(device.Info.bufferSize);
                rx += buf.Length;
            }

            var time = DateTime.Now - start;
            var bytePerSecond = (double)(rx) / time.TotalSeconds;

            // NOTE there's not real lower limit, but around 10k sounds like we have real troble with our setup ...
            if (bytePerSecond < 10000)
                throw new Exception("Read speed was very slow (" + bytePerSecond + " byte/s)");

            Log.That(Severity.Info, "Pass read speed test (" + (bytePerSecond / 1024.0).ToString("F2") + " kbyte/s)");
        }

        void TestEepromMissingRead(IDeviceInterface10 device, byte[] testDataSend)
        {
            // Fill buffer with none-0 data
            device.CommandBufferWrite();
            device.SendToBuffer(testDataSend);
            device.CommandReadEeprom(0);
            // Don't issue read command, it should auto switch to read
            AssertAllZero(device.ReadFromBuffer(device.Info.bufferSize).Take(N64.N64Consts.EepromWordSizeBytes));
            AssertNoData(device);
            Log.That(Severity.Info, "Pass 0-eeprom read");
        }

        void TestUnmappedBusWrite(IDeviceInterface10 device)
        {
            // write the all 0 buffer to a non-mapped address
            device.CommandBusWrite(N64.N64Consts.UnMapped, (ushort)(device.Info.bufferSize / 2), N64.N64Consts.StrideWordsRom);
            Log.That(Severity.Info, "Pass bus write");
        }

        void TestUnmappedBusRead(IDeviceInterface10 device, byte[] testDataSend)
        {
            // Fill buffer with none-0 data
            device.CommandBufferWrite();
            device.SendToBuffer(testDataSend);
            // Read a full buffer from a non-mapped address, which should result in all 0
            device.CommandBusRead(N64.N64Consts.UnMapped, (ushort)(device.Info.bufferSize / 2), N64.N64Consts.StrideWordsRom);
            // Don't issue read command, it should auto switch to read
            AssertAllZero(device.ReadFromBuffer(device.Info.bufferSize));
            AssertNoData(device);
            Log.That(Severity.Info, "Pass 0-bus read");
        }

        void TestChunkedReadWrite(IDeviceInterface10 device, byte[] testDataSend, int chunksize)
        {
            // Simple read / write buffer test

            device.CommandBufferWrite();
            lowLevel.SendToBuffer(testDataSend, chunksize);

            device.CommandBufferRead();
            var testDataRead = device.ReadFromBuffer(testDataSend.Length);

            for (int i = 0; i < testDataSend.Length; ++i)
                if (testDataSend[i] != testDataRead[i])
                    throw new Exception("Data at " + i + " differs (" + testDataRead[i].Hex() + " != " + testDataSend[i].Hex() + ")");

            AssertNoData(device);
            Log.That(Severity.Info, "Pass read/write with chunk size " + chunksize);
        }

        static byte[] GenTestData(int len)
        {
            var testDataSend = new byte[len];
            for (int i = 0; i < testDataSend.Length; ++i)
                testDataSend[i] = (byte)(i % 251);
            return testDataSend;
        }

        static void AssertAllZero(IEnumerable<byte> dummy)
        {
            var dummyAr = dummy.ToArray();
            if (dummyAr.Any(e => e != 0))
                throw new IOException("Expected all-0 dummy block, but got " + dummyAr.Hex());
        }

        void AssertNoData(IDeviceInterface10 device)
        {
            byte[] resp;

            try
            {
                resp = device.ReadFromBuffer(DeviceInterface10Consts.PacketSize);
            }
            catch (Exception)
            {
                return;
            }

            throw new IOException("Did receive data: " + resp.Hex());
        }

    }
}
