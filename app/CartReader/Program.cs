﻿using System;
using System.IO;
using System.Reflection;

namespace CartReader
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            try
            {
                var vParser = new Binah.Parsing.ValueParser();
                vParser.Extend(typeof(Actions.Address), Actions.Address.ParseCliArg);

                var cmdParser = new Binah.CommandLineParser(vParser);

                var app = new Binah.ActionApp(cmdParser, Console.WriteLine)
                {
                    AppInfo = About.Info,
                    AppName = About.LongName + " v" + BuildInfo.VersionSemantic,
                };

                app.Add(new Actions.DumpRom());
                app.Add(new Actions.DumpSram());
                app.Add(new Actions.DumpEeprom());
                app.Add(new Actions.DumpFram());
                app.Add(new Actions.AutoDetectRomSize());
                app.Add(new Actions.RomInfo());
                app.Add(new Actions.NoIntroXml());
                app.Add(new Actions.SelfTest());
                app.Add(new Actions.ShowBuildInfo());

                app.Run(args);
            }
            catch (TargetInvocationException x)
            {
                Log.That(x.InnerException);
                Environment.ExitCode = -1;
            }
            catch (Exception x)
            {
                Log.That(x);
                Environment.ExitCode = -1;
            }
        }

    }
}
