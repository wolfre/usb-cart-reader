# USB N64 Cart Reader

I know, what a creative name right?! :rofl:

[![pipeline status](https://gitlab.com/wolfre/usb-cart-reader/badges/master/pipeline.svg)](https://gitlab.com/wolfre/usb-cart-reader/-/commits/master)

An open source, Nintendo 64 cartridge and save file reader with a focus on clean structure and documentation.
Features include:
- Reads ROMs of all sizes to files on your PC.
- Reads SRAM saves in any size to files on your PC.
- Reads FRAM saves in any size to files on your PC.
  - Supported FRAM chips are: MX29L1100, 29L1100KC-15B0, MX29L1101, 29L1101KC-15B0, MN63F81MPN
- Reads EEPROM saves of any size to files on your PC.
- Supports arbitrary reads from any bus address and with any length. 
  (Useful to poke around the entire 32 Bit address space.)
- Almost the entire logic is on the host PC side, which allows for quick and easy tinkering with chips in carts.
- Generates an info file from your dump, ready to submit to [no-intro.org][no-intro] to help preservation.

**This project would not have been possible without prior work by:**
- The folks of [crazynation and icequake](http://n64.icequake.net/mirror/www.crazynation.org/N64/).
- [sanni](https://github.com/sanni)
  and
  [everyone else](https://forum.arduino.cc/t/rom-reader-for-super-nintendo-super-famicom-game-cartridges/155260/126)
  who contributed to his
  [excellent cartreader project](https://github.com/sanni/cartreader).
- [Robin Jones and everyone who worked on Altra64](https://github.com/networkfusion/altra64).
- [Aaron Wilcott][cart-save-list] for his list on N64 cartridge save types.
- Everyone behind [Mupen64Plus](https://github.com/mupen64plus)
- And a lot more, see references in the code!


```
This project is in no way whatsoever associated with, or endorsed by Nintendo.
Piracy harms consumers as well as legitimate developers, publishers and retailers. 
The goal of this project is education and documentation of video game history.
It should also enhance the experience of playing your legally purchased games!
```

## How did we get here

I'm very much interested in (playing) Nintendo 64 games and also HW/SW development.
Just recently I installed the [Pixel FX N64 Digital](https://www.pixelfx.co/) into my (launch day) N64 :grin:.
And with this I was going about playing through my collection again.
That's when I noticed that I may have a game that has not yet been independently verified on [no-intro][no-intro]!
So I got the idea to build an N64 cart reader to contribute to the database.
Of course I came across sanni's excellent project and a some more research later I was intrigued to build one from scratch.
Mostly for the fun of doing it and learning much in the process.
But also the existing documentation on the whole topic was sparse and so I hoped to contribute by writing the things down that I found.

## How does it work

The dumper is based around an [STM32f1 "Blue-Pill" board](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill.html) 
that can be cheaply bought, and fully programmed with OSS.
The STM32 is connected via USB to a host PC running a dumper program.

On the [PC side everything is written C#][self-pcapp-readme], targeting regular .NET Framework.
This means it easily runs under Linux, Mac and Windows.
(Note currently only libusb 1.0 is supported for USB communication, which is available for all major operating systems).
The [firmware for the STM32][self-stm32-readme] side is written in plain c, and focuses only on the low level bus / EEPROM things.
All the logic of dumping ROMs, talking to FRAM, verifing things is entirely on the host PC side.
This keeps the (unsafe) c-code short and simple and re-flashing the STM32 should be rarely needed.
As the complex logic is on the host side it is easy to develop / try new things.

The goal is of course preservation, and so clean structure and reliability is key.
Speed is only a secondary goal, currently ROMs can be read with about 60-80 KByte/s.
The exact speed depends on your PCs USB stack, but a 64 MBit cart takes roughly 2 minutes.


## Usage example

Run a selftest to check that communication with the USB device is fine (no cartridge needed):

```bash
CartReader.exe self-test
```

Reading a 64 Mbit cartridge to a BigEndian z64 file on your PC:

```bash
CartReader.exe dump-rom --output-file my-rom.z64 --size-mbit 64
```

Reading the 3 save types to files on your PC (find out what save type your game has [over here][cart-save-list]):

```bash
CartReader.exe dump-sram   --output-file my-save.srm
CartReader.exe dump-eeprom --output-file my-save.eep --eeprom-words 64
CartReader.exe dump-fram   --output-file my-save.fla
```

Produce an info file from your dump (this does not require the hardware, just the dumped file):

```bash
CartReader.exe rom-info --rom-file my-rom.z64 --info-file my-rom.nfo
```

Reading 1 MBit from an arbitrary bus address to a file on your PC:

```bash
CartReader.exe dump-rom --output-file test-read.bin --size-mbit 1 --ignore-wrong-header --rom-base 0x12345678
```

Get lots of help for all the options:

```bash
CartReader.exe --help
```

## Building one

To get one yourself, you have to:

- Build [the hardware][self-hw-build].
- Download the [pre-build host PC app and Fw](https://gitlab.com/wolfre/usb-cart-reader/-/releases) and [flash the firmware onto the STM32][self-stm32-readme-flash]
  - Optionally you can ofcourse build the app and firmware yourself. 
    Have a look into the [CI pipeline to see how its done][self-ci-yaml].
    Then have a look at the [the firmware][self-stm32-readme] and [host PC app][self-pcapp-readme] readmes for more info.


## Roadmap

See also [milestones](https://gitlab.com/wolfre/usb-cart-reader/-/milestones).
Things to do, in rough order of importance.

- Proper USB subsystem to support Windows and Mac and find adapter by serial (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/3))
- Write a document with info on bus, and some preliminary EEPROM findings (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/4))
- Write a HW build guide (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/6))
  - Pinout
  - Notes on 2.54 mm pitch sockets vs 2.50 mm pitch
- Improve / fix EEPROM read to be more stable (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/7))
  - Extend documentation on EEPROM things
- SRAM write support (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/8))
- FRAM write support (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/9))
- EEPROM write support (see [issue](https://gitlab.com/wolfre/usb-cart-reader/-/issues/10))


[cart-save-list]: http://micro-64.com/database/gamesave.shtml
[self-hw-build]: build-hw.md
[self-stm32-readme]: stm32f1
[self-stm32-readme-flash]: stm32f1/README.md#flashing-it
[self-pcapp-readme]: app
[self-ci-yaml]: .gitlab-ci.yml
[no-intro]: https://no-intro.org
