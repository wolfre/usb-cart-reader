#ifndef _N64_H
#define _N64_H

#include <stdint.h>

// Call this once to setup things
void n64_init();

// Will read data from the cart bus,  word_count  16 Bit words, starting from  address  into  buffer, MSB first.
// The  stride_length  controls after how many words the address will be re-setup.
void n64_read_bus_data(uint32_t address, uint16_t word_count, uint8_t* buffer, uint16_t stride_length);

// Will write data to the cart bus,  word_count  16 Bit words, starting from  address  into  buffer, MSB first.
// The  stride_length  controls after how many words the address will be re-setup.
void n64_write_bus_data(uint32_t address, uint16_t word_count, uint8_t* buffer, uint16_t stride_length);

// Reads one EEPROM word at  word_index  into buffer, can address a maximum of 256 words (16Kbit)
void n64_read_eeprom_word(uint8_t word_index, uint8_t* buffer);


#endif
