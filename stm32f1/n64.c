#include <stm32f1xx.h>
#include "n64.h"
#include "n64-ll.h"
#include "dbg.h"

// Generally see here
// http://n64.icequake.net/mirror/www.crazynation.org/N64/
// https://os.mbed.com/users/hudakz/code/STM32F103C8T6_Hello/


void n64_init()
{
    n64_ll_init();
}

static void control_lines_hi()
{
    n64_ll_read_hi();
    n64_ll_write_hi();
    n64_ll_ael_h_hi();
    n64_ll_ael_l_hi();
}

static void address_setup(uint32_t address)
{
    uint16_t address_h = (address >> 16) & 0xffff;
    uint16_t address_l = address & 0xffff;
    
    // NOTE We give quite a bit more time for address setup than would actually be needed.
    // We're trying to be on the safe side and not rush things here :)
    
    control_lines_hi();
    n64_ll_delay_500ns();
    n64_ll_delay_500ns();
    
    n64_ll_address_data_output();
    
    n64_ll_address_data_write(address_h);
    n64_ll_delay_500ns();
    n64_ll_ael_h_low();
    n64_ll_delay_500ns();
    
    n64_ll_address_data_write(address_l);
    n64_ll_delay_500ns();
    n64_ll_ael_l_low();
    
    // After done setting up the address we seem to need at least 1us before read or write begins:
    // http://n64.icequake.net/mirror/www.crazynation.org/N64/
    // So we just go with 1500ns here to be safe
    n64_ll_delay_500ns();
    n64_ll_delay_500ns();
    n64_ll_delay_500ns();
}

void n64_read_bus_data(uint32_t address, uint16_t word_count, uint8_t* buffer, uint16_t stride_length)
{
    for(uint16_t i=0; i<word_count; ++i)
    {
        if( i % stride_length == 0 )
        {
            address_setup(address + (uint32_t)(i*2));
            n64_ll_address_data_input();
            n64_ll_delay_500ns();
        }
        
        // NOTE the response time from the ROM could be around 300ns, but we just 500ns to be on the safe side.
        n64_ll_read_low();
        n64_ll_delay_500ns();
        
        uint16_t tmp = n64_ll_address_data_read();
        uint16_t off = i*2;
        buffer[off] = (tmp >> 8) & 0xff;
        buffer[off+1] = tmp & 0xff;
        
        n64_ll_read_hi();
        n64_ll_delay_100ns();   
    }
    
    control_lines_hi();
}

void n64_write_bus_data(uint32_t address, uint16_t word_count, uint8_t* buffer, uint16_t stride_length)
{
    for(uint16_t i=0; i<word_count; ++i)
    {
        if( i % stride_length == 0 )
        {
            address_setup(address + (uint32_t)(i*2));
        }
        
        uint16_t off = i*2;
        uint16_t d = buffer[off];
        d <<= 8;
        d |= buffer[off+1];
        
        n64_ll_address_data_write(d);
        n64_ll_delay_100ns();
        
        // NOTE the timinng could be around 300ns, but we just use 1000ns to be on the safe side.
        n64_ll_write_low();
        n64_ll_delay_500ns();
        n64_ll_delay_500ns();
        
        n64_ll_write_hi();
    }
    
    n64_ll_address_data_input();
    control_lines_hi();
}

// <eeprom>

// This whole section is much inspired by 
// http://n64devkit.square7.ch/pro-man/pro26/26-06.htm
// https://github.com/sanni/cartreader/blob/d8cb1246f8aa9504f6de271730a8a451117fe138/Cart_Reader/N64.ino#L1920

// This should be more than 15ms for writing
// http://n64devkit.square7.ch/pro-man/pro26/26-06.htm
#define _idle_clock_pulses  1000
#define _eeprom_500ns_delay 2

static void serial_data_clock_pulse(uint16_t pulses)
{
    // This is probably around 1.6 MHz in a real console:
    // pin 19 overe here http://n64.icequake.net/mirror/www.crazynation.org/N64/
    // However it seems this is not an entirely sync bus thing as there are
    // occasional read errors. The below timinng was hand tuned to give good
    // results. But be prepared to double check the things you've read!
    
    for(uint16_t i=0; i<pulses; ++i)
    {
        for(int i=0; i<_eeprom_500ns_delay; ++i)
            n64_ll_delay_500ns();
        
        n64_ll_clk_low();
        
        for(int i=0; i<_eeprom_500ns_delay; ++i)
            n64_ll_delay_500ns();
        
        n64_ll_clk_hi();
    }
}

static void serial_data_send(uint8_t data)
{
    for (uint8_t i = 0; i < 8; i++) 
    {
        n64_ll_sdata_pull();

        // We send out MSB first, each bit is 8 clock cycles.
        uint16_t low_time = (data & 0x80) ? 2 : 6;
        uint16_t high_time = (data & 0x80) ? 6 : 2;

        serial_data_clock_pulse(low_time);
        n64_ll_sdata_release();
        serial_data_clock_pulse(high_time);

        // Shift to next bit
        data <<= 1;
    }
}

static void serial_data_stop()
{
    n64_ll_sdata_pull();
    serial_data_clock_pulse(2);
    n64_ll_sdata_release();
    serial_data_clock_pulse(4);
}

static uint8_t wait_for_sdata_state(uint8_t state)
{
    uint8_t dead = 0;
    while( n64_ll_sdata_state() != state)
    {
        serial_data_clock_pulse(1);
        
        // NOTE this is a rather long timeout, as bits are normally 8 clock cycles in length
        ++dead;
        if( dead == 0xff)
            return 1;
    }
    
    return 0;
}

static uint8_t serial_data_read()
{
    uint8_t d = 0;
    
    for(int i=0; i<8; ++i)
    {
        d<<=1;
        
        // The eeprom will pull the data line low when the bit starts
        if( wait_for_sdata_state(0) )
        {
            dbg_uart_tx_str(" tmo(s)");
            return 0;
        }
        
        // It will stay low for at least 2 pulses, depending on the state 
        // after the 4th pulse we have a 1 or 0 bit
        serial_data_clock_pulse(4);
        
        // Read the actual data bit
        if( n64_ll_sdata_state() != 0)
            d|=1;
        
        // Wait for bit to end
        if( wait_for_sdata_state(1) )
        {
            dbg_uart_tx_str(" tmo(e)");
            return 0;
        }
    }
    
    return d;
}

void n64_read_eeprom_word(uint8_t word_index, uint8_t* buffer)
{    
    // First we need to pulse the clock for a couple of cycles.
    // This prevents the EEPROM from having problems when writing (and should at least be 15 ms).
    // We'll do the same again at the end of the cycle.
    serial_data_clock_pulse(_idle_clock_pulses);

    serial_data_send(N64_EEPROM_CMD_READ);
    serial_data_send(word_index);
    serial_data_stop();
    
    for(int i=0; i<N64_EEPROM_WORD_SIZE; ++i)
        buffer[i] = serial_data_read();
    
    serial_data_stop();
    serial_data_clock_pulse(_idle_clock_pulses);
}

#undef _idle_clock_pulses

// </eeprom>
