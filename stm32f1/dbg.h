#ifndef _DBG_H
#define _DBG_H

#include <stdint.h>
#include <stm32f1xx.h>

// Debug pin low
#define dbg_low() GPIOC->BSRR = GPIO_BSRR_BR13

// Debug pin hi
#define dbg_hi()  GPIOC->BSRR = GPIO_BSRR_BS13

// Setup the debug output
void dbg_init();

// Transmits a byte via UART, this is synchronous
void dbg_uart_tx(uint8_t b);

// Transmits a cstring (0-terminated)
void dbg_uart_tx_str(const char* msg);

// Write out a hex representation of n on UART
void dbg_uart_tx_hex_u8(uint8_t n);
// Write out a hex representation of n on UART
void dbg_uart_tx_hex_u16(uint16_t n);
// Write out a hex representation of n on UART
void dbg_uart_tx_hex_u32(uint32_t n);

// Output the  indicator  on UART and completely halt execution, only external reset will work
void die(uint16_t indicator);

// Will disable any kind of output except for the die call
void dbg_disable_all_except_die();

#endif
