#include <string.h>
#include <stm32f1xx.h>
#include "n64-usb-descriptors.h"


// see reference manual "Unique device ID register (96 bits)"
#define _unique_device_id_len (96/8)
const uint8_t* device_id = (const uint8_t*)UID_BASE;

#define _unique_head_len 3
#define _unique_0_terminator 1
#define _unique_serial_len 8
static char unique_serial[_unique_head_len + _unique_serial_len + _unique_0_terminator];


static void to_hex_string(char* hex_here, uint8_t* data, uint16_t data_len )
{
    for( int i=0; i<(data_len*2); ++i)
    {
        uint8_t n = data[i/2];
        
        if( (i & 0x1) == 0)
        {
            n >>= 4;
        }
        
        n &= 0x0f;
        
        hex_here[i] = ( n < 10 ) ? '0' + n : 'a' + (n-10);
    }
}

static void unique_serial_init()
{
    unique_serial[0] = 'N'; // N64 ...
    unique_serial[1] = 'C'; // ... Cart ...
    unique_serial[2] = 'D'; // ... Dumper
    
    uint8_t id_buf[_unique_serial_len / 2];
    memset( id_buf, 0, sizeof(id_buf));
    
    for(int i=0; i<_unique_device_id_len; ++i)
    {
        id_buf[i%sizeof(id_buf)] ^= device_id[i];
    }
    
    to_hex_string( unique_serial + _unique_head_len, id_buf, sizeof(id_buf) );
    
    unique_serial[sizeof(unique_serial) - 1] = 0;
}

static uint8_t init = 0;

const char* n64_usb_get_serial()
{
    if( init != 1 )
    {
        unique_serial_init();
        init = 1;
    }
    
    return (const char*)unique_serial;
}

#undef _unique_serial_len
#undef _unique_0_terminator
#undef _unique_head_len
#undef _unique_device_id_len
