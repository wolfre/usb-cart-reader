#ifndef _USB_H
#define _USB_H

#include <stdint.h>


#define USB_MAX_PKG_SIZE 64


typedef enum
{ 
    ep_type_bulk,
    ep_type_control,
    ep_type_isochronous,
    ep_type_interrupt
} ep_type;

typedef enum
{ 
    transfer_out,
    transfer_setup,
} transfer_type;

typedef void (*on_reset)();
typedef void (*on_rx_data)(transfer_type, uint16_t, uint8_t*);
typedef void (*on_tx_done)();

typedef struct 
{
    // [0;15]
    uint8_t address;
    ep_type type;
    uint8_t tx_buffer_size;
    uint8_t rx_buffer_size;
    on_reset reset_handler;
    on_rx_data rx_handler;
    on_tx_done tx_done_handler;
} usb_ep_config;

// Init all the USB peripherial things and clear internal states
//   USB is not yet activated
void usb_init();

// Allocate a new endpoint, must be done prior to  usb_connect
usb_ep_config* usb_new_ep();

// Enable USB to start connecting with the host
void usb_connect();

// Must be done during setup procedure / enumeration (via endpoint 0), after (each) reset.
void usb_set_device_address(uint8_t address);

// Setup data to TX on the next IN transfer from host
void usb_tx_data(usb_ep_config* config, uint16_t len, const void* data);

// Transmit a 0-byte packet on next IN transfer
void usb_tx_0(usb_ep_config* config);

// The next IN transfer from host will be NAcKed
void usb_tx_nak(usb_ep_config* config);

// The next IN transfer from host will be STALLed
void usb_tx_stall(usb_ep_config* config);

// The EP is ready to receive data up to  max_rx_size  bytes on the next OUT or SETUP transfer
void usb_rx_ready(usb_ep_config* config, uint16_t max_rx_size);

#endif
