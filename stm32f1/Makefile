
CC = arm-none-eabi-gcc
OC = arm-none-eabi-objcopy
OD = arm-none-eabi-objdump

CFLAGS += -mlittle-endian 
CFLAGS += -mfloat-abi=soft 
CFLAGS += -mcpu=cortex-m3 
CFLAGS += -mthumb
CFLAGS += -O3
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -Wl,--gc-sections

FLASH_START=0x08000000

STM32_CUBE_F1=STM32CubeF1

# NOTE adapt that to the chip you are using
CHIP_DEF += -DSTM32F10X_MD
# NOTE not entirely correct, but the B variant is closest to C in peripherials
CHIP_DEF += -DSTM32F103xB

# NOTE not entirely correct, this linker script is for 128k flash, but the c8 has 64k flash.
#   However as long as we don't go over board things should be fine
CHIP_LINK += -T./$(STM32_CUBE_F1)/Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/gcc/linker/STM32F103XB_FLASH.ld

CHIP_INCL += -I./$(STM32_CUBE_F1)/Drivers/CMSIS/Core/Include
CHIP_INCL += -I./$(STM32_CUBE_F1)/Drivers/CMSIS/Device/ST/STM32F1xx/Include

# NOTE not entirely correct, but the amount of interrupts set up should fit the c8
CHIP_SRC += $(STM32_CUBE_F1)/Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/gcc/startup_stm32f103xb.s
CHIP_SRC += $(STM32_CUBE_F1)/Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/system_stm32f1xx.c

# NOTE extend the HAL_SRC with what ever you need in code
HAL_INCL += -I./$(STM32_CUBE_F1)/Drivers/STM32F1xx_HAL_Driver/Inc


TARGET = usb-cart-reader
SRC += main.c
SRC += dbg.c
SRC += usb.c
SRC += usb-ep0.c
SRC += n64-ll.c
SRC += n64.c
SRC += n64-usb-descriptors.c
SRC += n64-usb-protocol.c
HEADER += main.h
HEADER += dbg.h
HEADER += usb.h
HEADER += usb-ep0.h
HEADER += n64-ll.h
HEADER += n64.h
HEADER += n64-usb-descriptors.h
HEADER += n64-usb-protocol.h

$(TARGET).elf: cubef1 Makefile $(SRC) $(HEADER)
	@test "$(EMAIL)" != "" || bash -c "echo You need to set EMAIL in order to build the usb part, see 'https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt' && exit 1"
	$(CC) \
		-DEMAIL=\"$(EMAIL)\" \
		$(CFLAGS) \
		$(CHIP_DEF) $(CHIP_INCL) $(CHIP_LINK) $(HAL_INCL) \
		-o $(TARGET).elf \
		$(CHIP_SRC) $(HAL_SRC) $(SRC)
	$(OD) -h -S $(TARGET).elf > $(TARGET).lst

$(TARGET).bin: $(TARGET).elf
	$(OC) -O binary $(TARGET).elf $(TARGET).bin

$(TARGET).hex: $(TARGET).elf
	$(OC) -O ihex $(TARGET).elf $(TARGET).hex
	
flash: $(TARGET).bin
	st-flash write $(TARGET).bin $(FLASH_START)

cubef1: $(STM32_CUBE_F1)/README.md
$(STM32_CUBE_F1)/README.md:
	git clone https://github.com/STMicroelectronics/STM32CubeF1 -b v1.8.3 --depth 1 $(STM32_CUBE_F1)
	
clean:
	rm -f $(TARGET).elf $(TARGET).hex $(TARGET).bin $(TARGET).lst

clangd-completion: clean
	bear make $(TARGET).elf
	
