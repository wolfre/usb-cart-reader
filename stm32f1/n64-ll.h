#ifndef _N64_LL_H
#define _N64_LL_H

#include <stdint.h>

// Init low level perepherial, call this once before any other function in here
void n64_ll_init();

// Pull the (serial data) reset
void n64_ll_reset_pull();
// Release the (serial data) reset
void n64_ll_reset_release();

// Set read line low
void n64_ll_read_low();
// Set read line high
void n64_ll_read_hi();

// Set write line low
void n64_ll_write_low();
// Set write line high
void n64_ll_write_hi();

// Set addres enable latch, low address, low
void n64_ll_ael_l_low();
// Set addres enable latch, low address, high
void n64_ll_ael_l_hi();

// Set addres enable latch, high address, low
void n64_ll_ael_h_low();
// Set addres enable latch, high address, high
void n64_ll_ael_h_hi();

// Configure the multiplexed address and data lines as input
void     n64_ll_address_data_input();
// Read from the multiplexed address and data lines (need to be configured as input)
uint16_t n64_ll_address_data_read();
// Configure the multiplexed address and data lines as output
void     n64_ll_address_data_output();
// Write to  the multiplexed address and data lines (need to be configured as output)
void     n64_ll_address_data_write(uint16_t);

// Set the cart bus to idle state. This is the "safe state", 
// not causing the ROM to drive any of its outputs.
void n64_ll_bus_idle();


// Set serial clock line low
void n64_ll_clk_low();
// Set serial clock line hi
void n64_ll_clk_hi();
// Release serial data line back to high
void n64_ll_sdata_release();
// Pull serial data line low
void n64_ll_sdata_pull();

// Return 0 if data is low, 1 if it is high
uint8_t n64_ll_sdata_state();

// Set the serial data lines to idle state. This is the "safe state", 
// not causing the EEPROM to drive the data line.
void n64_ll_serial_idle();

// A delay of at least 500ns
void n64_ll_delay_500ns();

// A delay of at least 100ns
void n64_ll_delay_100ns();

// https://github.com/sanni/cartreader/blob/d8cb1246f8aa9504f6de271730a8a451117fe138/Cart_Reader/N64.ino#L2074
#define N64_EEPROM_CMD_READ 0x04

// http://n64devkit.square7.ch/pro-man/pro26/26-06.htm
#define N64_EEPROM_WORD_SIZE 8

#endif
