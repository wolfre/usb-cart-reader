#include "dbg.h"

static uint8_t disabled = 0;

void dbg_init()
{
    // enable gpio C and A and UART1 clock on APB2
    RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_USART1EN;

    // set pin C13 to output push-pull with 50mhz
    GPIOC->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
    GPIOC->CRH |= (3<<GPIO_CRH_MODE13_Pos);
    
    
    // map pin A9 to alt function for UART1 TX (with 50mhz)
    GPIOA->CRH &= ~(GPIO_CRH_CNF9_Msk | GPIO_CRH_MODE9_Msk);
    GPIOA->CRH |= (2<<GPIO_CRH_CNF9_Pos) | (3<<GPIO_CRH_MODE9_Pos);
    
    
    // program 39.0625 to baud rate reg for 112.5 kbaude @72 MHz sysclock
    // See ref manual "27.3.4 Fractional baud rate generation" for details
    USART1->BRR = 625; // NOTE devide by 16 to get Fractional representation
    
    // enable UART, enable TX, rest defaults which are ok
    USART1->CR1 = (1<<USART_CR1_UE_Pos)| (1<< USART_CR1_TE_Pos);
    dbg_hi();
    
    disabled = 0;
}


void dbg_uart_tx(uint8_t b)
{
    if( disabled != 0 ) return;
    
    while( (USART1->SR & USART_SR_TXE) == 0);
    USART1->DR = (uint32_t)b;
}

void dbg_uart_tx_str(const char* msg)
{
    if( disabled != 0 ) return;
    
    const uint8_t* ptr = (const uint8_t*)msg;
    
    while((*ptr) != 0)
    {
       dbg_uart_tx( *ptr );
       ++ptr;
    }
}

static void dbg_tx_hex4bit(uint8_t n)
{
    if( n < 10 )
        dbg_uart_tx('0'+n);
    else
        dbg_uart_tx('a'+(n-10));
}

void dbg_uart_tx_hex_u8(uint8_t n)
{
    if( disabled != 0 ) return;
    
    dbg_tx_hex4bit((n>>4)&0xf);
    dbg_tx_hex4bit(n & 0xf);
}

void dbg_uart_tx_hex_u16(uint16_t n)
{
    if( disabled != 0 ) return;
    
    for(int i=0;i<4;++i)
        dbg_tx_hex4bit((n >> (16 - ((i+1)*4)))&0xf);
}

void dbg_uart_tx_hex_u32(uint32_t n)
{
    if( disabled != 0 ) return;
    
    for(int i=0;i<8;++i)
        dbg_tx_hex4bit((n >> (32 - ((i+1)*4)))&0xf);
}


void die(uint16_t indicator)
{
    // https://stm32f4-discovery.net/2015/06/how-to-properly-enabledisable-interrupts-in-arm-cortex-m/
    __disable_irq();

    disabled = 0;
    
    for(int i=0;i<3;++i)
    {
        dbg_uart_tx('d');
        dbg_uart_tx('i');
        dbg_uart_tx('e');
        dbg_uart_tx('(');
        dbg_uart_tx('0');
        dbg_uart_tx('x');
        dbg_uart_tx_hex_u16(indicator);
        dbg_uart_tx(')');
        dbg_uart_tx('\n');
    }
    
    while(1)
    {
        dbg_hi();
        dbg_low();
    }
}


void dbg_disable_all_except_die()
{
    disabled = 1;
}
