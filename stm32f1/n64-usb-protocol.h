#ifndef _N64_USB_PROTOCOL_H
#define _N64_USB_PROTOCOL_H

#include "usb.h"

// This file contains the details on the protocol between the host app and the hardware dumper


// The one (and currently only) usb interface this device implements
#define USB_INTERFACE_NUMBER 0

// The USB endpoints, all of bulk type
#define USB_ENDPOINT_COMMAND 1
#define USB_ENDPOINT_DATA    2

// https://github.com/obdev/v-usb/blob/master/usbdrv/USB-IDs-for-free.txt
#define USB_VID 0x16c0
#define USB_PID 0x05dc

// The version of this protocol. Can be used by a host to detect the version running
// on the usb n64 cart dumper. This can be found in the device descriptors bcdDevice
// field
#define USB_N64_PROTOCOL_VERSION_MAJOR 0x01
#define USB_N64_PROTOCOL_VERSION_MINOR 0x00

// Size of the internal buffer
#define USB_DATA_BUFFER_SIZE 8*1024

// Each packet sent to the command endpoint is considered a command.
// Each command starts with a single byte denoting the command type.
// A command may carry additional parameters, see individual commands for details.
// The response to a command will be a single byte indicating the status of the command:
#define USB_CMD_STATUS_OK   1
#define USB_CMD_STATUS_FAIL 2
#define USB_CMD_UNKNOWN     3

// Will fill the internal buffer with the device info. The data endpoint will
// automatically start supplying the data. This removes the need for a seperate USB_CMD_BUS_READ.
// No additional parameters.
// The device info is a structure intended to detect and show certain properties of the device
// (firmware). The structure is composed of fields. Each field starts with a byte containing the 
// length of the data (of the field) that follows. Field are identified by their position in the
// structure. Currently defined fields are (in this order):
// - USB_DATA_BUFFER_SIZE encoded as an 16 bits (unsigned), MSB first
// - git commit hash, encoded as an 20 bytes, MSB first
// A field with the length 0 terminates the structure.
#define USB_CMD_DEVICE_INFO  0

// Setup a read from the internal buffer on the data endpoint. Each read will give the maximum
// amount of data allowed per transfer. The data endpoint will be nacked when reaching the end
// of the buffer.
// No additional parameters.
#define USB_CMD_BUFFER_READ  1

// Setup a write from the data endpoint to the internal buffer. Each write will increment
// the offeset in the buffer (by the ammount of data in the request). The data endpoint will 
// be nacked when reaching the end of the buffer.
// No additional parameters.
#define USB_CMD_BUFFER_WRITE  2

// Does a read from the cart bus to the internal buffer. After the read is done, the data endpoint
// automatically starts supplying the data. This removes the need for a seperate USB_CMD_BUS_READ.
// Parameters:
// - bus address to start the read, 32 bits (unsigned), MSB first. The lowest bit must be 0,
//   that means only even addresses are allowed! (This is how the address bus works on the N64)
// - amount of words to read, 16 bits (unsigned), MSB first. Max USB_DATA_BUFFER_SIZE/2.
// - stride in words, 16 bits (unsigned), MSB first. Max USB_DATA_BUFFER_SIZE/2.
//   This controls how may words are read per address setup. This depends on the kind of chip
//   that serves the address. If unsure, try 32.
#define USB_CMD_BUS_READ  3

// Does a write to the cart bus from the internal buffer.
// Parameters:
// - bus address to start the write, 32 bits (unsigned), MSB first. The lowest bit must be 0,
//   that means only even addresses are allowed! (This is how the address bus works on the N64)
// - amount of words to write, 16 bits (unsigned), MSB first. Max USB_DATA_BUFFER_SIZE/2.
// - stride in words, 16 bits (unsigned), MSB first. Max USB_DATA_BUFFER_SIZE/2.
//   This controls how may words are written per address setup. This depends on the kind of chip
//   that serves the address. If unsure, try 32.
#define USB_CMD_BUS_WRITE  4

// This command will read one word (8 byte) from a serial eeprom into the internal data buffer.
// After the read is done, the data endpoint automatically starts supplying the data. This
// removes the need for a seperate USB_CMD_BUS_READ.
// WARNING: This is unreliable, probably due to incorrect / unknown EEPROM timing!
// Make sure to double or tripple check each word you've read!
// Parameters:
// - eeprom word to read, 8 bits (unsigned)
#define USB_CMD_EEPROM_READ  5

void n64_usb_init(usb_ep_config* ep_command, usb_ep_config* ep_data);

#endif
