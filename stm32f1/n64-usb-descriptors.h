#ifndef _N64_USB_DESCRIPTORS_H
#define _N64_USB_DESCRIPTORS_H

#include <stdint.h>
#include "usb.h"
#include "usb-ep0.h"
#include "n64-usb-protocol.h"

#define _idx_str_lang 0
static const uint8_t str_language[] = 
{
    4,
    usb_string_descriptor,
    // English only
    0x09, 0x04, 
};

#define _idx_str_manufactur 1
#define _idx_str_product 2
#define _idx_str_serial 3

static const uint8_t device_descriptor[] = 
{
    18,   // bLength
    1,    // bDescriptorType
    0x00, 0x02, // bcdUSB
    0xFF, // bDeviceClass
    0xFF, // bDeviceSubClass
    0xFF, // bDeviceProtocol
    USB_EP0_MAX_PKG_SIZE, // bMaxPacketSize0
    USB_VID & 0xff, (USB_VID>>8) & 0xff, // idVendor
    USB_PID & 0xff, (USB_PID>>8) & 0xff, // idProduct
    USB_N64_PROTOCOL_VERSION_MINOR, USB_N64_PROTOCOL_VERSION_MAJOR,  // bcdDevice
    _idx_str_manufactur, // iManufacturer
    _idx_str_product,    // iProduct
    _idx_str_serial,     // iSerialNumber
    1 // bNumConfigurations
};


#define _len_desc_cfg   9
#define _len_desc_iface 9
#define _len_desc_ep 7
#define _num_logic_ep 4
static const uint8_t config_descriptor[] = 
{
    _len_desc_cfg, // bLength
    usb_config_descriptor,   // bDescriptorType
    _len_desc_cfg + _len_desc_iface + (_len_desc_ep * _num_logic_ep), 0, // wTotalLength
    1, // bNumInterfaces
    1, // bConfigurationValue
    0, // iConfiguration
    (1<<7 | 1<<6), // bmAttributes (we are bus powered)
    100, // bMaxPower Maximum Power Consumption in 2mA units -> 200mA not entirely sure about the ROM consumption ...
    
    // Interface 0 descriptor
    _len_desc_iface, // bLength
    usb_interface_descriptor, // bDescriptorType
    USB_INTERFACE_NUMBER, // bInterfaceNumber
    0, // bAlternateSetting
    _num_logic_ep, // bNumEndpoints
    0xff, // bInterfaceClass
    0xff, // bInterfaceSubClass
    0xff, // bInterfaceProtocol
    0, // iInterface
    
    // EP1 is the RX command Bulk OUT endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (0<<7)|(USB_ENDPOINT_COMMAND<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
    
    // EP2 is the TX status Bulk IN endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (1<<7)|(USB_ENDPOINT_COMMAND<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
    
    // EP3 is the RX data Bulk OUT endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (0<<7)|(USB_ENDPOINT_DATA<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
    
    // EP4 is the TX data Bulk IN endpoint
    _len_desc_ep, // bLength
    usb_endpoint_descriptor, // bDescriptorType
    (1<<7)|(USB_ENDPOINT_DATA<<0), // bEndpointAddress
    (2<<0), // bmAttributes
    USB_EP0_MAX_PKG_SIZE, 0, // wMaxPacketSize
    0, // bInterval
};


const char* n64_usb_get_serial();


#endif
