#include <stm32f1xx.h>
#include "n64-ll.h"
#include "dbg.h"

// Generally see here
// http://n64.icequake.net/mirror/www.crazynation.org/N64/
// https://os.mbed.com/users/hudakz/code/STM32F103C8T6_Hello/
// https://github.com/sanni/cartreader/blob/d8cb1246f8aa9504f6de271730a8a451117fe138/Cart_Reader/N64.ino#L17

void n64_ll_init()
{
    // enable gpio A and B, alt-function io clock on APB2
    // GPIO A+B is needed for the cart interface, and alt-function is needed for remapping
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_AFIOEN;            

    // Disable the JTAG port (which is overriding PA15, PB3 and PB4 GPIO functions by default).
    // We only disable the JTAG part, but not the "SW-DP" part, otherwise we couldn't
    // programm the device any more. See
    // - User manual 9.3.5 "JTAG/SWD alternate function remapping"
    // - https://community.st.com/s/question/0D50X00009XkZipSAF/stm32f103-pa15-as-gpio
    AFIO->MAPR &= ~( AFIO_MAPR_SWJ_CFG_Msk );
    AFIO->MAPR |= 2 << AFIO_MAPR_SWJ_CFG_Pos;
    
    // Config the permanent outputs:
    // - RST   => PA8
    // - CLK   => PB6
    // - ALE_H => PB4
    // - ALE_L => PB5
    // - READ  => PB3
    // - WRITE => PA15
        
    
    // Set pin A8 (Cart RST) to output push-pull with 50mhz
    GPIOA->CRH &= ~(GPIO_CRH_CNF8_Msk | GPIO_CRH_MODE8_Msk);
    GPIOA->CRH |= (3<<GPIO_CRH_MODE8_Pos);
    
    // Set pin B3 (Cart READ) to output push-pull with 50mhz
    GPIOB->CRL &= ~(GPIO_CRL_CNF3_Msk | GPIO_CRL_MODE3_Msk);
    GPIOB->CRL |= (3<<GPIO_CRL_MODE3_Pos);
    
    // Set pin A15 (Cart WRITE) to output push-pull with 50mhz
    GPIOA->CRL &= ~(GPIO_CRH_CNF15_Msk | GPIO_CRH_MODE15_Msk);
    GPIOA->CRL |= (3<<GPIO_CRH_MODE15_Pos);
    
    // Set pin B4 (Cart ALE_H) to output push-pull with 50mhz
    GPIOB->CRL &= ~(GPIO_CRL_CNF4_Msk | GPIO_CRL_MODE4_Msk);
    GPIOB->CRL |= (3<<GPIO_CRL_MODE4_Pos);
    
    // Set pin B5 (Cart ALE_L) to output push-pull with 50mhz
    GPIOB->CRL &= ~(GPIO_CRL_CNF5_Msk | GPIO_CRL_MODE5_Msk);
    GPIOB->CRL |= (3<<GPIO_CRL_MODE5_Pos);
    
    // Set pin B6 (Cart serial clock) to output push-pull with 2mhz (fast enough for our clock)
    GPIOB->CRL &= ~(GPIO_CRL_CNF6_Msk | GPIO_CRL_MODE6_Msk);
    GPIOB->CRL |= (2<<GPIO_CRL_MODE6_Pos);
    
    
    // Setup the reset state of the cart port connector and serial lines
    n64_ll_serial_idle();
    n64_ll_bus_idle();
}

void n64_ll_serial_idle()
{
    n64_ll_reset_release();
    // https://github.com/sanni/cartreader/blob/d8cb1246f8aa9504f6de271730a8a451117fe138/Cart_Reader/N64.ino#L447
    n64_ll_clk_hi();
    n64_ll_sdata_release();
}

void n64_ll_bus_idle()
{
    n64_ll_address_data_input();
    n64_ll_read_hi();
    n64_ll_write_hi();
    n64_ll_ael_l_low();
    n64_ll_ael_h_low();   
}


inline void n64_ll_reset_pull()
{
    GPIOA->BSRR = GPIO_BSRR_BR8;
}

inline void n64_ll_reset_release()
{
    GPIOA->BSRR = GPIO_BSRR_BS8;
}


inline void n64_ll_read_low()
{
    GPIOB->BSRR = GPIO_BSRR_BR3;
}

inline void n64_ll_read_hi()
{
    GPIOB->BSRR = GPIO_BSRR_BS3;
}


inline void n64_ll_write_low()
{
    GPIOA->BSRR = GPIO_BSRR_BR15;
}

inline void n64_ll_write_hi()
{
    GPIOA->BSRR = GPIO_BSRR_BS15;
}


inline void n64_ll_ael_l_low()
{
    GPIOB->BSRR = GPIO_BSRR_BR5;
}

inline void n64_ll_ael_l_hi()
{
    GPIOB->BSRR = GPIO_BSRR_BS5;
}


inline void n64_ll_ael_h_low()
{
    GPIOB->BSRR = GPIO_BSRR_BR4;
}

inline void n64_ll_ael_h_hi()
{
    GPIOB->BSRR = GPIO_BSRR_BS4;
}


// NOTE on wiring of multiplexed address and data lines:
// pa[0;7]  => ad[0;7]
// pb[8;15] => ad[8;15]

void n64_ll_address_data_input()
{
    // We configure PORTA [0;7] pins as inputs with pull down resistor
    
    GPIOA->BSRR = \
        GPIO_BSRR_BR0 | \
        GPIO_BSRR_BR1 | \
        GPIO_BSRR_BR2 | \
        GPIO_BSRR_BR3 | \
        GPIO_BSRR_BR4 | \
        GPIO_BSRR_BR5 | \
        GPIO_BSRR_BR6 | \
        GPIO_BSRR_BR7;
    
    GPIOA->CRL = \
        ( 2<<GPIO_CRL_CNF0_Pos ) | \
        ( 2<<GPIO_CRL_CNF1_Pos ) | \
        ( 2<<GPIO_CRL_CNF2_Pos ) | \
        ( 2<<GPIO_CRL_CNF3_Pos ) | \
        ( 2<<GPIO_CRL_CNF4_Pos ) | \
        ( 2<<GPIO_CRL_CNF5_Pos ) | \
        ( 2<<GPIO_CRL_CNF6_Pos ) | \
        ( 2<<GPIO_CRL_CNF7_Pos );
 
    // We configure PORTB [8;15] pins as inputs with pull down resistor
        
    GPIOB->BSRR = \
        GPIO_BSRR_BR8 | \
        GPIO_BSRR_BR9 | \
        GPIO_BSRR_BR10 | \
        GPIO_BSRR_BR11 | \
        GPIO_BSRR_BR12 | \
        GPIO_BSRR_BR13 | \
        GPIO_BSRR_BR14 | \
        GPIO_BSRR_BR15;
        
    GPIOB->CRH = \
        ( 2<<GPIO_CRH_CNF8_Pos ) | \
        ( 2<<GPIO_CRH_CNF9_Pos ) | \
        ( 2<<GPIO_CRH_CNF10_Pos ) | \
        ( 2<<GPIO_CRH_CNF11_Pos ) | \
        ( 2<<GPIO_CRH_CNF12_Pos ) | \
        ( 2<<GPIO_CRH_CNF13_Pos ) | \
        ( 2<<GPIO_CRH_CNF14_Pos ) | \
        ( 2<<GPIO_CRH_CNF15_Pos );
}

void n64_ll_address_data_output()
{
    // We configure PORTA [0;7] pins as output push-pull with 50mhz
    
    GPIOA->BSRR = \
        GPIO_BSRR_BR0 | \
        GPIO_BSRR_BR1 | \
        GPIO_BSRR_BR2 | \
        GPIO_BSRR_BR3 | \
        GPIO_BSRR_BR4 | \
        GPIO_BSRR_BR5 | \
        GPIO_BSRR_BR6 | \
        GPIO_BSRR_BR7;
    
    GPIOA->CRL = \
        ( 3<<GPIO_CRL_MODE0_Pos ) | \
        ( 3<<GPIO_CRL_MODE1_Pos ) | \
        ( 3<<GPIO_CRL_MODE2_Pos ) | \
        ( 3<<GPIO_CRL_MODE3_Pos ) | \
        ( 3<<GPIO_CRL_MODE4_Pos ) | \
        ( 3<<GPIO_CRL_MODE5_Pos ) | \
        ( 3<<GPIO_CRL_MODE6_Pos ) | \
        ( 3<<GPIO_CRL_MODE7_Pos );
        
    // We configure PORTB [8;15] pins as output push-pull with 50mhz
    
    GPIOB->BSRR = \
        GPIO_BSRR_BR8 | \
        GPIO_BSRR_BR9 | \
        GPIO_BSRR_BR10 | \
        GPIO_BSRR_BR11 | \
        GPIO_BSRR_BR12 | \
        GPIO_BSRR_BR13 | \
        GPIO_BSRR_BR14 | \
        GPIO_BSRR_BR15;
    
    GPIOB->CRH = \
        ( 3<<GPIO_CRH_MODE8_Pos ) | \
        ( 3<<GPIO_CRH_MODE9_Pos ) | \
        ( 3<<GPIO_CRH_MODE10_Pos ) | \
        ( 3<<GPIO_CRH_MODE11_Pos ) | \
        ( 3<<GPIO_CRH_MODE12_Pos ) | \
        ( 3<<GPIO_CRH_MODE13_Pos ) | \
        ( 3<<GPIO_CRH_MODE14_Pos ) | \
        ( 3<<GPIO_CRH_MODE15_Pos );
}

uint16_t n64_ll_address_data_read()
{
    // The data / addres lines are spread across 2 ports. Try to read them as
    // "atomic" as possible. So its two read operations as close together as
    // possible, doing the masking after reading.
    uint16_t l = GPIOA->IDR;
    uint16_t h = GPIOB->IDR;
    return (h & 0xff00) | (l & 0x00ff);
}

// User manual 9.2.5 "Port bit set/reset register (GPIOx_BSRR) (x=A..G)"
#define GPIO_BSRR_RESET_Pos 16

static uint32_t to_bsrr(uint16_t data, uint16_t relevant_bits)
{
    uint32_t bsrr = data & relevant_bits;
    bsrr ^= relevant_bits;
    bsrr <<= GPIO_BSRR_RESET_Pos;
    bsrr |= data & relevant_bits;
    return bsrr;
}

#undef GPIO_BSRR_RESET_Pos

void n64_ll_address_data_write(uint16_t data)
{
    uint32_t l_bsrr = to_bsrr(data, 0x00ff);
    uint32_t h_bsrr = to_bsrr(data, 0xff00);
    
    // We try to set the data / addres state as "atomic" as possible. Meaning we precalc
    // the register values, then set them in the minimum amount of operations. We spread
    // the pins across 2 GPIO ports, so its 2 write operations.
    GPIOA->BSRR = l_bsrr;
    GPIOB->BSRR = h_bsrr;
}

void n64_ll_clk_low()
{
    GPIOB->BSRR = GPIO_BSRR_BR6;
}

void n64_ll_clk_hi()
{
    GPIOB->BSRR = GPIO_BSRR_BS6;
}

uint8_t n64_ll_sdata_state()
{
    return (GPIOB->IDR & (1<<7)) ? 1 : 0;
}

void n64_ll_sdata_release()
{
    // Set pin B7 (Serial DATA) to input with pull up
    GPIOB->BSRR = GPIO_BSRR_BS7;
    GPIOB->CRL &= ~(GPIO_CRL_CNF7_Msk | GPIO_CRL_MODE7_Msk);
    GPIOB->CRL |= (2<<GPIO_CRL_CNF7_Pos);
}

void n64_ll_sdata_pull()
{
    // Set pin B7 (Serial DATA) to output push-pull with 50mhz
    GPIOB->BSRR = GPIO_BSRR_BR7;
    GPIOB->CRL &= ~(GPIO_CRL_CNF7_Msk | GPIO_CRL_MODE7_Msk);
    GPIOB->CRL |= (3<<GPIO_CRL_MODE7_Pos);
}

void n64_ll_delay_500ns()
{
    // Manually timed at 72mhz to be a bit over 500ns
    __NOP (); __NOP (); __NOP (); __NOP ();
    __NOP (); __NOP (); __NOP (); __NOP ();
    __NOP (); __NOP (); __NOP (); __NOP ();
    __NOP (); __NOP (); __NOP (); __NOP ();
    __NOP (); __NOP (); __NOP (); __NOP ();
    __NOP (); __NOP (); __NOP (); __NOP ();
    __NOP (); __NOP (); __NOP (); __NOP ();
}

void n64_ll_delay_100ns()
{
    // Manually timed at 72mhz to be a bit over 100ns
    __NOP (); __NOP ();
}
