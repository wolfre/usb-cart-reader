#include "n64-usb-protocol.h"
#include "dbg.h"
#include "n64.h"
#include "n64-ll.h"
#include "build-info.h"
#include <string.h>

static usb_ep_config* ep_command;
static usb_ep_config* ep_data;

typedef uint8_t (*cmd_hdl)(uint16_t, uint8_t*);

#define _max_cmd 8
static cmd_hdl command_handler[_max_cmd];

static uint32_t decode_u32(uint8_t* data)
{
    uint32_t u32 = data[0];
    u32 <<= 8;
    u32 |= data[1];
    u32 <<= 8;
    u32 |= data[2];
    u32 <<= 8;
    u32 |= data[3];
    return u32;
}

static uint16_t decode_u16(uint8_t* data)
{
    uint16_t u16 = data[0];
    u16 <<= 8;
    u16 |= data[1];
    return u16;
}

typedef enum
{
    mode_nop,
    mode_read,
    mode_wirte,
} mode;

static mode     buffer_mode = mode_nop;
static uint32_t buffer_off = 0;
static uint8_t  buffer[USB_DATA_BUFFER_SIZE];

static void next_read()
{
    uint32_t len = USB_DATA_BUFFER_SIZE - buffer_off;
    if( len > ep_data->rx_buffer_size)
        len = ep_data->rx_buffer_size;
    
    if( len > 0 )
    {
        usb_tx_data(ep_data, len, buffer + buffer_off);
        buffer_off += len;
    }
    else
    {
        buffer_mode = mode_nop;
        dbg_uart_tx_str("end read");
    }
}

static void next_write()
{
    uint32_t len = USB_DATA_BUFFER_SIZE - buffer_off;
    
    if( len > 0 )
    {
        if( len > ep_data->rx_buffer_size )
            len = ep_data->rx_buffer_size;
        
        usb_rx_ready(ep_data, len);
    }
    else
    {
        dbg_uart_tx_str("end write");
        buffer_mode = mode_nop;
    }
}


// <commands>

static uint8_t cmd_device_info(uint16_t length, uint8_t* data)
{
    memset(buffer, 0, sizeof(buffer));
    buffer[0] = 2;
    buffer[1] = (sizeof(buffer) >> 8) & 0xff;
    buffer[2] = sizeof(buffer) & 0xff;
    buffer[3] = sizeof(git_commit);
    memcpy(buffer + 4, git_commit, sizeof(git_commit));
    
    buffer_off = 0;
    buffer_mode = mode_read;
    next_read();
    return USB_CMD_STATUS_OK;
}

static uint8_t cmd_buffer_write(uint16_t length, uint8_t* data)
{
    buffer_off = 0;
    buffer_mode = mode_wirte;
    next_write();
    usb_tx_nak(ep_data);
    return USB_CMD_STATUS_OK;
}

static uint8_t cmd_buffer_read(uint16_t length, uint8_t* data)
{
    buffer_off = 0;
    buffer_mode = mode_read;
    next_read();
    return USB_CMD_STATUS_OK;
}

typedef struct 
{
    uint32_t address;
    uint16_t words;
    uint16_t stride;
}
bus_args;

static uint8_t decode_args_bus_write_read(bus_args* args, uint16_t length, uint8_t* data)
{
    if( length < 8 )
    {
        dbg_uart_tx_str("args?");
        dbg_uart_tx_hex_u16(length);
        return USB_CMD_STATUS_FAIL;
    }
    
    args->address = decode_u32(data);
    args->words = decode_u16(data+4);
    args->stride = decode_u16(data+6);
    
    dbg_uart_tx('0');
    dbg_uart_tx('x');
    dbg_uart_tx_hex_u16(args->words);
    dbg_uart_tx('@');
    dbg_uart_tx_hex_u32(args->address);
    dbg_uart_tx('*');
    dbg_uart_tx_hex_u16(args->stride);
    
    if( args->words > (USB_DATA_BUFFER_SIZE/2) )
    {
        dbg_uart_tx_str("words");
        return USB_CMD_STATUS_FAIL;
    }
    
    if( args->stride < 1 )
    {
        dbg_uart_tx_str("stride");
        return USB_CMD_STATUS_FAIL;
    }
    
    return USB_CMD_STATUS_OK;
}

static uint8_t cmd_bus_write(uint16_t length, uint8_t* data)
{
    bus_args args;
    
    if( decode_args_bus_write_read(&args, length, data) != USB_CMD_STATUS_OK )
        return USB_CMD_STATUS_FAIL;
    
    buffer_off = 0;
    buffer_mode = mode_nop;
    n64_write_bus_data(args.address, args.words, buffer, args.stride);
    
    return USB_CMD_STATUS_OK;
}

static uint8_t cmd_bus_read(uint16_t length, uint8_t* data)
{
    bus_args args;
    
    if( decode_args_bus_write_read(&args, length, data) != USB_CMD_STATUS_OK )
        return USB_CMD_STATUS_FAIL;

    n64_read_bus_data(args.address, args.words, buffer, args.stride);
    buffer_off = 0;
    buffer_mode = mode_read;
    next_read();
    
    return USB_CMD_STATUS_OK;
}

static uint8_t cmd_eeprom_read(uint16_t length, uint8_t* data)
{
    if( length < 1 )
    {
        dbg_uart_tx_str("args?");
        return USB_CMD_STATUS_FAIL;
    }
    
    n64_read_eeprom_word(data[0], buffer);
    buffer_off = 0;
    buffer_mode = mode_read;
    next_read();
    
    return USB_CMD_STATUS_OK;
}

// </commands>

// <ep-callbacks>

static void ep_command_rx(transfer_type type, uint16_t length, uint8_t* data)
{
    dbg_low();
    uint8_t status = USB_CMD_UNKNOWN;
    
        
    if( length < 1 )
    {
        dbg_uart_tx_str("empty CMD?\n");
    }
    else
    {
        uint8_t cmd = data[0];
        
        dbg_uart_tx_str("CMD(");
        dbg_uart_tx_hex_u8(cmd);
        dbg_uart_tx(')');
        
        cmd_hdl hdl = cmd < _max_cmd ? command_handler[cmd] : NULL;
        
        if( hdl != NULL)
        {
            status = hdl(length-1, data+1);
        }
        else
        {
            dbg_uart_tx('?');
        }
    }
    
    usb_tx_data(ep_command, sizeof(1), &status);
    usb_rx_ready(ep_command, ep_command->rx_buffer_size);
    dbg_uart_tx('\n');
    dbg_hi();
}

static void ep_command_tx_done()
{
    // NOOP
    dbg_uart_tx_str("ep CMD tx\n");
}

static void ep_command_reset()
{
    usb_rx_ready(ep_command, ep_command->rx_buffer_size);
    usb_tx_nak(ep_command);
    dbg_uart_tx_str("ep CMD rst\n");
}

static void ep_data_reset()
{
    // NOOP
    usb_tx_nak(ep_data);
    dbg_uart_tx_str("ep DATA rst\n");
}

static void ep_data_rx(transfer_type type, uint16_t length, uint8_t* data)
{
    dbg_uart_tx('r');
    
    if( buffer_mode == mode_wirte )
    {
        uint32_t len = USB_DATA_BUFFER_SIZE - buffer_off;
        if( len > length )
            len = length;
        
        memcpy(buffer + buffer_off, data, len);
        buffer_off += len;
        
        next_write();
    }
    else
    {
        dbg_uart_tx('?');
    }
    
    dbg_uart_tx('\n');
}

static void ep_data_tx_done()
{
    dbg_uart_tx('t');
    
    if( buffer_mode == mode_read )
    {
        next_read();
    }
    else
    {
        dbg_uart_tx('?');
    }
    
    dbg_uart_tx('\n');
}

// </ep-callbacks>

void n64_usb_init(usb_ep_config* ep_cmd, usb_ep_config* ep_d)
{
    ep_command = ep_cmd;
    ep_data = ep_d;
    n64_init();
    
    memset(command_handler, 0, sizeof(command_handler));
    command_handler[USB_CMD_DEVICE_INFO] = &cmd_device_info;
    command_handler[USB_CMD_BUFFER_WRITE] = &cmd_buffer_write;
    command_handler[USB_CMD_BUFFER_READ] = &cmd_buffer_read;
    command_handler[USB_CMD_BUS_READ] = &cmd_bus_read;
    command_handler[USB_CMD_BUS_WRITE] = &cmd_bus_write;
    command_handler[USB_CMD_EEPROM_READ] = &cmd_eeprom_read;
    
    
    // NOTE we are implementing the 4 logical enpoints, the comman and status enpoints are both
    // the same physical (bidirectional) endpoint. However the two data endpoints are implemented
    // as one physical each.
    
    ep_command->address = USB_ENDPOINT_COMMAND;
    ep_command->reset_handler = &ep_command_reset;
    ep_command->rx_handler = &ep_command_rx;
    ep_command->tx_done_handler = &ep_command_tx_done;
    ep_command->rx_buffer_size = USB_MAX_PKG_SIZE;
    ep_command->tx_buffer_size = USB_MAX_PKG_SIZE;
    ep_command->type = ep_type_bulk;
    
    ep_data->address = USB_ENDPOINT_DATA;
    ep_data->reset_handler = &ep_data_reset;
    ep_data->rx_handler = &ep_data_rx;
    ep_data->tx_done_handler = &ep_data_tx_done;
    ep_data->rx_buffer_size = USB_MAX_PKG_SIZE;
    ep_data->tx_buffer_size = USB_MAX_PKG_SIZE;
    ep_data->type = ep_type_bulk;
}
