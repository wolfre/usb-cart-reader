#include <string.h>
#include <stdint.h>
#include <stm32f1xx.h>

#include "dbg.h"
#include "main.h"
#include "usb.h"
#include "usb-ep0.h"
#include "n64-usb-descriptors.h"
#include "n64-usb-protocol.h"


static void ext_crystal_8mhz_72mhz()
{
    // See Figure 2. Clock tree  in Datasheet
    // See 3.1 Flash access control register  in memory programming manual


    // Enable flash prefetch
    FLASH->ACR |= FLASH_ACR_PRFTBE;
    // Set flash wait states to 4, which is needed for 72 mhz cpu operation
    FLASH->ACR |= 2 << FLASH_ACR_LATENCY_Pos;
    
    // Switch on HSE (aka crystal) and wait for it to start up
    RCC->CR |= RCC_CR_HSEON;
    while( ( RCC->CR & RCC_CR_HSERDY) == 0 );
    
    uint32_t cfgr = RCC->CFGR;
    // PLLXTPRE = dont divide by 2 (8Mhz crystal / 1 = 8Mhz)
    cfgr &= ~RCC_CFGR_PLLXTPRE_Msk;
    
    // PLLMUL = 9 (9*8Mhz==72MHz)
    cfgr &= ~RCC_CFGR_PLLMULL_Msk;
    cfgr |= RCC_CFGR_PLLMULL9;
    
    // PLLSCR = HSE
    cfgr |= 1 << RCC_CFGR_PLLSRC_Pos;
    
    // AHB  prescaler = /1
    cfgr &= ~RCC_CFGR_HPRE_Msk;
    cfgr |= RCC_CFGR_HPRE_DIV1;
    
    // APB2 prescaler = /1
    cfgr &= ~RCC_CFGR_PPRE2_Msk;
    cfgr |= RCC_CFGR_PPRE2_DIV1;
    
    // APB1 prescaler = /2
    cfgr &= ~RCC_CFGR_PPRE1_Msk;
    cfgr |= RCC_CFGR_PPRE1_DIV2;
    
    // "commit" the above settings
    RCC->CFGR = cfgr;
    
    // startup pll and wait for it to be stable
    RCC->CR |= RCC_CR_PLLON;
    while( ( RCC->CR & RCC_CR_PLLRDY) == 0 );
    
    // Switch sysclock to PLLCLK
    cfgr = RCC->CFGR;
    cfgr &= ~RCC_CFGR_SW_Msk;
    cfgr |= RCC_CFGR_SW_PLL;
    RCC->CFGR = cfgr;
    
    // Wait for switch to complete
    while( ( RCC->CFGR & RCC_CFGR_SWS_Msk) != (RCC_CFGR_SWS_PLL) );
}

static void do_ep0_setup(usb_ep_config* ep0)
{
    usb_ep0_setup(ep0);
    usb_ep0_add_descriptor( usb_device_descriptor, 0, sizeof(device_descriptor), device_descriptor);
    usb_ep0_add_descriptor( usb_config_descriptor, 0, sizeof(config_descriptor), config_descriptor);
    usb_ep0_add_descriptor( usb_interface_descriptor, 0, sizeof(_len_desc_iface), config_descriptor + _len_desc_cfg);
    
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 0, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*0));
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 1, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*1));
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 2, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*2));
    usb_ep0_add_descriptor( usb_endpoint_descriptor, 3, sizeof(_len_desc_ep), config_descriptor + _len_desc_cfg + _len_desc_iface + (_len_desc_ep*3));
    
    usb_ep0_add_descriptor( usb_string_descriptor, _idx_str_lang, sizeof(str_language), str_language);
    // NOTE EMAIL must be defined by who ever is compiling this, see Makefile
    usb_ep0_add_string_descriptor(_idx_str_manufactur, EMAIL);
    usb_ep0_add_string_descriptor(_idx_str_product, "N64UsbCartDump");
    usb_ep0_add_string_descriptor(_idx_str_serial, n64_usb_get_serial()); 
}

int main()
{
    ext_crystal_8mhz_72mhz();
    dbg_init();
    
    dbg_uart_tx_str("\n========== Main (");
    dbg_uart_tx_str(n64_usb_get_serial());
    dbg_uart_tx_str(") ==========\n\n");

    usb_init();
        
    usb_ep_config* ep0 = usb_new_ep();
    if( ep0 == NULL )
        die(5);
    
    do_ep0_setup(ep0);
    
    usb_ep_config* ep_command = usb_new_ep();
    if( ep_command == NULL )
        die(6);
        
    usb_ep_config* ep_data = usb_new_ep();
    if( ep_data == NULL )
        die(7);
    
    
    n64_usb_init(ep_command, ep_data);

    usb_connect();
    
    while( usb_ep0_cfg_active() == USB_EP0_NO_CFG_ACTIVE );
    dbg_uart_tx_str("Config activated, go for app!\n");
    
    while(1) 
    {
    }
    
    return 0;
}
