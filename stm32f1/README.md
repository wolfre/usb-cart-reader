# The STM32f1 firmware

The firmware is composed of a couple of parts
- Chip setup
  - [main.c](main.c) and [main.h](main.h)
- Usb stack
  - [usb.c](usb.c), [usb.h](usb.h), [usb-ep0.c](usb-ep0.c) and [usb-ep0.h](usb-ep0.h)
- Usb app interface
  - [n64-usb-protocol.c](n64-usb-protocol.c), [n64-usb-protocol.h](n64-usb-protocol.h), [n64-usb-descriptors.c](n64-usb-descriptors.c) and [n64-usb-descriptors.h](n64-usb-descriptors.h)
- N64 cart routines
  - [n64-ll.c](n64-ll.c), [n64-ll.h](n64-ll.h), [n64.c](n64.c) and [n64.h](n64.h)

The entire application logic is inside the USB call backs. 
This means all interaction with the cart is handled in sync with USB transactions.
On one side this prevents any kind of parallel USB transactions and cart operations, reducing the throughput.
However it enables a very deterministic and un-interrupted interaction with the cart (especially important for the EEPROM parts).

## USB app interface

So how does the USB interface work?
For the real details, look into the code [here](n64-usb-protocol.h) and [here](n64-usb-descriptors.h).
This chapter covers the general idea and modus operandi of the device.

The dumper device has 3 USB endpoints:
- Endpoint 0 is the mandatory control endpoint for enumeration
- A (bidirectional BULK) endpoint for commands
- A (bidirectional BULK) endpoint for data

The dumper device also features an internal data buffer, which can be read from and written to, by the host PC app.
All communication to/from the cartridge goes via this buffer.
For the exact details (size, available operations/commands), have a look into the code.

### Example - Read cart header

To better illustrate the workings, lets say you want to read the header of an N64 cart.
Here's the rough steps for this:
- Send a command to read 32 words (64 bytes) from bus address 0x10000000 into the internal buffer
- Wait for read to finish
- Read out the internal buffer to the host PC

From a USB point of view, this is how the above is done:

```mermaid
sequenceDiagram
    host->>+Command-EP: OUT(ReadBus(32,0x10000000))
    Command-EP-->>-host: IN(Status(Ok))
    Data-EP->>host: IN(byte[64])
```

While the bus read is being executed, the command endpoint is NAKed.
The host will wait until data becomes available (which is the status).
As everything is nicely synchronized, the data is now available on the data endpoint.

### Example - Write data to bus

Let's look at a second example: write 64 words (128 bytes) to bus address 0x12345678.
Here's the rough steps for this:
- Send a command to write to the internal buffer
- Wait for command to finish
- Send 128 bytes of data
- Send a command to write 64 words to address 0x12345678
- Wait for command (write) to finish

And here again from a USB point of view:

```mermaid
sequenceDiagram
    host->>+Command-EP: OUT(WriteBuffer)
    Command-EP-->>-host: IN(Status(Ok))
    host->>Data-EP: OUT(byte[64])
    host->>Data-EP: OUT(byte[64])
    host->>+Command-EP: OUT(WriteBus(64,0x12345678))
    Command-EP-->>-host: IN(Status(Ok))
```

The first command tells the device to write incoming data to the buffer, starting at offset 0 (of the internal buffer).
Then we need 2 OUT transfers to send the entire 128 bytes of data we want to write to the bus.
This is needed as the maximum packet size is 64 bytes.
After the 128 bytes of data is in the internal buffer, we send the command to write that data to the bus.
When the command returns, the write has been done, and we are ready for more commands.

## Build it

You will need a fitting GCC compiler and make installed.
The firmware builds on the [STM32CubeF1](https://github.com/STMicroelectronics/STM32CubeF1) which will be automatically downloaded when building.
For the exact needed packages, have a look at the [CI pipeline scripts](../.gitlab-ci.yml).
Then you should be able to just run:

```bash
make EMAIL="you-mail@goes.here" 
```

See [Makefile](Makefile) for details on EMAIL variable.

## Flashing it

For flashing you need to have [st-link tools](https://github.com/stlink-org/stlink) installed (debian has [a prebuild package](https://packages.debian.org/de/sid/stlink-tools)).
If you just want to flash a pre-build firmware, then attach your STM32 to your programmer and run:

```bash
st-flash write usb-cart-reader.bin 0x08000000
```

If you have a full source code checkout and all the tools, you can build and flash like so:

```bash
make EMAIL="you-mail@goes.here" flash
```
